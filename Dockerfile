FROM python:3.6
ENV authmodes chap pubkey
ENV sslargs --no-ssl
ENV log_level INFO
ENV autosave_interval 1000

COPY server_requirements.txt /tmp/
RUN pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple -r /tmp/server_requirements.txt

ADD . /server

VOLUME /server_data
WORKDIR /server_data

# expose two ports, 31013 for forwarding user IDA events, 56329 for dealing with web requests
EXPOSE 31013 56329
CMD python3 /server/idarling_server.py -h 0.0.0.0 -p 31013 ${sslargs} -m ${authmodes} -l ${log_level} -i ${autosave_interval}