# 服务端基本使用方法

## 安装方法

这里我们建议服务端安装在 Linux 上，其中 Ubuntu 18.04 最佳。

服务端主要分为两部分

- 前端，提供项目管理、用户认证的可视化操作方式。
- 后端，负责收集和分发用户的操作序列，项目管理、用户认证具体实现。

这里我们提供两种安装方式，手工安装和 docker 自动安装。推荐使用 docker 安装。

### docker 安装

参考 `https://gitlab.com/THUCSTA/IDArlingProject/idarling_deploy`。

### 手工安装

无论是前端还是后端，首先需要做的都是下载并解压 IDArling 压缩包。

具体的安装步骤如下：

1. 安装依赖库，这里后端依赖的环境为 python3。

```sh
pip3 install -r server_requirements.txt
```

2. 根据不同需求运行 idarling 。一般来说，建议 idarling 在内网中使用。这里我们不使用 ssl 加密通信，同时支持 chap 和公钥认证模式。 关于更多的方式，请参考 `python3 idarling_server.py --help` 。
    1. 如果需要 ssl 加密通信，那就自行生成证书，得到 ca.pem、ca.key（需要有效证书，否则需要信任）。
    2. 如果手工安装的话，需要将 `idarling/network/server/restserver.py` 中的 `server.socket_host` 修改为 `127.0.0.1`，避免外部 ip 访问。

```shell
python3 idarling_server.py -h 0.0.0.0 --no-ssl -l DEBUG --authmode chap pubkey
```

3. 添加管理员用户，便于在前端添加删除用户。管理员账户默认为 `admin`，密码默认是`idarling`。

4. 运行前端，请参考 `https://gitlab.com/THUCSTA/IDArlingProject/idarling_admin` 项目。

## 基本使用

这里以 docker 为例。

### 登录 Web UI 后台

访问部署 idarling 的 8000 端口，然后使用默认的管理员账号密码：admin/idarling 进行登录。建议部署后立即修改密码。

### 用户管理

点击右侧的 `Users` 按钮进行用户管理。

#### 添加用户

根据需要添加用户，其中用户可以有密码、公钥两种认证方式。这里公钥需要输入公私钥里的公钥。私钥留作客户端认证使用。

![image-20191207200444527](figure/add-user.png)

#### 删除用户

点击右侧的删除即可。

![image-20191207200646639](figure/delete-user.png)

### 项目管理

默认情况下，所有用户都不具有项目的权限，也没有任何项目。

![image-20191207200838646](figure/default-projects.png)

#### 给予用户权限

通过点击右侧的修改权限，我们可以为某个用户分配某个文件夹的权限。

![image-20191207201006616](figure/assign-permission.png)

这里我们为`test` 用户分配 `test2` 目录的权限。这样 test 用户就有 `test2` 目录下所有项目的权限了。

![image-20191207201105622](figure/assign-permission-1.png)