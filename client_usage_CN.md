# 客户端使用指南

为避免不兼容的问题，建议所有的客户端使用同一平台的相同的 IDA 版本。本项目在 Windows 上进行了良好的测试， 在 MacOS 上进行了一定的测试，应该也可以使用。

## 安装方法

客户端只需要 IDArling 项目中的位于根目录的 idarling 文件夹和 idarling_plugin.py，具体安装步骤如下

1. 解压 IDArling 压缩包到任意位置。
2. 执行以下命令安装依赖。在开发时，IDA 还只支持 python 2，因此这里我们使用 python 2 环境。在 IDA  较为良好地支持 python3 后，我们会考虑进行迁移。

```
pip install -r client_requirements.txt
```

3. 在 IDA 的 plugin 文件夹中创建 idarling 和 idarling_plugin.py 的符号链接。

```
# windows 
mklink /D idarling path\to\idarling
mklink idarling_plugin.py  path\to\idarling_plugin.py
# linux 
ln -s path/to/idarling idarling
ln -s path/to/idarling_plugin.py 
```

这样即安装完毕。

## 基本使用

基本使用步骤如下

### 设置

打开 IDA，点击右下角 no server 区域的 `Disconnected`，然后点击 Settings 进行设置。

![image-20191207173112101](figure/connected.png)

Settings 主要有两个子栏目，一个是通用设置，一个是网络相关的设置。

#### 通用设置

1. 用户名，在他人 IDA 中显示的人名。也同时对应着 idarling 服务器中的账户。
2. 用户颜色，在他人 IDA 中显示的光标颜色。
3. `Disable all user cursors` 控制要不要显示别人的光标，其中有若干个窗口。对于 hexrays 窗口，目前处于 beta 版本，默认不显示。测试过程中，hexrays 窗口的显示没有什么问题，可以直接使用。当然前提是互相协作的用户使用相同版本的 IDA。
4. 是否允许其他用户发送通知，默认是开启的。
5. log 级别，即记录日志的级别。

![image-20191207173141584](figure/settings.png)

#### 网络设置

然后我们可以点击网络相关的设置，主要有如下功能

1. 添加、修改、删除服务器配置。
2. 控制客户端与服务器通信的参数。

![image-20191207173257345](figure/network-settings.png)

这里以添加服务器为例介绍。

##### 添加服务器

可以点击`Add Server` 来添加服务器。

1. 设置服务器的 ip。
2. 设置服务器的 port。
3. 是否禁用 SSL，需要和服务器保持一致，如果服务器开启了，客户端也必须开启。
4. 用户认证模式，需要与最初服务器的设置的认证模式保持一致。根据不同的认证模式填写不同的内容。
    1. 在这一步之前，需要在 idarling 服务器中添加用户。
    2. 如果使用 public key 模式，建议为 idarling 单独建立一个公私钥对，避免泄露个人私钥。

![image-20191207173344305](figure/add-server.png)

### 连接服务器

点击`Disconnected` 会显示已经配置的服务器，然后点击目标服务器即可。注意，`none` 为对应的认证方式。

![image-20191207173420677](figure/choose-server.png)

## 保存项目到服务器

基本步骤如下

1. 用 IDA 打开一个 idb 或者一个新的 binary。
2. 确保已经连接到目标服务器。
3. 点击 IDA 左上角 `File` 菜单栏的 `Save to server`。
4. 创建目录，填写相关信息。如若没有发现任何目录，说明用户在服务器端没有任何权限。注意，不同的图标标记不同的类型，例如，test2 目录下的 test2 是一个项目，而 test3 为文件夹。
    1. 如果是新加的权限的话，可以点击 `Refresh Tree` 来更新左侧信息。
5. 填写完信息后，即可点击`Create project`。

![image-20191207173735152](figure/save-to-server.png)

6. 创建对应项目的分支，即一个特定的数据库。然后点击`Create Database` 即可。

![image-20191207174040280](figure/create-database.png)

## 从服务器下载项目

基本步骤如下

1. 确保已经连接到目标服务器。
2. 点击 IDA 左上角 `File` 菜单栏的 `Open from server`。
3. 然后选择目标项目下载即可。这里我们选择上面保存的 hello 项目的 master 分支。然后点击 `Open database` 即可。

![image-20191207174248883](figure/open-database.png)

实际上，如果从服务器上看的话，我们会看到其它的项目和文件夹，如 `test`项目， `test1` 文件夹。

![image-20191207174514025](figure/server-project.png)

## 更新

目前该项目默认托管在 gitlab 中，项目地址为 `https://gitlab.com/THUCSTA/IDArlingProject/IDArling`，点击`Disconnected` 中显示的 `check for update` 就会自动下载更新。如果想要自行搭建，需要自行修改更新部分的代码。