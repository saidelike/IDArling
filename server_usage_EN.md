This is an english version of the [server_usage_CN.md](server_usage_CN.md) documentation.

# IDArling server tutorial

## Installation

We recommend to install the IDArling server on Linux, e.g. Ubuntu 18.04.

The IDArling server is divided into two parts:

- frontend: providing an entry point for project management and user authentication.
- backend: responsible for handling general IDA user's operations and the specific implementation for the project management, and user authentication.

Here we provide two installation methods, docker automatic installation and manual installation. It is recommended to use the docker installation that installs both the IDArling server (idarling_server) and the IDArling management web server (idarling_admin) automatically.

### docker installation

Refer to https://gitlab.com/saidelike/IDArling_deploy/README_EN.md

### Manual installation

The specific installation steps are as follows:

1. Install dependencies, which are Python 3 libraries:

```sh
cd idarling_server/
pip3 install -r server_requirements.txt
```

2. Run `idarling_server.py` according to your needs. 

It is recommended to execute it in a `screen`.

Generally speaking, it is recommended to use IDArling in your intranet. Here we do not use SSL encrypted communication, and support both chap and public key authentication modes. For more help, please refer to `python3 idarling_server.py --help`.

If you need SSL encrypted communication, then generate your own certificate and get `ca.pem`, `ca.key` (you need a valid certificate, otherwise trust).

Since we are interested in a manual installation here, you need to modify `server.socket_host` in `idarling/network/server/restserver.py` to `127.0.0.1` to avoid external IP access on port 56329.

```shell
python3 idarling_server.py -h 0.0.0.0 --no-ssl -l DEBUG --authmode chap pubkey
```

3. Run the management web server, please refer to the https://gitlab.com/saidelike/IDArling_admin/README_EN.md

4. Use the management web server to manage users. The default account is username `admin` and password `idarling`. Don't forget to change the password after the first connection.

## Basic use

Take docker as an example here.

### Log in to the management web server

Access port 8000 where IDArling is deployed, and log in using the default administrator account: `admin` / `idarling`. It is recommended to change the password immediately after deployment.

### User Management

Click the `Users` button on the left to manage users.

#### Add user

Add users as needed, in which users can have two authentication methods: password and public key. The public key is the key associated with the private key used by the client for authentication.

![image-20191207200444527](figure/add-user.png)

#### Delete users

Click Delete on the right.

![image-20191207200646639](figure/delete-user.png)

### Project management

By default, all users do not have project permissions, nor do they have any projects.

![image-20191207200838646](figure/default-projects.png)

#### Give user permissions

By clicking the edit permission on the right, we can assign a user a folder permission.

![image-20191207201006616](figure/assign-permission.png)

Here we assign the permission of the `test2` directory to the` test` user. In this way, the `test` user has the permissions of all items in the `test2` directory.

![image-20191207201105622](figure/assign-permission-1.png)