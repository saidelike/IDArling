This is an English version of the [client_usage_CN.md](client_usage_CN.md) documentation.

# IDArling client tutorial

To avoid incompatibilities, it is recommended that all clients use the same IDA version. This project has been well tested on Windows, and has been tested on MacOS and should be usable.

## Installation

The client only needs the `idarling` folder and `idarling_plugin.py` from `idarling_server` root directory. The specific installation steps are as follows:

1. Decompress the IDArling compressed package to any location.
2. Execute the following command to install dependencies. During development, IDA was only supporting Python 2, so here we use the Python 2 environment. After IDA supports Python 3 well, we will consider the migration.

```
pip install -r client_requirements.txt
```

3. Create a symbolic link of `idarling` and `idarling_plugin.py` in the IDA plugin folder.

On Windows:

```
mklink / D idarling path\to\idarling
mklink / D idarling_plugin.py path\to\idarling_plugin.py
```

On Linux:

```
ln -s path/to/idarling idarling
ln -s path/to/idarling_plugin.py 
```

This completes the installation.

## Basic use

The basic steps are as follows

### Settings

Open IDA, click `Disconnected` in the `no server` area in the lower right corner, and then click `Settings`.

![image-20191207173112101](figure/connected.png)

`Settings` mainly has two sub-columns, one is `General settings` and the other is `Network settings`.

#### General settings

1. User name: the name displayed in the IDA of others. It also corresponds to the account in the IDArling server used for authentication
2. User color: the cursor color displayed in the IDA of other users
3. Disable all user cursors: controls whether to display other people’s cursors. There are several windows. For the hexrays window, it is currently in the beta version and is not displayed by default. During testing, there was no problem with the display of the hexrays window, and it can be used directly. Of course, the premise is that users who collaborate with each other use the same version of IDA.
4. Whether to allow other users to send notifications is enabled by default.
5. Log level: the level of logging on the IDA plugin side.

![image-20191207173141584](figure/settings.png)

#### Network settings

Then we can click on the network related settings, which mainly have the following functions:

1. Add, modify, and delete server configurations.
2. The parameters that control the communication between the IDArling client and the IDArling server.

![image-20191207173257345](figure/network-settings.png)

Here we will add a server as an example.

##### Add server

You can click `Add Server` to add a server.

1. Set the server's IP.
2. Set the server port.
3. Whether to disable SSL needs to be consistent with the server. If the server is turned on, the client must also be turned on.
4. The user authentication mode needs to be consistent with the supported authentication modes set on the IDArling server.
    1. Before this step, users need to be added to the IDArling server using the web management system.
    2. If the public key mode is used, it is recommended to establish a public and private key pair separately for IDArling to avoid revealing personal private keys.

![image-20191207173344305](figure/add-server.png)

### Connect to the server

Clicking `Disconnected` will display the configured server, then click the target server. Note that `none` is the corresponding authentication method.

![image-20191207173420677](figure/choose-server.png)

## Save project to server

The basic steps are as follows:

1. Use IDA to open an existing IDB or a new binary.
2. Make sure you are connected to the target server.
3. Click `Save to server` on the` File` menu bar in the upper left corner of IDA.
4. Right click on `Server root` and create a directory. If no directory is found, the user does not have any permissions on the server. Note that different icons mark different types. For example, `test2` in the `test2` directory is a project, and `test3` is another directory.
    * Note: If a new permission has been recently added on the server side, you can click `Refresh Tree` to update the information on the left.
5. After filling in the information, you can click `Create project`.

![image-20191207173735152](figure/save-to-server.png)

6. Create a branch inside the project, that is, a specific database. Then click `Create Database`.

![image-20191207174040280](figure/create-database.png)

## Download project from server

The basic steps are as follows

1. Make sure you are connected to the target server.
2. Click `Open from server` on the` File` menu bar in the upper left corner of IDA.
3. Then select the target project to download. Here we select the `master` branch of the `hello` project saved above. Then click `Open database`.

![image-20191207174248883](figure/open-database.png)

In fact, if you look at it on the server side using the web management system, you will see the created projects and directories, such as the `test` project and the` test1` directory.

![image-20191207174514025](figure/server-project.png)

## Update

Currently the project is hosted in gitlab by default, and the project address is https://gitlab.com/THUCSTA/IDArlingProject/IDArling. Clicking` check for update` displayed in `Disconnected` will automatically download the latest update. If you want to build your own, you need to modify the code of the updated part yourself.