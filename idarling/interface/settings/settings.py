from functools import partial

from PyQt5.QtCore import Qt  # noqa: I202
from PyQt5.QtGui import QIcon, QPixmap, QColor
from PyQt5.QtWidgets import (
    QFrame,
    QCheckBox,
    QColorDialog,
    QComboBox,
    QDialog,
    QFormLayout,
    QHBoxLayout,
    QHeaderView,
    QLabel,
    QLineEdit,
    QPushButton,
    QSpinBox,
    QTableWidget,
    QTableWidgetItem,
    QTabWidget,
    QVBoxLayout,
    QWidget,
    QSizePolicy,
)

from idarling.managers import *

from idarling.interface.settings.serverinfo import ServerInfoDialog
from idarling.core.user.usercommands import *

import logging

from idarling.shared.utils import ROOT_LOGGER_NAME

logger = logging.getLogger("idarling.interface.settings")

class SettingsDialog(QDialog):
    """
    The dialog allowing an user to configure the plugin. It has multiple tabs
    used to group the settings by category (general, network, etc.).
    """

    def __init__(self):
        super(SettingsDialog, self).__init__()

        # General setup of the dialog
        logger.debug("Showing settings dialog")
        self.setWindowTitle("Settings")
        icon_path = ResourceManager().static_resource("settings.png")
        self.setWindowIcon(QIcon(icon_path))
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowCloseButtonHint)

        window_frame = QFrame(self)
        window_layout = QVBoxLayout(window_frame)
        tabs = QTabWidget(window_frame)
        window_layout.addWidget(tabs)

        # "General Settings" tab
        tab = QWidget(tabs)
        layout = QFormLayout(tab)
        layout.setFormAlignment(Qt.AlignVCenter)
        tabs.addTab(tab, "General Settings")

        user_frame = QFrame(tab)
        user_layout = QHBoxLayout(user_frame)
        layout.addRow(user_frame)

        # User color
        self._color_button = QPushButton("")
        self._color_button.setFixedSize(50, 30)

        def color_button_activated(_):
            self._set_color(qt_color=QColorDialog.getColor().rgb())

        self._color = ConfigManager().user.color
        self._set_color(ida_color=self._color)
        self._color_button.clicked.connect(color_button_activated)
        user_layout.addWidget(self._color_button)

        # User name
        self._name_line_edit = QLineEdit()
        name = ConfigManager().user.name
        self._name_line_edit.setText(name)
        user_layout.addWidget(self._name_line_edit)

        text = "Disable all user cursors"
        self._disable_all_cursors_checkbox = QCheckBox(text)
        layout.addRow(self._disable_all_cursors_checkbox)
        navbar_checked = not ConfigManager().cursors.navbar
        funcs_checked = not ConfigManager().cursors.funcs
        disasm_checked = not ConfigManager().cursors.disasm
        hexrays_checked = not ConfigManager().cursors.hexrays
        all_checked = navbar_checked and funcs_checked and disasm_checked and hexrays_checked
        self._disable_all_cursors_checkbox.setChecked(all_checked)

        def state_changed(state):
            enabled = state == Qt.Unchecked
            self._disable_navbar_cursors_checkbox.setChecked(not enabled)
            self._disable_navbar_cursors_checkbox.setEnabled(enabled)
            self._disable_funcs_cursors_checkbox.setChecked(not enabled)
            self._disable_funcs_cursors_checkbox.setEnabled(enabled)
            self._disable_disasm_cursors_checkbox.setChecked(not enabled)
            self._disable_disasm_cursors_checkbox.setEnabled(enabled)
            self._disable_hexrays_cursors_checkbox.setChecked(not enabled)
            self._disable_hexrays_cursors_checkbox.setEnabled(enabled)

        self._disable_all_cursors_checkbox.stateChanged.connect(state_changed)

        style_sheet = """QCheckBox{ margin-left: 20px; }"""

        text = "Disable navigation bar user cursors"
        self._disable_navbar_cursors_checkbox = QCheckBox(text)
        layout.addRow(self._disable_navbar_cursors_checkbox)
        self._disable_navbar_cursors_checkbox.setChecked(navbar_checked)
        self._disable_navbar_cursors_checkbox.setEnabled(not all_checked)
        self._disable_navbar_cursors_checkbox.setStyleSheet(style_sheet)

        text = "Disable functions window user cursors"
        self._disable_funcs_cursors_checkbox = QCheckBox(text)
        layout.addRow(self._disable_funcs_cursors_checkbox)
        self._disable_funcs_cursors_checkbox.setChecked(funcs_checked)
        self._disable_funcs_cursors_checkbox.setEnabled(not all_checked)
        self._disable_funcs_cursors_checkbox.setStyleSheet(style_sheet)

        text = "Disable disassembly view user cursors"
        self._disable_disasm_cursors_checkbox = QCheckBox(text)
        layout.addRow(self._disable_disasm_cursors_checkbox)
        self._disable_disasm_cursors_checkbox.setChecked(disasm_checked)
        self._disable_disasm_cursors_checkbox.setEnabled(not all_checked)
        self._disable_disasm_cursors_checkbox.setStyleSheet(style_sheet)

        text = "Disable hexrays view user cursors (beta)"
        self._disable_hexrays_cursors_checkbox = QCheckBox(text)
        layout.addRow(self._disable_hexrays_cursors_checkbox)
        self._disable_hexrays_cursors_checkbox.setChecked(hexrays_checked)
        self._disable_hexrays_cursors_checkbox.setEnabled(not all_checked)
        self._disable_hexrays_cursors_checkbox.setStyleSheet(style_sheet)

        text = "Allow other users to send notifications"
        self._notifications_checkbox = QCheckBox(text)
        layout.addRow(self._notifications_checkbox)
        checked = ConfigManager().user.notifications
        self._notifications_checkbox.setChecked(checked)

        # Log level
        debug_level_label = QLabel("Logging level: ")
        self._debug_level_combo_box = QComboBox()
        self._debug_level_combo_box.addItem("CRITICAL", logging.CRITICAL)
        self._debug_level_combo_box.addItem("ERROR", logging.ERROR)
        self._debug_level_combo_box.addItem("WARNING", logging.WARNING)
        self._debug_level_combo_box.addItem("INFO", logging.INFO)
        self._debug_level_combo_box.addItem("DEBUG", logging.DEBUG)
        self._debug_level_combo_box.addItem("TRACE", logging.TRACE)
        level = ConfigManager().level
        index = self._debug_level_combo_box.findData(level)
        self._debug_level_combo_box.setCurrentIndex(index)
        layout.addRow(debug_level_label, self._debug_level_combo_box)

        # "Network Settings" tab
        tab = QWidget(tabs)
        layout = QVBoxLayout(tab)
        tab.setLayout(layout)
        tabs.addTab(tab, "Network Settings")

        top_frame = QFrame(tab)
        layout.addWidget(top_frame)
        top_layout = QHBoxLayout(top_frame)

        self._servers = list(ConfigManager().servers)
        self._servers_table = QTableWidget(len(self._servers), 2, self)
        top_layout.addWidget(self._servers_table)
        for i, server in enumerate(self._servers):
            # Server host and port
            item = QTableWidgetItem("%s:%d (%s)" % (server["host"], server["port"], server["authmode"]))
            item.setData(Qt.UserRole, server)
            item.setFlags(item.flags() & ~Qt.ItemIsEditable)
            if NetworkManager().server == server:
                item.setFlags((item.flags() & ~Qt.ItemIsSelectable))
            self._servers_table.setItem(i, 0, item)

            # Server has SSL enabled?
            checkbox = QTableWidgetItem()
            state = Qt.Unchecked if server["no_ssl"] else Qt.Checked
            checkbox.setCheckState(state)
            checkbox.setFlags((checkbox.flags() & ~Qt.ItemIsEditable))
            checkbox.setFlags((checkbox.flags() & ~Qt.ItemIsUserCheckable))
            if NetworkManager().server == server:
                checkbox.setFlags((checkbox.flags() & ~Qt.ItemIsSelectable))
            self._servers_table.setItem(i, 1, checkbox)

        self._servers_table.setHorizontalHeaderLabels(("Servers", "SSL"))
        horizontal_header = self._servers_table.horizontalHeader()
        horizontal_header.setSectionsClickable(False)
        horizontal_header.setSectionResizeMode(0, QHeaderView.Stretch)
        horizontal_header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self._servers_table.verticalHeader().setVisible(False)
        self._servers_table.setSelectionBehavior(QTableWidget.SelectRows)
        self._servers_table.setSelectionMode(QTableWidget.SingleSelection)
        self._servers_table.itemClicked.connect(self._server_clicked)

        buttons_frame = QWidget(top_frame)
        buttons_layout = QVBoxLayout(buttons_frame)
        top_layout.addWidget(buttons_frame)

        # Add server button
        self._add_button = QPushButton("Add Server")
        self._add_button.clicked.connect(self._add_button_clicked)
        buttons_layout.addWidget(self._add_button)

        # Edit server button
        self._edit_button = QPushButton("Edit Server")
        self._edit_button.setEnabled(False)
        self._edit_button.clicked.connect(self._edit_button_clicked)
        buttons_layout.addWidget(self._edit_button)

        # Delete server button
        self._delete_button = QPushButton("Delete Server")
        self._delete_button.setEnabled(False)
        self._delete_button.clicked.connect(self._delete_button_clicked)
        buttons_layout.addWidget(self._delete_button)

        bottom_frame = QWidget(tab)
        bottom_layout = QFormLayout(bottom_frame)
        layout.addWidget(bottom_frame)

        # TCP Keep-Alive settings
        keep_cnt_label = QLabel("Keep-Alive Count: ")
        self._keep_cnt_spin_box = QSpinBox(bottom_frame)
        self._keep_cnt_spin_box.setRange(0, 86400)
        self._keep_cnt_spin_box.setValue(ConfigManager().keep.cnt)
        self._keep_cnt_spin_box.setSuffix(" packets")
        bottom_layout.addRow(keep_cnt_label, self._keep_cnt_spin_box)

        keep_intvl_label = QLabel("Keep-Alive Interval: ")
        self._keep_intvl_spin_box = QSpinBox(bottom_frame)
        self._keep_intvl_spin_box.setRange(0, 86400)
        self._keep_intvl_spin_box.setValue(
            ConfigManager().keep.intvl
        )
        self._keep_intvl_spin_box.setSuffix(" seconds")
        bottom_layout.addRow(keep_intvl_label, self._keep_intvl_spin_box)

        keep_idle_label = QLabel("Keep-Alive Idle: ")
        self._keep_idle_spin_box = QSpinBox(bottom_frame)
        self._keep_idle_spin_box.setRange(0, 86400)
        self._keep_idle_spin_box.setValue(ConfigManager().keep.idle)
        self._keep_idle_spin_box.setSuffix(" seconds")
        bottom_layout.addRow(keep_idle_label, self._keep_idle_spin_box)

        # Buttons commons to all tabs
        actions_frame = QWidget(self)
        actions_layout = QHBoxLayout(actions_frame)

        # Cancel = do not save the changes and close the dialog
        def cancel(_):
            self.reject()

        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(cancel)
        actions_layout.addWidget(cancel_button)

        # Reset = reset all settings from all tabs to default values
        reset_button = QPushButton("Reset")
        reset_button.clicked.connect(self._reset)
        actions_layout.addWidget(reset_button)

        # Save = save the changes and close the dialog
        def save(_):
            self._commit()
            self.accept()

        save_button = QPushButton("Save")
        save_button.clicked.connect(save)
        actions_layout.addWidget(save_button)
        window_layout.addWidget(actions_frame)

        self.setLayout(window_layout)
        # Do not allow the user to resize the dialog (Are you SB?)
        #   self.setFixedSize(
        #    window_widget.sizeHint().width(), window_widget.sizeHint().height()
        #)
        #self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setMinimumSize(window_frame.sizeHint())

    def _set_color(self, ida_color=None, qt_color=None):
        """Sets the color of the user color button."""
        # IDA represents colors as 0xBBGGRR
        if ida_color is not None:
            r = ida_color & 255
            g = (ida_color >> 8) & 255
            b = (ida_color >> 16) & 255

        # Qt represents colors as 0xRRGGBB
        if qt_color is not None:
            r = (qt_color >> 16) & 255
            g = (qt_color >> 8) & 255
            b = qt_color & 255

        ida_color = r | g << 8 | b << 16
        qt_color = r << 16 | g << 8 | b

        # Set the stylesheet of the button
        #css = "QPushButton {background-color: #%06x; color: #%06x;}"
        #self._color_button.setStyleSheet(css % (qt_color, qt_color))
        buttonrect = self._color_button.rect()
        pixmapsize = buttonrect.adjusted(0, 0, -6, -6).size() # adds a little margin
        pixmap = QPixmap(pixmapsize)
        pixmap.fill(QColor(qt_color))
        buttonicon = QIcon(pixmap)
        self._color_button.setIcon(buttonicon)
        self._color_button.setIconSize(pixmapsize)
        self._color = ida_color

    def _server_clicked(self, _):
        self._edit_button.setEnabled(True)
        self._delete_button.setEnabled(True)

    def _add_button_clicked(self, _):
        dialog = ServerInfoDialog("Add server")
        dialog.accepted.connect(partial(self._add_dialog_accepted, dialog))
        dialog.exec_()

    def _edit_button_clicked(self, _):
        sels =  self._servers_table.selectedItems()
        if len(sels) == 0:
            return
        item = self._servers_table.selectedItems()[0]
        server = item.data(Qt.UserRole)
        dialog = ServerInfoDialog("Edit server", server)
        dialog.accepted.connect(partial(self._edit_dialog_accepted, dialog))
        dialog.exec_()

    def _delete_button_clicked(self, _):
        sels = self._servers_table.selectedItems()
        if len(sels) == 0:
            return
        item = self._servers_table.selectedItems()[0]
        server = item.data(Qt.UserRole)
        self._servers.remove(server)
        ConfigManager().save_config()
        self._servers_table.removeRow(item.row())
        self.update()

    def _add_dialog_accepted(self, dialog):
        """Called when the dialog to add a server is accepted."""
        server = dialog.get_result()
        self._servers.append(server)
        row_count = self._servers_table.rowCount()
        self._servers_table.insertRow(row_count)

        new_server = QTableWidgetItem(
            "%s:%d (%s)" % (server["host"], server["port"], server["authmode"])
        )
        new_server.setData(Qt.UserRole, server)
        new_server.setFlags(new_server.flags() & ~Qt.ItemIsEditable)
        self._servers_table.setItem(row_count, 0, new_server)

        new_checkbox = QTableWidgetItem()
        state = Qt.Unchecked if server["no_ssl"] else Qt.Checked
        new_checkbox.setCheckState(state)
        new_checkbox.setFlags((new_checkbox.flags() & ~Qt.ItemIsEditable))
        new_checkbox.setFlags(new_checkbox.flags() & ~Qt.ItemIsUserCheckable)
        self._servers_table.setItem(row_count, 1, new_checkbox)
        self.update()

    def _edit_dialog_accepted(self, dialog):
        """Called when the dialog to edit a server is accepted."""
        server = dialog.get_result()
        item = self._servers_table.selectedItems()[0]
        self._servers[item.row()] = server

        item.setText("%s:%d (%s)" % (server["host"], server["port"], server["authmode"]))
        item.setData(Qt.UserRole, server)
        item.setFlags(item.flags() & ~Qt.ItemIsEditable)

        checkbox = self._servers_table.item(item.row(), 1)
        state = Qt.Unchecked if server["no_ssl"] else Qt.Checked
        checkbox.setCheckState(state)
        self.update()

    def _reset(self, _):
        """Resets all the form elements to their default value."""
        ConfigManager().reset_config()
        config = ConfigManager()
        self._name_line_edit.setText(config.user.name)
        self._set_color(ida_color=config.user.color)

        navbar_checked = not config.cursors.navbar
        funcs_checked = not config.cursors.funcs
        disasm_checked = not config.cursors.disasm
        hexrays_checked = not config.cursors.hexrays
        all_checked = navbar_checked and funcs_checked and disasm_checked and hexrays_checked
        self._disable_all_cursors_checkbox.setChecked(all_checked)

        self._disable_navbar_cursors_checkbox.setChecked(navbar_checked)
        self._disable_navbar_cursors_checkbox.setEnabled(not all_checked)
        self._disable_funcs_cursors_checkbox.setChecked(funcs_checked)
        self._disable_funcs_cursors_checkbox.setEnabled(not all_checked)
        self._disable_disasm_cursors_checkbox.setChecked(disasm_checked)
        self._disable_disasm_cursors_checkbox.setEnabled(not all_checked)
        self._disable_hexrays_cursors_checkbox.setChecked(hexrays_checked)
        self._disable_hexrays_cursors_checkbox.setEnabled(not all_checked)

        checked = config.user.notifications
        self._notifications_checkbox.setChecked(checked)

        index = self._debug_level_combo_box.findData(config.level)
        self._debug_level_combo_box.setCurrentIndex(index)

        del self._servers[:]
        # delete everything except headers
        self._servers_table.setRowCount(0)
        self._keep_cnt_spin_box.setValue(config.keep.cnt)
        self._keep_intvl_spin_box.setValue(config.keep.intvl)
        self._keep_idle_spin_box.setValue(config.keep.idle)

    def _commit(self):
        """Commits all the changes made to the form elements."""
        name = self._name_line_edit.text()
        if ConfigManager().user.name != name:
            old_name = ConfigManager().user.name
            #NetworkManager().send_packet(UpdateUserName(old_name, name))
            NetworkManager().disconnect()
            ConfigManager().user.name = name

        if ConfigManager().user.color != self._color:
            name = ConfigManager().user.name
            old_color = ConfigManager().user.color
            packet = UpdateUserColor(name, old_color, self._color)
            NetworkManager().send_packet(packet)
            ConfigManager().user.color = self._color
            InterfaceManager().widget.refresh()

        all_ = self._disable_all_cursors_checkbox.isChecked()
        checked = self._disable_navbar_cursors_checkbox.isChecked()
        ConfigManager().cursors.navbar = not all_ and not checked
        checked = self._disable_funcs_cursors_checkbox.isChecked()
        ConfigManager().cursors.funcs = not all_ and not checked
        checked = self._disable_disasm_cursors_checkbox.isChecked()
        ConfigManager().cursors.disasm = not all_ and not checked
        checked = self._disable_hexrays_cursors_checkbox.isChecked()
        ConfigManager().cursors.hexrays = not all_ and not checked

        checked = self._notifications_checkbox.isChecked()
        ConfigManager().user.notifications = checked

        index = self._debug_level_combo_box.currentIndex()
        level = self._debug_level_combo_box.itemData(index)
        logging.getLogger(ROOT_LOGGER_NAME).setLevel(level)
        ConfigManager().level = level

        ConfigManager().servers = self._servers
        cnt = self._keep_cnt_spin_box.value()
        ConfigManager().keep.cnt = cnt
        intvl = self._keep_intvl_spin_box.value()
        ConfigManager().keep.intvl = intvl
        idle = self._keep_idle_spin_box.value()
        ConfigManager().keep.idle = idle
        if NetworkManager().client:
            NetworkManager().client.set_keep_alive(cnt, intvl, idle)

        ConfigManager().save_config()
