from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (
    QCheckBox,
    QDialog,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QComboBox, QFrame, QStackedWidget, QTextEdit)

from idarling.managers import *

import logging

from idarling.shared.authutils import calc_passhash

logger = logging.getLogger("idarling.interface.settings")

class ServerInfoDialog(QDialog):
    """The dialog shown when an user creates or edits a server."""

    def __init__(self, title, server=None):
        super(ServerInfoDialog, self).__init__()

        # General setup of the dialog
        logger.debug("Showing server info dialog")
        self.setWindowTitle(title)
        icon_path = ResourceManager().static_resource("settings.png")
        self.setWindowIcon(QIcon(icon_path))
        self.resize(100, 100)

        # Setup the layout and widgets

        self.mainlayout = QVBoxLayout(self)

        self.mainframe = mainframe = QWidget(self)
        layout = QVBoxLayout(mainframe)

        self._server_name_label = QLabel("<b>Server Host</b>")
        layout.addWidget(self._server_name_label)
        self._server_name = QLineEdit()
        self._server_name.setPlaceholderText("127.0.0.1")
        layout.addWidget(self._server_name)

        self._server_name_label = QLabel("<b>Server Port</b>")
        layout.addWidget(self._server_name_label)
        self._server_port = QLineEdit()
        self._server_port.setPlaceholderText("31013")
        layout.addWidget(self._server_port)

        self._no_ssl_checkbox = QCheckBox("Disable SSL")
        layout.addWidget(self._no_ssl_checkbox)

        self._authmode_label = QLabel("<b>Auth mode</b>")
        layout.addWidget(self._authmode_label)

        self._authmode_combo = QComboBox()
        self._authmode_combo.addItem("No Auth (none)", "none")
        self._authmode_combo.addItem("Password (chap)", "chap")
        self._authmode_combo.addItem("Public Key (pubkey)", "pubkey")
        layout.addWidget(self._authmode_combo)

        self.none_config_frame = QFrame(self)

        self.password_config_frame = QFrame(self)
        password_config_layout = QVBoxLayout(self.password_config_frame)
        self.password_label = QLabel("<b>Password</b>")
        password_config_layout.addWidget(self.password_label)
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.Password)
        password_config_layout.addWidget(self.password_input)
        self.password_config_frame.setLayout(password_config_layout)

        self.pubkey_config_frame = QFrame(self)
        pubkey_config_layout = QVBoxLayout(self.pubkey_config_frame)
        self._keytype_label = QLabel("<b>Key type</b>")
        pubkey_config_layout.addWidget(self._keytype_label)
        self._keytype_combo = QComboBox()
        self._keytype_combo.addItem("RSA", "ssh-rsa")
        self._keytype_combo.addItem("DSS", "ssh-dss")
        self._keytype_combo.addItem("ECDSA", "ecdsa-sha2-nistp256")
        self._keytype_combo.addItem("ED25519", "ssh-ed25519")
        self._keyblob_input = QTextEdit()
        pubkey_config_layout.addWidget(self._keytype_combo)
        pubkey_config_layout.addWidget(self._keyblob_input)
        self.pubkey_config_frame.setLayout(pubkey_config_layout)

        self.frames = [self.none_config_frame, self.password_config_frame, self.pubkey_config_frame]

        self._authmode_combo.currentIndexChanged.connect(self.on_authmode_change)

        self.down_side = down_side = QWidget(self)
        buttons_layout = QHBoxLayout(down_side)
        self._add_button = QPushButton("OK")
        self._add_button.clicked.connect(self.accept)
        buttons_layout.addWidget(self._add_button)
        self._cancel_button = QPushButton("Cancel")
        self._cancel_button.clicked.connect(self.reject)
        buttons_layout.addWidget(self._cancel_button)

        self.mainlayout.addWidget(mainframe)
        self.mainlayout.addWidget(self.none_config_frame)
        self.mainlayout.addWidget(down_side)
        self.setLayout(self.mainlayout)

        # Set the form elements values if we have a base
        if server is not None:
            self.set_server(server)

    def on_authmode_change(self):
        while self.mainlayout.takeAt(0):
            pass
        for frame in self.frames:
            frame.setVisible(False)
        curFrame = self.frames[self._authmode_combo.currentIndex()]
        curFrame.setVisible(True)
        self.mainlayout.addWidget(self.mainframe)
        self.mainlayout.addWidget(curFrame)
        self.mainlayout.addWidget(self.down_side)

    def set_server(self, server):
        self._server_name.setText(server["host"])
        self._server_port.setText(str(server["port"]))
        self._no_ssl_checkbox.setChecked(server["no_ssl"])
        authmode = server["authmode"]
        authargs = server["authargs"]
        authdict = {self._authmode_combo.itemData(i): i for i in range(self._authmode_combo.count())}
        self._authmode_combo.setCurrentIndex(authdict[authmode])

        if authmode == "none":
            authargs = []
            pass
        elif authmode == "chap":
            #authargs = [self.password_input.text()]
            self.password_input.setText(authargs[0])
        elif authmode == "pubkey":
            #authargs = [self._keytype_combo.currentData(), self._keyblob_input.toPlainText()]
            keytypedict = {self._keytype_combo.itemData(i): i for i in range(self._keytype_combo.count())}
            self._keytype_combo.setCurrentIndex(keytypedict[authargs[0]])
            self._keyblob_input.setPlainText(authargs[1])
        else:
            #authargs = []
            pass

        self.on_authmode_change()

    def get_result(self):
        """Get the server resulting from the form elements values."""
        authmode = self._authmode_combo.currentData()
        if authmode == "none":
            authargs = []
        elif authmode == "chap":
            authargs = [calc_passhash(self.password_input.text())]
        elif authmode == "pubkey":
            authargs = [self._keytype_combo.currentData(), self._keyblob_input.toPlainText()]
        else:
            authargs = []
        return {
            "host": self._server_name.text() or "127.0.0.1",
            "port": int(self._server_port.text() or "31013"),
            "no_ssl": self._no_ssl_checkbox.isChecked(),
            "authmode": authmode,
            "authargs": authargs
        }
