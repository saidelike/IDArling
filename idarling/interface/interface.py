# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import time

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import qApp, QMainWindow

from idarling.managers import *
from idarling.shared.utils import Singleton

from ..module import Module

import logging
logger = logging.getLogger("idarling.interface.manager")

@Singleton
class InterfaceManager(Module):
    """
    This is the interface module. It is responsible for all interactions with
    the user interface. It manages the all the actions, dialog, cursors,
    invites and the handy status bar widget.
    """

    def __init__(self):
        #super(InterfaceManager, self).__init__()
        Module.__init__(self)

        from idarling.interface.chat.context import ContextMenuEventFilter
        from idarling.interface.chat.tooltip import TooltipEventFilter
        from idarling.interface.misc.abouticon import AboutIconEventFilter
        from idarling.interface.project.open import OpenAction
        from idarling.interface.project.save import SaveAction
        from .painter import Painter
        from idarling.interface.statuswidget import StatusWidget


        self._invites = []
        self._followed = None

        # Find the QMainWindow instance
        logger.debug("Searching for the main window")
        for widget in qApp.topLevelWidgets():
            if isinstance(widget, QMainWindow):
                self._window = widget
                break

        self._actions = [OpenAction(), SaveAction()]

        self._painter = Painter()
        self._filters = [AboutIconEventFilter(), TooltipEventFilter(), ContextMenuEventFilter()]
        self._widget = StatusWidget()

    @property
    def widget(self):
        return self._widget

    @property
    def painter(self):
        return self._painter

    @property
    def invites(self):
        """Get all active invites."""
        invites = []
        for invite in self._invites:
            # Check if still active
            if (
                invite.callback
                and not invite.triggered
                and time.time() - invite.time < 180.0
            ):
                invites.append(invite)
        return invites

    @property
    def followed(self):
        return self._followed

    @followed.setter
    def followed(self, followed):
        self._followed = followed

    def _install(self):
        for action in self._actions:
            action.install()
        for f in self._filters:
            f.install()
        self._widget.install(self._window)
        return True

    def _uninstall(self):
        for action in self._actions:
            action.uninstall()
        for f in self._filters:
            f.uninstall()
        self._widget.uninstall(self._window)
        return True

    def update(self):
        """Update the actions and widget."""
        if not NetworkManager().connected:
            self.clear_invites()

        for action in self._actions:
            action.update()
        self._widget.refresh()

    def show_invite(self, text, icon, callback=None):
        """
        Display a toast notification to the user. The notification will have
        the specified text, icon and callback function (triggered on click).
        """
        from idarling.interface.chat.invites import InviteWidget

        # Check if notifications aren't disabled
        if not ConfigManager().user.notifications:
            return

        invite = InviteWidget(self._window)
        invite.time = time.time()
        invite.text = text
        invite.icon = QPixmap(icon)
        invite.callback = callback
        invite.show()
        self._invites.append(invite)

    def clear_invites(self):
        """Clears the invites list."""
        del self._invites[:]
        self._widget.refresh()
