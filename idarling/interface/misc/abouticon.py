# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtCore import QObject, Qt  # noqa: I202
from PyQt5.QtGui import QPixmap, QShowEvent
from PyQt5.QtWidgets import (
    QDialog,
    QGroupBox,
    QLabel,
)

from idarling.managers import *
from idarling.interface.base.filter import EventFilter


class AboutIconEventFilter(EventFilter):
    """
    This Qt event filter is used to replace the IDA icon with our
    own and to setup the invites context menu in the disassembler view.
    """

    DESC = "AboutIcon"

    def __init__(self, parent=None):
        super(AboutIconEventFilter, self).__init__(parent)

    def _replace_icon(self, label):
        pixmap = QPixmap(ResourceManager().static_resource("idarling.png"))
        pixmap = pixmap.scaled(
            label.sizeHint().width(),
            label.sizeHint().height(),
            Qt.KeepAspectRatio,
            Qt.SmoothTransformation,
        )
        label.setPixmap(pixmap)

    def eventFilter(self, obj, ev):  # noqa: N802
        # Is it a QShowEvent on a QDialog named "Dialog"?
        if (
                ev.__class__ == ev,
                QShowEvent
                and obj.__class__ == QDialog
                and obj.windowTitle() == "About",
        ):
            # Find a child QGroupBox
            for groupBox in obj.children():
                if groupBox.__class__ == QGroupBox:
                    # Find a child QLabel with an icon
                    for label in groupBox.children():
                        if isinstance(label, QLabel) and label.pixmap():
                            self._replace_icon(label)

        return False
