# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import ida_kernwin

from PyQt5.QtGui import QContextMenuEvent, QIcon, QImage, QPixmap, QShowEvent
from PyQt5.QtWidgets import (
    QAction,
    QMenu,
    QWidget,
)

from idarling.managers import *

from idarling.interface.base.filter import EventFilter
from idarling.interface.statuswidget import StatusWidget
from idarling.core.user.usercommands import *


class ContextMenuEventFilter(EventFilter):
    """
    This Qt event filter is used to replace the IDA icon with our
    own and to setup the invites context menu in the disassembler view.
    """

    DESC = "ContextMenu"

    def __init__(self=None):
        super(ContextMenuEventFilter, self).__init__()

    def _insert_menu(self, obj):
        # Find where to install our submenu
        sep = None
        for act in obj.actions():
            if act.isSeparator():
                sep = act
            if "Undefine" in act.text():
                break
        obj.insertSeparator(sep)

        # Setup our custom menu text and icon
        menu = QMenu("Invite to location", obj)
        pixmap = QPixmap(ResourceManager().static_resource("invite.png"))
        menu.setIcon(QIcon(pixmap))

        # Setup our first submenu entry text and icon
        everyone = QAction("Everyone", menu)
        pixmap = QPixmap(ResourceManager().static_resource("users.png"))
        everyone.setIcon(QIcon(pixmap))

        def invite_to(name):
            """Send an invitation to the current location."""
            loc = ida_kernwin.get_screen_ea()
            packet = InviteToLocation(name, loc)
            NetworkManager().send_packet(packet)

        # Handler for when the action is clicked
        def invite_to_everyone():
            invite_to("everyone")

        everyone.triggered.connect(invite_to_everyone)
        menu.addAction(everyone)

        menu.addSeparator()
        template = QImage(ResourceManager().static_resource("user.png"))

        def create_action(name, color):
            action = QAction(name, menu)
            pixmap = StatusWidget.make_icon(template, color)
            action.setIcon(QIcon(pixmap))

            # Handler for when the action is clicked
            def invite_to_user():
                invite_to(name)

            action.triggered.connect(invite_to_user)
            return action

        # Insert an action for each connected user
        for name, user in UserManager().get_users().items():
            menu.addAction(create_action(name, user["color"]))
        obj.insertMenu(sep, menu)

    def eventFilter(self, obj, ev):  # noqa: N802
        # Is it a QContextMenuEvent on a QWidget?
        if isinstance(obj, QWidget) and isinstance(ev, QContextMenuEvent):
            # Find a parent titled "IDA View"
            parent = obj
            while parent:
                if parent.windowTitle().startswith("IDA View"):
                    # Intercept the next context menu
                    self._intercept = True
                parent = parent.parent()

        # Is it a QShowEvent on a QMenu?
        if isinstance(obj, QMenu) and isinstance(ev, QShowEvent):
            # Should we intercept?
            if self._intercept:
                self._insert_menu(obj)
                self._intercept = False
        return False
