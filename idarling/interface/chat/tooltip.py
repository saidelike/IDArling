# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import ida_funcs

from PyQt5.QtCore import QEvent, QObject, Qt  # noqa: I202
from PyQt5.QtWidgets import (
    QTableView,
    QWidget,
)

from idarling.managers import *

from idarling.interface.base.filter import EventFilter

import logging

logger = logging.getLogger("idarling.interface.chat")


class TooltipEventFilter(EventFilter):
    """
    This Qt event filter is used to replace the IDA icon with our
    own and to setup the invites context menu in the disassembler view.
    """
    DESC = "Tooltip"

    def __init__(self, parent=None):
        super(EventFilter, self).__init__(parent)

    def _set_tooltip(self, obj, ev):
        if not ConfigManager().cursors.funcs:
            return

        obj.setToolTip("")
        index = obj.parent().indexAt(ev.pos())
        row = index.row()
        if row == -1:
            return

        func_ea = int(index.sibling(index.row(), 2).data(), 16)
        func = ida_funcs.get_func(func_ea)

        # Find the corresponding username
        for name, user in UserManager().get_users().items():
            if ida_funcs.func_contains(func, user["ea"]):
                # Set the tooltip
                obj.setToolTip(name)
                break

    def eventFilter(self, obj, ev):  # noqa: N802
        # Is it a ToolTip event on a QWidget with a parent?
        if (
            ev.type() == QEvent.ToolTip
            and obj.__class__ == QWidget
            and obj.parent()
        ):
            table_view = obj.parent()
            # Is it a QTableView with a parent?
            if table_view.__class__ == QTableView and table_view.parent():
                func_window = table_view.parent()
                # Is it a QWidget titled "Functions window"?
                if (
                    func_window.__class__ == QWidget
                    and func_window.windowTitle() == "Functions window"
                ):
                    self._set_tooltip(obj, ev)

        return False
