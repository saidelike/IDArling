# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtCore import QObject, Qt  # noqa: I202
from PyQt5.QtWidgets import (
    qApp,
)

import logging
logger = logging.getLogger("idarling.interface.base.filter")

class EventFilter(QObject, object):
    """
    This Qt event filter is used to replace the IDA icon with our
    own and to setup the invites context menu in the disassembler view.
    """

    DESC = "Base"

    def __init__(self, parent=None):
        super(EventFilter, self).__init__(parent)
        self._intercept = False

    def install(self):
        logger.debug("Installing the event filter %s" % self.DESC)
        qApp.instance().installEventFilter(self)

    def uninstall(self):
        logger.debug("Uninstalling the event filter %s" % self.DESC)
        qApp.instance().removeEventFilter(self)

    def eventFilter(self, obj, ev):  # noqa: N802
        raise NotImplementedError("eventFilter not implement")
