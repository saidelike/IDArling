from functools import partial

import ida_idaapi

from idarling.managers import *
import ida_kernwin

import logging
logger = logging.getLogger("idarling.interface.base.action")

class Action(object):
    """
    An action is attached to a specific menu, has a custom text, icon, tooltip
    and finally a handler that is called when it is clicked by the user.
    """

    _ACTION_ID = None

    def __init__(self, menu, text, tooltip, icon, handler):
        super(Action, self).__init__()

        self._menu = menu
        self._text = text
        self._tooltip = tooltip
        self._icon = icon
        self._icon_id = ida_idaapi.BADADDR
        self._handler = handler

    @property
    def handler(self):
        return self._handler

    def install(self):
        action_name = self.__class__.__name__

        # Read and load the icon file
        icon_data = str(open(self._icon, "rb").read())
        self._icon_id = ida_kernwin.load_custom_icon(data=icon_data)

        # Create the action descriptor
        action_desc = ida_kernwin.action_desc_t(
            self._ACTION_ID,
            self._text,
            self._handler,
            None,
            self._tooltip,
            self._icon_id,
        )

        # Register the action using its descriptor
        result = ida_kernwin.register_action(action_desc)
        if not result:
            raise RuntimeError("Failed to register action %s" % action_name)

        # Attach the action to the chosen menu
        result = ida_kernwin.attach_action_to_menu(
            self._menu, self._ACTION_ID, ida_kernwin.SETMENU_APP
        )
        if not result:
            action_name = self.__class__.__name__
            raise RuntimeError("Failed to install action %s" % action_name)

        logger.debug("Installed action %s" % action_name)
        return True

    def uninstall(self):
        action_name = self.__class__.__name__

        # Detach the action from the chosen menu
        result = ida_kernwin.detach_action_from_menu(
            self._menu, self._ACTION_ID
        )
        if not result:
            return False

        # Un-register the action using its id
        result = ida_kernwin.unregister_action(self._ACTION_ID)
        if not result:
            return False

        # Free the custom icon using its id
        ida_kernwin.free_custom_icon(self._icon_id)
        self._icon_id = ida_idaapi.BADADDR

        logger.debug("Uninstalled action %s" % action_name)
        return True

    def update(self):
        """Check if the action should be enabled or not."""
        ida_kernwin.update_action_state(
            self._ACTION_ID, self._handler.update(None)
        )


class ActionHandler(ida_kernwin.action_handler_t):
    """An action handler will display one of the dialogs to the user."""

    _DIALOG = None

    @staticmethod
    def _on_progress(progress, count, total):
        """Called when some progress has been made."""
        progress.setRange(0, total)
        progress.setValue(count)

    def __init__(self):
        super(ActionHandler, self).__init__()

    def update(self, ctx):
        """Update the state of the associated action."""
        if NetworkManager().connected:
            return ida_kernwin.AST_ENABLE
        return ida_kernwin.AST_DISABLE

    def activate(self, ctx):
        """Called when the action is clicked by the user."""
        dialog_name = self._DIALOG.__name__
        logger.debug("Showing dialog %s" % dialog_name)
        dialog = self._DIALOG()
        dialog.accepted.connect(partial(self._dialog_accepted, dialog))
        dialog.exec_()
        return 1

    def _dialog_accepted(self, dialog):
        """Called when the dialog is accepted by the user."""
        raise NotImplementedError("dialog_accepted() not implemented")
