from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QTableWidget,
    QTableWidgetItem,
)
from idarling.managers import *

class DatabaseTableItem(QTableWidgetItem):
    def __init__(self, database, text):
        super(DatabaseTableItem, self).__init__(text)
        self.database = database
        self.setFlags(self.flags() & ~Qt.ItemIsEditable)


class DatabaseTable(QTableWidget):
    def __init__(self, parent):
        super(DatabaseTable, self).__init__(0, 4, parent)
        horizontal_header = self.horizontalHeader()
        horizontal_header.setSectionsClickable(False)
        horizontal_header.setSectionResizeMode(0, horizontal_header.Stretch)
        self.verticalHeader().setVisible(False)
        self.setSelectionBehavior(QTableWidget.SelectRows)
        self.setSelectionMode(QTableWidget.SingleSelection)
        self.project = None

    def add_item(self, i, database):
        labels = ("Name", "id", "Date", "Ticks")
        self.setHorizontalHeaderLabels(labels)
        self.setItem(
            i, 0, DatabaseTableItem(database, database.name)
        )
        self.setItem(
            i, 1, DatabaseTableItem(database, database.id)
        )
        self.setItem(i, 2, DatabaseTableItem(database, database.date))
        tick = str(database.tick) if database.tick != -1 else "<none>"
        self.setItem(i, 3, DatabaseTableItem(database, tick))

    def refresh(self, newproj=None):
        if newproj:
            self.project = newproj # type: Project
        if not self.project:
            self.clear()
            return

        self.clear()
        def listed(dbs):
            if not newproj:
                # we're not on a project, ignore
                return
            req_proj = newproj.nodeid
            if self.project.nodeid != req_proj:
                # Wrong order of packet, deny
                return
            '''
            self.form3.rename_database_btn.setEnabled(False)
            self.form3.upload_database_btn.setEnabled(False)
            '''
            self.setRowCount(len(dbs))
            for i, database in enumerate(dbs):
                self.add_item(i, database)

        self.project.list_database(cb=listed)

    def _rename_database_clicked(self):
        """Called when the rename database button is clicked."""
        '''
        database = self.form3._IDB_table.selectedItems()[0].data(Qt.UserRole)
        dialog = RenamedDatabaseDialog()
        dialog.accepted.connect(partial(self._renamed_database_accepted, dialog, database))
        dialog.exec_()'''
        pass

    def _renamed_database_accepted(self, dialog, database):
        self.newName = dialog.get_result()
        # Database(databaseID=20190805203947, name=dcdc, state=ok, projectID=20190728145615,
        # projectPathWithoutID=ServerRoot\test1\PRJ2, comments=, flag=ok, date=2019/08/05 20:39, tick=0)
        databasePath = database.projectPathWithoutID + "$id" + \
                       database.projectID + "\\" + database.name + "$idb" + database.databaseID
        ProjectManager().renamed_database(databasePath, database.databaseID, \
                                          self.newName, cb=self._database_renamed)

