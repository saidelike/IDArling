import datetime
import os
from functools import partial

from PyQt5.QtCore import Qt, QRegExp, QTimer  # noqa: I202
from PyQt5.QtGui import QIcon, QRegExpValidator
from PyQt5.QtWidgets import (
    QDialog,
    QHBoxLayout,
    QLabel,
    QMessageBox,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QLineEdit, QTreeWidgetItem, QTreeWidget, QAction, QMenu)

from idarling.core.project.project import TreeNode, Project
from idarling.managers import *
from idarling.shared.proto import NODETYPE_PROJECT, NODETYPE_DIRECTORY
from idarling.shared.utils import explode_path


class TreeItemWrapper(QTreeWidgetItem):
    """Adding attributes to tree nodes"""
    def __init__(self, node):
        super(TreeItemWrapper, self).__init__()
        self.node = node

class InputDialog(QDialog):
    def __init__(self, title, icon, prompt, placeholder, maxlen, *args, **kwargs):  # plugin
        super(InputDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle(title)

        #self.setWindowIcon(QIcon(icon_path))
        self.setWindowIcon(icon)
        self.resize(100, 100)

        # Set up the layout and widgets
        layout = QVBoxLayout(self)

        self._nameLabel = QLabel("<b>%s</b>" % prompt) # Directory Name
        layout.addWidget(self._nameLabel)
        self._inputEdit = QLineEdit()
        self._inputEdit.setValidator(QRegExpValidator(QRegExp("[a-zA-Z0-9-]+")))
        self._inputEdit.setPlaceholderText(placeholder) #'Please enter directory name'
        self._inputEdit.setMaxLength(maxlen)  # length of name <= 30
        self._inputEdit.setClearButtonEnabled(True)  # set clear content action
        layout.addWidget(self._inputEdit)

        buttons = QWidget(self)
        buttons_layout = QHBoxLayout(buttons)
        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.accept)
        buttons_layout.addWidget(self.ok_button)
        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.reject)
        buttons_layout.addWidget(self.cancel_button)
        layout.addWidget(buttons)

    def get_result(self):
        """Get the name entered by the user."""
        return self._inputEdit.text()

class ProjectTreeWidget(QTreeWidget):
    def __init__(self, *args, **kwargs):
        super(ProjectTreeWidget, self).__init__(*args, **kwargs)

        '''
        self.mkdirFun = mkdirFun
        self.renameFun = renameFun
        self.moveFun = moveFun
        self.deleteFun = deleteFun
        '''

        icon_path = ResourceManager().static_resource("prj.ico")
        self.prjicon = QIcon(icon_path)
        icon_path = ResourceManager().static_resource("dir.ico")
        self.diricon = QIcon(icon_path)

        self.setColumnCount(1)
        self.setHeaderLabels(['Remote Projects'])

        self.setContextMenuPolicy(Qt.CustomContextMenu) # handle custom right click
        self.customContextMenuRequested.connect(self.contextMenuEvent)
        self.itemDoubleClicked.connect(self.itemDoubleClickedEvent)

    def _build_item(self, node):
        node = node  # type: Project
        item = TreeItemWrapper(node)
        if node.nodetype == NODETYPE_DIRECTORY:
            item.setText(0, node.get_name())
            item.setIcon(0, self.diricon)
        elif node.nodetype == NODETYPE_PROJECT:
            item.setText(0, "%s (%s)" % (node.get_name(), node.nodeid))
            icon_path = ResourceManager().static_resource("prj.ico")
            item.setIcon(0, self.prjicon)
        return item

    def build_tree(self, rootnode, from_node=None):  # plugin
        # self_tree: self._tree
        # It's just that the class hasn't been instantiated yet.
        if not from_node:
            root = self._build_item(rootnode)
            root.setText(0, "Server root")
        else:
            root = from_node

        def process(curnode, parent_item):
            """
            :param TreeNode curnode:
            :param TreeItemWrapper parent_item:
            :return:
            """
            # Recursive traversal to generate directory tree
            # j is directory
            curnode = curnode  # type: TreeNode
            for node in curnode.subnodes:
                newitem = self._build_item(node)
                #if parent_item == root and not from_node:  # first-level items in fresh build
                #    newitem.setText(0, node.nodepath.lstrip('/'))
                parent_item.addChild(newitem)
                process(node, newitem)  # plugin

        process(rootnode, root)  # plugin
        return root

    def refresh_root(self, rootnode):
        root_item = self.build_tree(rootnode)
        self.addTopLevelItem(root_item)
        self.expandAll()

    def rebuild_item(self, node, item):
        """
        :param TreeNode node:
        :param TreeItemWrapper item:
        :return:
        """
        item.takeChildren() # remove all childs
        self.build_tree(node, item) # re-insert all childs

    def refresh_item(self, item):
        def listed(reply):
            self.rebuild_item(item.node, item)
        item.node.list_tree(cb=listed)

    def itemDoubleClickedEvent(self, item, column):
        if item.node.nodetype == NODETYPE_PROJECT:
            return
        if item.childCount() == 0:
            self.refresh_item(item)

    def contextMenuEvent(self, point):
        # right clicked to pop menu for creating the directory
        item = self.currentItem()
        if item == None:
            #QMessageBox.information(self, 'Warning info', 'Please select the directory')
            return

        treeMenu = QMenu(self)
        if item.node.nodetype == NODETYPE_DIRECTORY:
            mkdirAction = QAction('Create directory...', self)
            mkdirAction.triggered.connect(partial(self.mkdirFun, item))
            treeMenu.addAction(mkdirAction)

        renamedAction = QAction('Rename...', self)
        treeMenu.addAction(renamedAction)
        renamedAction.triggered.connect(partial(self.renameFun, item))

        '''
        deleteAction = QAction('Delete...', self)
        treeMenu.addAction(deleteAction)
        deleteAction.triggered.connect(partial(self.deleteFun, item))
        '''
        treeMenu.popup(self.mapToGlobal(point))


    def get_item_at_path(self, path):
        folders = explode_path(path)
        curItem = self.topLevelItem(0)
        curLevel = 0
        while True:
            if curLevel == len(folders):
                break
            childCount = curItem.childCount()
            for i in range(childCount):
                c = curItem.child(i)
                if c.node.get_name() == folders[curLevel]:
                    curItem = c
                    curLevel += 1
                    break
            else:
                return None  # failed to find target item

        return curItem

    def expandToNode(self, targetnode):
        def handleCb(node):
            # all path listed
            self.refresh_root(ProjectManager().get_root_node())
            self.expandAll()
            targetitem = self.get_item_at_path(targetnode.nodepath)
            if targetitem:
                self.setCurrentItem(targetitem)

        self.clear()
        ProjectManager().get_node_at_path(targetnode.nodepath, handleCb)

    def mkdirFun(self, item):
        icon_path = ResourceManager().static_resource("upload.png")
        icon = QIcon(icon_path)
        dialog = InputDialog("Creating Directory", icon, "Please enter directory name", "New directory name", 50)

        def accepted():
            """Called when the directory creation dialog is accepted."""
            name = dialog.get_result()
            item.node.add_directory(name, cb=_directory_created)

        def _directory_created(reply):
            """Called when the reply of dir_create is received."""
            if not reply.newnode:
                if not reply.err:
                    QMessageBox.warning(self, 'Error', 'Failed to create the directory, please refresh and try again.')
                else:
                    QMessageBox.warning(self, 'Error', 'Failed to create the directory: %s' % reply.err)
            else:
                self.expandToNode(reply.newnode)

        dialog.accepted.connect(accepted)
        dialog.exec_()
        pass

    def renameFun(self, item):
        icon_path = ResourceManager().static_resource("upload.png")
        icon = QIcon(icon_path)
        dialog = InputDialog("Renaming", icon, "Please enter new name", "New name", 50)

        def _renamed(reply):
            if reply.newnode:
                QMessageBox.information(self, 'Renamed info', 'Rename success')
                self.expandToNode(reply.newnode)
            else:
                QMessageBox.information(self, 'Renamed info', 'Failed to rename the item, please refresh and try again')

        def accepted():
            """Called when the directory creation dialog is accepted."""
            name = dialog.get_result()
            item.node.rename(name, cb=_renamed)

        dialog.accepted.connect(accepted)
        dialog.exec_()
        pass

    def deleteFun(self, item):
        def _deleted(reply):
            QMessageBox.information(self, 'Deleting', 'Delete success')

        dialog = QMessageBox(QMessageBox.Warning, "Deleting",
                             "Are you sure want to delete %s?" % item.node.nodepath,
                             QMessageBox.Ok | QMessageBox.Cancel,
                             self)
        dialog.exec_()
        if dialog.result() == QMessageBox.Ok:
            item.node.delete(cb=self._deleted)


class TreeFrame(QWidget):
    def __init__(self, dialog):
        super(TreeFrame, self).__init__()
        self._dialog = dialog

        self.qvl = QVBoxLayout(self)

        self._tree = ProjectTreeWidget()
        # self._tree.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self._tree.setColumnWidth(0, 150)
        self._tree.header().setMinimumSectionSize(500)
        # treeWidget -> header() -> setMinimumSectionSize(500)
        self._tree.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self._update_tree_btn = QPushButton('Refresh Tree')
        self._update_tree_btn.clicked.connect(self._update_tree_clicked)  # accept

        # Update Tree First
        self._update_tree_clicked()

        ###add the scroll
        # self.scroll = QScrollArea(self)
        # self.scroll.setAutoFillBackground(True)
        # self.scroll.setWidgetResizable(True)
        # self.qvl.addWidget(self.scroll)
        ###
        self.qvl.addWidget(self._tree)
        self.qvl.addWidget(self._update_tree_btn)
        self._tree.currentItemChanged.connect(self._dialog.currentItemChanged)
        self._ItemList = []
        self._ItemPath = []
        # need to create dirTree

    def _update_tree_clicked(self):
        self._update_tree_btn.setEnabled(False)
        self._tree.clear()
        rootNode = ProjectManager().get_root_node()

        def _tree_listed(reply):
            """Called when the  tree info  is received."""
            self._tree.refresh_root(rootNode)
            self._update_tree_btn.setEnabled(True)
        rootNode.list_tree(cb=_tree_listed)