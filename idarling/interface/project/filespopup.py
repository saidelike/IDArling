import ida_loader
from PyQt5.QtCore import Qt  # noqa: I202
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (
    QMessageBox,
    QPushButton,
    QProgressDialog)

from idarling.managers import *
from idarling.core.project.project import Project

class FilePopup(object):
    def __init__(self, window_title, window_text, icon_path, callback):
        # Create the upload progress dialog
        self.icon = icon_path
        self.fin_callback = callback
        self.progress = QProgressDialog(window_text, "Cancel", 0, 1)
        self.progress.setCancelButton(None)  # Remove cancel button
        self.progress.setModal(True)  # Set as a modal dialog
        window_flags = self.progress.windowFlags()  # Disable close button
        self.progress.setWindowFlags(window_flags & ~Qt.WindowCloseButtonHint)
        self.progress.setWindowTitle(window_title)
        self.progress.setWindowIcon(QIcon(self.icon))

    def _on_progress(self, count, total):
        """Called when some progress has been made."""
        self.progress.setRange(0, total)
        self.progress.setValue(count)

    def callback(self, reply):
        self.progress.close()
        self.fin_callback(reply)

    def errback(self, reply):
        QMessageBox.warning(self, 'Error', 'Network error occurred during the transmission. Please check your network and try again.')
        self.progress.close()
        self.fin_callback(None)

class UploadPopup(FilePopup):
    """The action handler for the "Save to server..." action."""

    def __init__(self, project, database_id, fin_callback):
        # type: (Project, str, object) -> None
        self.project = project
        self.database_id = database_id
        icon_path = ResourceManager().static_resource("upload.png")
        super(UploadPopup, self).__init__(
            "Save to server",
            "Uploading database to server, please wait...",
            icon_path,
            fin_callback
        )

    def execute(self):
        # type: () -> None
        # Save the current database
        #upload_data = self.project.get_database_data()
        # Send the packet to upload the file
        self.project.upload_database(self.database_id, #upload_data,
                                upback=self._on_progress,
                                callback=self.callback, errback=self.errback)
        self.progress.show()

class DownloadPopup(FilePopup):
    def __init__(self, project, database_id, fin_callback):
        # type: (Project, str, None) -> None
        self.project = project
        self.database_id = database_id
        icon_path = ResourceManager().static_resource("download.png")
        super(DownloadPopup, self).__init__(
            "Open from server",
            "Downloading database from server, please wait...",
            icon_path,
            fin_callback
        )

    def execute(self):
        # type: () -> None
        self.project.download_database(self.database_id,
                                     downback=self._on_progress,
                                     callback=self.callback)
        self.progress.show()