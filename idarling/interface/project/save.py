# coding=utf-8
import datetime
from functools import partial

import ida_auto
import ida_kernwin
import ida_loader
import ida_nalt

from PyQt5.QtCore import Qt, QRegExp, QCoreApplication  # noqa: I202
from PyQt5.QtGui import QIcon, QRegExpValidator, QPixmap, QFont
from PyQt5.QtWidgets import (
    QMessageBox,
    QPushButton,
    QProgressDialog, QWidget, QVBoxLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit, QTableWidget, QRadioButton,
    QTextEdit, QGridLayout)

from idarling.interface.project.dbtable import DatabaseTable
from idarling.interface.project.filespopup import UploadPopup
from idarling.interface.project.projdialog import ProjectListDialog
from idarling.interface.project.projtree import TreeItemWrapper
from idarling.managers import *

from idarling.core.project.projectcommands import *
from idarling.interface.base.action import Action, ActionHandler
from idarling.interface.project.create import CreateProjectDialog, CreateDatabaseDialog
from idarling.interface.project.open import OpenDialog
from idarling.shared.proto import *
from idarling.core.project.project import Project

import logging
logger = logging.getLogger("idarling.interface.project")

# Class handling project create
class ProjCreateFrame(QWidget):
    def __init__(self, dialog):
        super(ProjCreateFrame, self).__init__()
        self._dialog = dialog
        self.qvl = QVBoxLayout(self)

        self.project_creation = QGroupBox("Project Creation", self)
        self.project_creation_layout = QVBoxLayout(self.project_creation)

        self.projname_layout = QHBoxLayout()
        self.projname_label = QLabel("Project Name:")
        self.projname_lineEdit = QLineEdit()
        self.projname_lineEdit.setMaxLength(100)
        self.projname_lineEdit.setValidator(QRegExpValidator(QRegExp("[a-zA-Z0-9-]+")))
        self.projname_lineEdit.setClearButtonEnabled(True)
        self.projname_lineEdit.setPlaceholderText('Please edit the project name')
        self.projname_layout.addWidget(self.projname_label)
        self.projname_layout.addWidget(self.projname_lineEdit)
        self.project_creation_layout.addLayout(self.projname_layout)

        self.projpath_layout = QHBoxLayout()
        self.projpath_label = QLabel()
        self.projpath_label.setText("Project Path:")
        self.projpath_lineEdit = QLineEdit()
        self.projpath_lineEdit.setPlaceholderText('No directory selected')
        self.projpath_lineEdit.setReadOnly(True)

        self.projpath_layout.addWidget(self.projpath_label)
        self.projpath_layout.addWidget(self.projpath_lineEdit)
        self.project_creation_layout.addLayout(self.projpath_layout)

        self.projhash_layout = QHBoxLayout()
        self.projhash_label = QLabel()
        self.projhash_label.setText("Project Hash:")
        self.projhash_lineEdit = QLineEdit()
        self.projhash_lineEdit.setPlaceholderText('Reserved')
        self.projhash_lineEdit.setReadOnly(True)
        # hash = ida_nalt.retrieve_input_file_md5().lower()
        # self.projhash_lineEdit.setText(hash)
        self.projhash_layout.addWidget(self.projhash_label)
        self.projhash_layout.addWidget(self.projhash_lineEdit)
        self.project_creation_layout.addLayout(self.projhash_layout)

        self.project_comments = QGroupBox("Project Comments:", self.project_creation)
        self.project_comments_layout = QVBoxLayout(self.project_comments)
        self.project_comments_text = QTextEdit('')
        self.project_comments_layout.addWidget(self.project_comments_text)
        self.project_creation_layout.addWidget(self.project_comments)

        '''
        self.availability = QGroupBox("Availability", self.project_creation)
        self.availability_hvl = QHBoxLayout(self.availability)
        self.rbPublic = QRadioButton('Public', self.availability)
        self.rbPrivate = QRadioButton('Private', self.availability)
        self.rbReserved = QRadioButton('Reserved', self.availability)
        self.availability_hvl.addWidget(self.rbPublic)
        self.availability_hvl.addWidget(self.rbPrivate)
        self.availability_hvl.addWidget(self.rbReserved)
        self.project_creation_layout.addWidget(self.availability)
        '''

        self.buttons = QWidget(self)
        self.buttons_layout = QHBoxLayout(self.buttons)
        self.project_creation_btn = QPushButton('Create Project', self.buttons)
        self.project_creation_btn.clicked.connect(self.create_project)  ### self.create_project 0609
        # self.qw2_cancel_btn = QPushButton('Cancle', self.qw2)   ###how2 set the slot function
        self.buttons_layout.addWidget(self.project_creation_btn)
        # self.qw2_layout.addWidget(self.qw2_cancel_btn)

        self.qvl.addWidget(self.project_creation)
        self.qvl.addWidget(self.buttons)

    def create_project(self):
        project_name = self.projname_lineEdit.text()
        project_path = self.projpath_lineEdit.text()
        if not project_name:
            QMessageBox.information(self, 'Warning info', 'Please input the project name')
            return
        if not project_path:
            QMessageBox.information(self, 'Warning info', 'Please select the project path')
            return

        curItem = self._dialog.treeFrame._tree.currentItem()
        if (curItem.node.nodetype != NODETYPE_DIRECTORY):
            QMessageBox.information(self, 'Warning info', 'Please select a directory on the left panel')
            return

        # Get all the information we need and sent it to the server
        hash = ida_nalt.retrieve_input_file_md5().lower()
        file = ida_nalt.get_root_filename()
        type = ida_loader.get_file_type_name()
        curItem.node.add_project(project_name, file, hash, type, self._project_created)
        #ProjectManager().add_project(project_path + '/' + project_name, file, hash, type, self._project_created)

    def _project_created(self, reply):  # (self, project,reply) argv[2] is _ infact is reply
        """Called when the create project reply is received."""
        newnode = reply.newnode
        if newnode: # successfully created
            curItem = self._dialog.treeFrame._tree.currentItem()  # type: TreeItemWrapper
            newItem = self._dialog.treeFrame._tree._build_item(newnode)
            curItem.addChild(newItem)
            self._dialog.treeFrame._tree.setCurrentItem(newItem)

            self._dialog.treeFrame._tree.expandAll()
            QMessageBox.information(self, 'Project info', 'Create project successful')
            self._dialog.showDbFrame(newnode)
        else:
            if not reply.err:
                QMessageBox.warning(self, 'Warnning info', 'Create project fail')
            else:
                QMessageBox.warning(self, 'Warnning info', 'Create project fail: %s' % reply.err)

# Class handling database creation
class DatabaseCreateFrame(QWidget):
    def __init__(self, dialog):
        super(DatabaseCreateFrame, self).__init__()
        self._dialog = dialog
        self._project = None
        self.qvl = QVBoxLayout(self)

        ###qw1
        self.upload_databse = QGroupBox("Upload Database", self)
        self.upload_databse_layout = QVBoxLayout(self.upload_databse)

        ####p1 but in fact 3 qw
        self.idbname_hvl = QHBoxLayout()
        self.idbname_label = QLabel("IDB Name:")
        self.idbname_lineEdit = QLineEdit()
        self.idbname_lineEdit.setValidator(QRegExpValidator(QRegExp("[a-zA-Z0-9-]+")))
        self.idbname_lineEdit.setClearButtonEnabled(True)
        self.idbname_lineEdit.setPlaceholderText('Please edit the database name')
        self.idbname_lineEdit.setMaxLength(30)
        self.idbtype_label = QLabel()
        app_path = QCoreApplication.applicationFilePath()
        self.idb_type = '.i64' if 'ida64' in app_path else '.idb'
        self.idbtype_label.setText(self.idb_type)
        self.idbname_hvl.addWidget(self.idbname_label)
        self.idbname_hvl.addWidget(self.idbname_lineEdit)
        self.idbname_hvl.addWidget(self.idbtype_label)
        self.upload_databse_layout.addLayout(self.idbname_hvl)

        self.project_hvl = QHBoxLayout()
        self.project_label = QLabel()
        self.project_label.setText("Project:")
        self.project_lineEdit = QLineEdit()
        self.project_lineEdit.setPlaceholderText('No project selected')  ###0609
        self.project_lineEdit.setReadOnly(True)  # zj debug
        self.project_hvl.addWidget(self.project_label)
        self.project_hvl.addWidget(self.project_lineEdit)
        self.upload_databse_layout.addLayout(self.project_hvl)

        self.comment_hvl = QHBoxLayout()
        self.comment_label = QLabel()
        self.comment_label.setText("IDB comments:")
        self.comment_lineEdit = QLineEdit()
        self.comment_lineEdit.setValidator(QRegExpValidator(QRegExp("[a-zA-Z0-9-]+")))
        self.comment_lineEdit.setClearButtonEnabled(True)
        self.comment_lineEdit.setPlaceholderText('Please edit the IDB comments')
        self.comment_lineEdit.setMaxLength(80)
        self.comment_hvl.addWidget(self.comment_label)
        self.comment_hvl.addWidget(self.comment_lineEdit)
        self.upload_databse_layout.addLayout(self.comment_hvl)

        self.idb_group = QGroupBox("Project ID:", self.upload_databse)
        self.idb_group_layout = QVBoxLayout(self.idb_group)

        self.idb_table = DatabaseTable(self.idb_group) #QTableWidget(0, 3, self.idb_group)
        self.idb_table.itemClicked.connect(self.database_table_clicked)
        self.idb_group_layout.addWidget(self.idb_table)
        self.upload_databse_layout.addWidget(self.idb_group)
        
        self.buttons_widget = QWidget(self)
        self.buttons_layout = QHBoxLayout(self.buttons_widget)
        self.create_database_btn = QPushButton('Create Database', self.buttons_widget)
        self.create_database_btn.clicked.connect(self._create_database_clicked)  ### self.create_project 0609
        #self.rename_database_btn = QPushButton('Rename Database', self.buttons_widget)
        #self.rename_database_btn.clicked.connect(self._rename_database_clicked)
        self.upload_database_btn = QPushButton('Upload Database', self.buttons_widget)  # old version 'Cancel'
        self.upload_database_btn.clicked.connect(
            self._dialog.upload_database_clicked)  # self._dialog.accept old_version
        #self.rename_database_btn.setEnabled(False)
        self.upload_database_btn.setEnabled(False)
        self.buttons_layout.addWidget(self.create_database_btn)
        #self.buttons_layout.addWidget(self.rename_database_btn)
        self.buttons_layout.addWidget(self.upload_database_btn)

        self.qvl.addWidget(self.upload_databse)
        self.qvl.addWidget(self.buttons_widget)

    def database_table_clicked(self):
        #self.rename_database_btn.setEnabled(True)
        self._database = self.idb_table.selectedItems()[0].database
        self.upload_database_btn.setEnabled(True)

    def _create_database_clicked(self):
        """Called when the create database button is clicked."""
        curItem = self._dialog.treeFrame._tree.currentItem()
        if not curItem or curItem.node.nodetype != NODETYPE_PROJECT:
            QMessageBox.information(self, 'Warning info', 'Please select the project in left tree')
            return
        self._project = curItem.node
        dbname = self.idbname_lineEdit.text()
        if not dbname:
            QMessageBox.information(self, 'Warning info', 'Please enter the IDB name')
            return

        self._project.create_database(dbname, #idb_comments,
                                         callback=self._database_created)

    def _database_created(self, reply):  # (self, database, reply)
        """Called when the new database reply is received."""
        if reply.database:
            QMessageBox.information(self, 'Database info', 'Create database successful')
            self._database = reply.database
            self.idb_table.add_item(self.idb_table.rowCount(), reply.database)
            self.idb_table.selectRow(self.idb_table.rowCount() - 1)
            self._dialog.accept()
        else:
            QMessageBox.information(self, 'Warnning info', 'Create database fail')

    def updateProject(self, project=None):
        if project:
            self.project_lineEdit.setText(project.get_name())
            self.idb_group.setTitle('Project ID: ' + project.nodeid)
            self.project_lineEdit.setReadOnly(True)  ###0609
            self.upload_database_btn.setEnabled(False)
        self.idb_table.refresh(project)

    def get_result(self):
        return self._database

class SaveDialog(ProjectListDialog):
    """
    This dialog is shown to user to select which remote database to save. We
    extend the save dialog to reuse most of the UI setup code.
    """

    def __init__(self):
        super(SaveDialog, self).__init__("Save to Remote Server", ResourceManager().static_resource("upload.png"), "Save")

        self.projFrame = ProjCreateFrame(self)
        self.dbFrame = DatabaseCreateFrame(self)

        self.stackedWidget.addWidget(self.projFrame)
        self.stackedWidget.addWidget(self.dbFrame)

        self._project = None
        self.showProjFrame()

    def showProjFrame(self):
        self.frame_flag = 1
        self.stackedWidget.setCurrentIndex(0)
        self.currentItemChanged()  # refresh the path now

    def showDbFrame(self, project):
        self.frame_flag = 2
        self.stackedWidget.setCurrentIndex(1)
        self.dbFrame.updateProject(project)  # or exec in other location
        self._project = project
        #ProjectManager().list_database(project, self._databases_listed)

    def currentItemChanged(self):
        curItem = self.treeFrame._tree.currentItem()
        if curItem is None:
            return

        if self.frame_flag == 1:
            # in ProjCreateFrame
            if curItem.node.nodetype == NODETYPE_PROJECT:
                self.showDbFrame(curItem.node)
            else:
                self.projFrame.projpath_lineEdit.setText(curItem.node.nodepath)  # overwrite after
        elif self.frame_flag == 2:
            if curItem.node.nodetype == NODETYPE_DIRECTORY:
                self.showProjFrame()
            else:
                #self.dbFrame.rename_database_btn.setEnabled(False)
                self.dbFrame.upload_database_btn.setEnabled(False)
                self.dbFrame.project_lineEdit.setText(curItem.node.nodepath)
                self.dbFrame.idb_table.refresh(curItem.node)
                #ProjectManager().list_database(project, self._databases_listed)
                # d = self._plugin.network.send_packet(ListDatabases.Query(curItem2Path[:-1]+'$id'+projectID))
                # d.add_callback(partial(self._databases_listed))
                # d.add_errback(self._plugin.logger.exception)

    def _database_created(self, database):
        """Called when the new database reply is received."""
        self._databases.append(database)
        self._refresh_databases()
        row = len(self._databases) - 1
        self._databases_table.selectRow(row)

    def _refresh_databases(self):
        super(SaveDialog, self)._refresh_databases()
        return
        '''
        for row in range(self._databases_table.rowCount()):
            for col in range(3):
                item = self._databases_table.item(row, col)
                item.setFlags(item.flags() | Qt.ItemIsEnabled)
        '''

    def get_result(self):
        return self._project, self.dbFrame.get_result()

class SaveAction(Action):
    """The "Save to server..." action installed in the "File" menu."""

    _ACTION_ID = "idarling:save"

    def __init__(self):
        super(SaveAction, self).__init__("File/Save", "Save to server...", "Save a database to server",
                                         ResourceManager().static_resource("upload.png"), SaveActionHandler())


class SaveActionHandler(ActionHandler):
    """The action handler for the "Save to server..." action."""

    _DIALOG = SaveDialog

    def update(self, ctx):
        if not ida_loader.get_path(ida_loader.PATH_TYPE_IDB):
            return ida_kernwin.AST_DISABLE
        if not ida_auto.auto_is_ok():
            return ida_kernwin.AST_DISABLE
        return super(SaveActionHandler, self).update(ctx)

    def uploaded(self, reply):
        if reply:
            # Show a success dialog
            success = QMessageBox()
            success.setIcon(QMessageBox.Information)
            success.setStandardButtons(QMessageBox.Ok)
            success.setText("Database successfully uploaded!")
            success.setWindowTitle("Save to server")
            icon_path = ResourceManager().static_resource("upload.png")
            success.setWindowIcon(QIcon(icon_path))
            success.exec_()

            # Subscribe to the event stream
            SessionManager().join_session()
        else:
            logger.info("Database uploading failed, not doing anything.")

    def _dialog_accepted(self, dialog):
        project, database = dialog.get_result()
        if SessionManager().project and SessionManager().project.nodeid != project.nodeid: # already have a different project
            SessionManager().tick = database.tick
        SessionManager().project = project
        SessionManager().database = database

        SessionManager().save_netnode()
        popup = UploadPopup(project, database.id, fin_callback=self.uploaded)
        popup.execute()
