from PyQt5.QtCore import QRegExp, Qt  # noqa: I202
from PyQt5.QtGui import QIcon, QRegExpValidator
from PyQt5.QtWidgets import (
    QDialog,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from idarling.managers import *

import logging
logger = logging.getLogger("idarling.interface.project")

class CreateProjectDialog(QDialog, object):
    """The dialog shown when an user wants to create a project."""

    def __init__(self):
        super(CreateProjectDialog, self).__init__()

        # General setup of the dialog
        logger.debug("Create project dialog")
        self.setWindowTitle("Create Project")
        icon_path = ResourceManager().static_resource("upload.png")
        self.setWindowIcon(QIcon(icon_path))
        self.resize(100, 100)

        # Set up the layout and widgets
        layout = QVBoxLayout(self)

        self._nameLabel = QLabel("<b>Project Name</b>")
        layout.addWidget(self._nameLabel)
        self._nameEdit = QLineEdit()
        self._nameEdit.setValidator(QRegExpValidator(QRegExp("[a-zA-Z0-9-]+")))
        layout.addWidget(self._nameEdit)

        buttons = QWidget(self)
        buttons_layout = QHBoxLayout(buttons)
        create_button = QPushButton("Create")
        create_button.clicked.connect(self.accept)
        buttons_layout.addWidget(create_button)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.reject)
        buttons_layout.addWidget(cancel_button)
        layout.addWidget(buttons)

    def get_result(self):
        """Get the name entered by the user."""
        return self._nameEdit.text()


class CreateDatabaseDialog(CreateProjectDialog):
    """
    The dialog shown when an user wants to create a database. We extend the
    create project dialog to avoid duplicating the UI setup code.
    """

    def __init__(self):
        super(CreateDatabaseDialog, self).__init__()
        self.setWindowTitle("Create Database")
        self._nameLabel.setText("<b>Database Name</b>")
