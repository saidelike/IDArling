# coding:utf8
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (
    QDialog,
    QGridLayout,
    QHBoxLayout,
    QMessageBox,
    QPushButton,
    QTableWidgetItem,
    QVBoxLayout,
    QWidget,
    QStackedWidget)

from idarling.managers import *
from .projtree import TreeFrame

class ProjectListDialog(QDialog, QWidget, object):
    """This dialog is shown to user to select which remote database to load."""

    # def __init__(self,plugin=0):
    def __init__(self, title, icon_path, accept_button_text):
        super(ProjectListDialog, self).__init__()
        # self._plugin = plugin
        self._projects = None
        self._databases = None
        self.frame_flag = 0  # when use by right clicked in treeArea
        self._existed_project = 0

        # General setup of the dialog
        self.setWindowTitle(title)
        # self.icon_path = self._plugin.plugin_resource("upload.png")
        self.setWindowIcon(QIcon(icon_path))
        self.resize(900, 450)

        # Setup of the layout and widgets
        self.layout = QVBoxLayout(self)
        self.main = QWidget(self)
        self.main_layout = QGridLayout(self.main)
        self.layout.addWidget(self.main)

        self._left_side = QWidget(self.main)
        self._left_layout = QVBoxLayout(self._left_side)

        self.treeFrame = TreeFrame(self)
        self.treeFrame.setMinimumWidth(300)
        self._left_layout.addWidget(self.treeFrame)

        self.main_layout.addWidget(self._left_side, 0, 0)
        self.main_layout.setColumnStretch(0, 1)

        self.right_side = QWidget(self.main)
        self.right_layout = QVBoxLayout(self.right_side)

        self.stackedWidget = QStackedWidget()
        self.right_layout.addWidget(self.stackedWidget)

        self.bottom_button_layout = QHBoxLayout()
        self.cancel_btn = QPushButton('Cancel')
        self.bottom_button_layout.addWidget(self.cancel_btn)
        self.cancel_btn.clicked.connect(self.reject)

        self.right_layout.addLayout(self.bottom_button_layout)

        self.main_layout.addWidget(self.right_side, 0, 1)
        self.main_layout.setColumnStretch(1, 2)

    def currentItemChanged(self):
        return

    def _database_renamed(self, reply):
        if (reply.feedbackInfo == 'ok'):
            QMessageBox.information(self, 'Renamed info', 'Rename database success')
            item = QTableWidgetItem(self.newName)
            self.form3_selected_item_obj.name = self.newName
            item.setData(Qt.UserRole, self.form3_selected_item_obj)
            item.setFlags(item.flags() & ~Qt.ItemIsEditable)
            self.form3._IDB_table.setItem(self.form3_selected_item_row_num, 0, item)
        else:
            QMessageBox.information(self, 'Renamed info', reply.feedbackInfo)

    def get_result(self):
        """overload later"""
        #databaseObj = self.form3._IDB_table.selectedItems()[0].data(Qt.UserRole)
        #return databaseObj
        raise NotImplementedError("get_result() not implemented")

    def upload_database_clicked(self):
        self.accept()