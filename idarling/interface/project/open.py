import ctypes
import os
import shutil
import tempfile
from functools import partial

import ida_kernwin
import ida_loader

from PyQt5.QtCore import Qt, QCoreApplication, QFileInfo  # noqa: I202
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (
    QDialog,
    QGridLayout,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QMessageBox,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
    QWidget,
    QProgressDialog)

from idarling.interface.project.filespopup import DownloadPopup
from idarling.interface.project.projdialog import ProjectListDialog
from idarling.interface.project.dbtable import DatabaseTable
from idarling.managers import *
from idarling.core.project.projectcommands import *
from idarling.interface.base.action import Action, ActionHandler
from idarling.shared.idautils import get_ida_dll, open_idb

import logging

logger = logging.getLogger("idarling.interface.project")

class OpenFrame(QWidget):
    def __init__(self, dialog):
        super(OpenFrame, self).__init__()
        self._dialog = dialog
        self.qvl = QVBoxLayout(self)

        ### project details
        self.project_details = QGroupBox("Project Details && ID:", self)
        self.project_details_layout = QGridLayout(self.project_details)
        self._file_label = QLabel("<b>File:</b>")
        self.project_details_layout.addWidget(self._file_label, 0, 0)
        self._hash_label = QLabel("<b>Hash:</b>")
        self.project_details_layout.addWidget(self._hash_label, 1, 0)
        self.project_details_layout.setColumnStretch(0, 1)
        self._type_label = QLabel("<b>Type:</b>")
        self.project_details_layout.addWidget(self._type_label, 0, 1)
        self._date_label = QLabel("<b>Date:</b>")
        self.project_details_layout.addWidget(self._date_label, 1, 1)
        self._project_path_label = QLabel("<b>Project Path:</b>")
        self.project_details_layout.addWidget(self._project_path_label, 2, 0)
        self._project_comments_label = QLabel("<b>Project Comments:</b>")
        self.project_details_layout.addWidget(self._project_comments_label, 3, 0)
        self.project_details_layout.setColumnStretch(1, 1)
        ###

        ### project details

        self._databases_group = QGroupBox("Databases", self)
        self._databases_layout = QVBoxLayout(self._databases_group)

        self.idb_comments_hvl = QHBoxLayout()
        self.idb_comments_label = QLabel()
        self.idb_comments_label.setText("<b>IDB comments:</b>")
        # self.idb_comments_lineEdit = QLineEdit()
        # self.idb_comments_lineEdit.setReadOnly(True)

        self.idb_comments_hvl.addWidget(self.idb_comments_label)
        # self.idb_comments_hvl.addWidget(self.idb_comments_lineEdit)

        self._databases_table = DatabaseTable(self._databases_group)
        self._databases_table.itemClicked.connect(
            self._table_database_clicked
        )
        self._databases_layout.addLayout(self.idb_comments_hvl)
        self._databases_layout.addWidget(self._databases_table)

        self.open_btn = QPushButton('Open database')
        self.open_btn.clicked.connect(self._dialog._open_database_clicked)
        ###
        self.qvl.addWidget(self.project_details)
        self.qvl.addWidget(self._databases_group)
        self.qvl.addWidget(self.open_btn)

    def _table_database_clicked(self):
        database = self._databases_table.selectedItems()[0].database
        #self.formOpen.idb_comments_label.setText("<b>IDB comments:</b> %s" % database.comments)
        self.open_btn.setEnabled(True)

    def get_result(self):
        return self._databases_table.selectedItems()[0].database

class OpenDialog(ProjectListDialog):
    """This dialog is shown to user to select which remote database to load."""

    def __init__(self):
        icon_path = ResourceManager().static_resource("download.png")
        super(OpenDialog, self).__init__("Open from Remote Server", icon_path, "Open")

        self.formOpen = OpenFrame(self)
        self.stackedWidget.addWidget(self.formOpen)

        self._projects = None
        self._databases = None

    def _open_database_clicked(self):
        self.accept()

    def currentItemChanged(self):
        #self._dialog.openDatabase.setEnabled(False)
        curItem = self.treeFrame._tree.currentItem()
        if not curItem:
            return
        proj = curItem.node
        if curItem.node.nodetype == NODETYPE_PROJECT:
            self.formOpen.project_details.setTitle('Project Details && ID: ' + proj.nodeid)
            self.formOpen._file_label.setText("<b>File:</b> %s" % str(proj.file))
            self.formOpen._hash_label.setText("<b>Hash:</b> %s" % proj.hash)
            self.formOpen._type_label.setText("<b>Type:</b> %s" % proj.filetype)
            self.formOpen._date_label.setText("<b>Date:</b> %s" % proj.mdate)
            self.formOpen._project_path_label.setText("<b>Project Path:</b> %s" % str(proj.nodepath))
            self.formOpen._databases_table.refresh(proj)
            #projectComments = reply.projectComments
            #self.formOpen._project_comments_label.setText("<b>Project Comments:</b> %s" % str(projectComments))
            return
        else:
            self.formOpen._databases_table.clear()
            self.formOpen._databases_table.refresh()
            return

    def get_result(self):
        proj = self.treeFrame._tree.currentItem().node
        return proj, self.formOpen.get_result()

class OpenAction(Action):
    """The "Open from server..." action installed in the "File" menu."""

    _ACTION_ID = "idarling:open"

    def __init__(self):
        super(OpenAction, self).__init__("File/Open", "Open from server...", "Load a database from server",
                                         ResourceManager().static_resource("download.png"), OpenActionHandler())


class OpenActionHandler(ActionHandler):
    """The action handler for the "Open from server..." action."""

    _DIALOG = OpenDialog

    def _dialog_accepted(self, dialog):
        self.project, self.database = dialog.get_result()
        popup = DownloadPopup(self.project, self.database.id, fin_callback=self.downloaded)
        popup.execute()

    def downloaded(self, reply):
        """Called when the file has been downloaded."""
        if reply:
            # Get the absolute path of the file
            app_path = QCoreApplication.applicationFilePath()
            app_name = QFileInfo(app_path).fileName()
            file_ext = "i64" if "64" in app_name else "idb"
            username = ConfigManager().user.name
            if not username:
                username = "unknown"
            file_name = "%s_%s_%s_%s.%s" % (self.project.get_name(), self.database.name, self.database.id, username, file_ext)
            file_path = ResourceManager().user_resource("files", file_name)

            # Write the file to disk
            with open(file_path, "wb") as output_file:
                output_file.write(reply.content)
            logger.info("Saved file %s" % file_name)

            # Save the old database
            database = ida_loader.get_path(ida_loader.PATH_TYPE_IDB)
            if database:
                ida_loader.save_database(database, ida_loader.DBFL_TEMP)

            open_idb(file_path)
        else:
            logger.info("Database downloading failed, not doing anything.")
