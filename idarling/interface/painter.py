# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import sys

import idaapi
import ida_funcs
import ida_kernwin

from PyQt5.QtCore import (  # noqa: I202
    QAbstractItemModel,
    QModelIndex,
    QObject,
    Qt,
)
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QStyledItemDelegate, QWidget
import sip

from idarling.managers import *

from idarling.interface.statuswidget import StatusWidget

if sys.version_info > (3,):
    long = int


class Painter(QObject):
    class ProxyItemDelegate(QStyledItemDelegate):
        def __init__(self, delegate, model, parent=None):
            super(Painter.ProxyItemDelegate, self).__init__(parent)
            self._delegate = delegate
            self._model = model

        def paint(self, painter, option, index):
            index = self._model.index(index.row(), index.column())
            self._delegate.paint(painter, option, index)

    class ProxyItemModel(QAbstractItemModel):
        def __init__(self, model, parent=None):
            super(Painter.ProxyItemModel, self).__init__(parent)
            self._model = model

        def index(self, row, column, parent=QModelIndex()):
            return self.createIndex(row, column)

        def parent(self, index):
            index = self._model.index(index.row(), index.column())
            return self._model.parent(index)

        def rowCount(self):  # noqa: N802
            return self._model.rowCount()

        def columnCount(self):  # noqa: N802
            return self._model.columnCount()

        def data(self, index, role=Qt.DisplayRole):
            # Check if disabled by the user
            if role == Qt.BackgroundRole and ConfigManager().cursors.funcs:
                func_ea = int(index.sibling(index.row(), 2).data(), 16)
                func = ida_funcs.get_func(func_ea)
                for user in UserManager().get_users().values():
                    if ida_funcs.func_contains(func, user["ea"]):
                        r, g, b = StatusWidget.ida_to_python(user["color"])
                        return QColor(StatusWidget.python_to_qt(r, g, b))
            index = self._model.index(index.row(), index.column())
            return self._model.data(index, role)

    def __init__(self):
        super(Painter, self).__init__()

        self._ida_nav_colorizer = None
        self._nbytes = 0
        self._pseudocode_vu = []
        self._refreshing_widgets = set()
        self._default_color = None

    def nav_colorizer(self, ea, nbytes):
        """This is the custom nav colorizer used by the painter."""
        self._nbytes = nbytes

        # There is a bug in IDA: with a huge number of segments, all the navbar
        # is colored with the user color. This will be resolved in IDA 7.2.
        if ConfigManager().cursors.navbar:
            for user in UserManager().get_users().values():
                # Cursor color
                if ea - nbytes * 2 <= user["ea"] <= ea + nbytes * 2:
                    return long(user["color"])
                # Cursor borders
                if ea - nbytes * 4 <= user["ea"] <= ea + nbytes * 4:
                    return long(0)
        orig = ida_kernwin.call_nav_colorizer(
            self._ida_nav_colorizer, ea, nbytes
        )
        return long(orig)

    def ready_to_run(self):
        # The default nav colorized can only be recovered once!
        ida_nav_colorizer = ida_kernwin.set_nav_colorizer(self.nav_colorizer)
        if ida_nav_colorizer is not None:
            self._ida_nav_colorizer = ida_nav_colorizer
        self.refresh()

    def get_ea_hint(self, ea):
        if not ConfigManager().cursors.navbar:
            return None

        for name, user in UserManager().get_users().items():
            start_ea = user["ea"] - self._nbytes * 4
            end_ea = user["ea"] + self._nbytes * 4
            # Check if the navbar range contains the user's address
            if start_ea <= ea <= end_ea:
                return str(name)

    def get_bg_color(self, ea):
        # Check if disabled by the user
        if not ConfigManager().cursors.disasm:
            return None

        for user in UserManager().get_users().values():
            if ea == user["ea"]:
                return user["color"]
        return None

    def _handle_widget(self, twidget):

        widget_type = ida_kernwin.get_widget_type(twidget)
        if widget_type == ida_kernwin.BWN_DISASM:
            #self._refreshing_widgets[long(twidget)] = twidget  # note the TWidget type does not support hashing
            #widget = sip.wrapinstance(long(twidget), QWidget)
            #self._refreshing_widgets.add(widget)
            pass
        #elif widget.windowTitle() == "Functions window":
        elif widget_type == ida_kernwin.BWN_FUNCS:
            #self._refreshing_widgets[long(twidget)] = twidget
            widget = sip.wrapinstance(long(twidget), QWidget)
            self._refreshing_widgets.add(widget)
            table = widget.layout().itemAt(0).widget()

            # Replace the table's item delegate
            model = Painter.ProxyItemModel(table.model(), self)
            old_deleg = table.itemDelegate()
            new_deleg = Painter.ProxyItemDelegate(old_deleg, model, self)
            table.setItemDelegate(new_deleg)
        else:
            pass

    def widget_visible(self, twidget):
        self._handle_widget(twidget)

    def current_widget_changed(self, widget, prev_widget):
        self._handle_widget(widget)

    def create_desktop_widget(self, ttl, cfg):
        # widget create order:
        # Functions window -> IDA View-A -> Hex View-1 -> Structures -> ...
        if ttl == "Functions window":
            #widget = idaapi.find_widget("Functions window")
            widget = idaapi.open_funcs_window(-1)
            if widget:
                self._handle_widget(widget)
            return widget

    def refresh(self):
        ida_kernwin.refresh_navband(True)
        for widget in self._refreshing_widgets:
            #ida_kernwin.repaint_custom_viewer(widget)
            #widget.update()
            focus = widget.hasFocus()
            if not focus:
                widget.setVisible(False); widget.setVisible(True) # works
                #widget.setFocus()
            #widget.repaint(0, 0, widget.width(), widget.height())
        ida_kernwin.request_refresh(ida_kernwin.IWID_DISASMS)
        #ida_kernwin.request_refresh(ida_kernwin.IWID_FUNCS)


    def update_pseudocode(self):
        self._pseudocode_vu = []
        names = ["Pseudocode-%c" % chr(ord("A") + i) for i in range(5)]
        for name in names:
            widget = ida_kernwin.find_widget(name)
            if widget:
                vu = idaapi.get_widget_vdui(widget)
                if vu:
                    self._pseudocode_vu.append(vu)
                    if not self._default_color:
                        self._default_color = vu.cfunc.pseudocode[0].bgcolor

    def refresh_pseudocode_window(self):
        if not ConfigManager().cursors.hexrays:
            return
        for vu in self._pseudocode_vu:
            eamap = self._get_vu_context(vu)
            ps = vu.cfunc.pseudocode
            for line in ps:
                line.bgcolor = self._default_color

            for user in UserManager().get_users().values():
                if eamap.has_key(user['ea']):
                    ps[eamap[user['ea']]].bgcolor = user['color']
        idaapi.refresh_idaview_anyway()

    def _get_item_indexes(self, line):
        indexes = []
        tag = idaapi.COLOR_ON + chr(idaapi.COLOR_ADDR)
        pos = line.find(tag)
        while pos != -1 and line[pos+len(tag):] >= idaapi.COLOR_ADDR_SIZE:
            item_idx = line[pos+len(tag):pos+len(tag)+idaapi.COLOR_ADDR_SIZE]
            indexes.append(int(item_idx, 16))
            pos = line.find(tag, pos+len(tag)+idaapi.COLOR_ADDR_SIZE)
        return indexes

    def _get_vu_context(self, vu):
        if vu:
            ps = vu.cfunc.get_pseudocode()
            if ps:
                eamap = {}
                for i in range(len(ps)):
                    item_idxs = self._get_item_indexes(ps[i].line)
                    for j in item_idxs:
                        try:
                            item = vu.cfunc.treeitems.at(j)
                            if item and item.ea != idaapi.BADADDR:
                                eamap[item.ea] = i
                        except:
                            pass
                return eamap
        return None
