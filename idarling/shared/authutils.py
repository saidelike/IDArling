from hashlib import sha256
def challenge_calc(name, passhash, challenge):
    return sha256((name + passhash + challenge).encode('utf-8')).hexdigest()

from hashlib import md5
def calc_passhash(password):
    if isinstance(password, str):
        return md5(password.encode('utf-8')).hexdigest()
    else:
        return md5(password).hexdigest()