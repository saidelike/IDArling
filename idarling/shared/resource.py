import os

import ida_diskio

from idarling.shared.utils import Singleton

@Singleton
class ResourceManager(object):
    @staticmethod
    def static_resource(filename):
        """
        Return the absolute path to a plugin resource located within the
        plugin's installation folder (should be within idarling/resources).
        """
        plugin_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
        return os.path.join(plugin_path, "resources", filename)

    @staticmethod
    def user_resource(directory, filename):
        """
        Return the absolute path to a resource located in the user directory.
        It should be:
        * %APPDATA%\\Roaming\\Hex-Rays\\IDA Pro\\plugin\\idarling under Windows
        * $HOME/.idapro/plugins/idarling under Linux and MacOS.
        """
        user_dir = ida_diskio.get_user_idadir()
        plug_dir = os.path.join(user_dir, "plugins")
        local_dir = os.path.join(plug_dir, "idarling")
        res_dir = os.path.join(local_dir, directory)
        if not os.path.exists(res_dir):
            os.makedirs(res_dir, 493)  # 0755
        return os.path.join(res_dir, filename)
