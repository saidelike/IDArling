# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from idarling.network.socketimpl.packets import Default


class Proto(Default):
    """
    A model is an object can be serialized and sent over the network, but that
    can be saved into the SQL database used by the server.
    """
    SERIALIZE_ATTRS = []

    def filter_attr(self, dct):
        return {
            key: val for key, val in dct.items() if key in self.SERIALIZE_ATTRS
        }

    def build(self, dct):
        dct.update(self.filter_attr(self.__dict__))
        return dct

    def parse(self, dct):
        self.__dict__.update(self.filter_attr(dct))
        return self

    def __repr__(self):
        """
        Return a textual representation of the object. It will mainly be used
        for pretty-printing into the console.
        """
        attrs = u", ".join(
            [
                u"{}={}".format(key, val)
                for key, val in self.filter_attr(self.__dict__).items()
            ]
        )
        return u"{}({})".format(self.__class__.__name__, attrs)


NODETYPE_DIRECTORY = 1
NODETYPE_PROJECT = 2


class TreeNodeProto(Proto):
    def __init__(self, nodepath, nodeid, mdate, subnodes, nodetype=NODETYPE_DIRECTORY):
        self.nodepath = nodepath
        self.nodeid = nodeid
        self.mdate = mdate
        self.subnodes = subnodes
        self.nodetype = nodetype

    SERIALIZE_ATTRS = ["nodepath", "nodeid", "mdate", "subnodes", "nodetype"]

class PathProto(Proto):
    def __init__(self, disppath, idpath):
        self.disppath = disppath
        self.idpath = idpath

    SERIALIZE_ATTRS = ["disppath", "idpath"]

class ProjectInfoProto(Proto):
    """
    IDBs are organized into projects and databases. A project regroups
    multiples revisions of an IDB. It has a name, the hash of the input file,
    the path to the input file, the type of the input file and the date of the
    database creation.
    """

    def __init__(self, nodeid, file, hash, filetype):
        self.nodeid = nodeid
        self.file = file
        self.hash = hash
        self.filetype = filetype

    SERIALIZE_ATTRS = ["nodeid", "hash", "file", "filetype"]

class ProjectProto(ProjectInfoProto, TreeNodeProto):
    """
    IDBs are organized into projects and databases. A project regroups
    multiples revisions of an IDB. It has a name, the hash of the input file,
    the path to the input file, the type of the input file and the date of the
    database creation.
    """

    def __init__(self, nodepath, nodeid, mdate, filename, filehash, filetype):
        TreeNodeProto.__init__(self, nodepath, nodeid, mdate, [], NODETYPE_PROJECT)
        ProjectInfoProto.__init__(self, nodeid, filename, filehash, filetype)

    SERIALIZE_ATTRS = list(set(ProjectInfoProto.SERIALIZE_ATTRS + TreeNodeProto.SERIALIZE_ATTRS))


class DatabaseProto(Proto):
    """
    IDBs are organized into projects and databases. A database corresponds to
    a revision of an IDB. It has a project, a name, a date of creation, and a
    current tick (events) count.
    """

    def __init__(self, id, name, date, tick=-1):
        super(DatabaseProto, self).__init__()
        #self.project = project
        self.id = id
        self.name = name
        self.date = date
        self.tick = tick

    SERIALIZE_ATTRS = ["project", "id", "name", "date", "tick"]

class UserProto(Proto):
    def __init__(self, name, passhash, pubkey):
        super(UserProto, self).__init__()
        self.name = name
        self.passhash = passhash
        self.pubkey = pubkey

    SERIALIZE_ATTRS = ["name", "passhash", "pubkey"]