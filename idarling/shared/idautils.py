import ctypes
import os
import shutil
import sys
import tempfile

import ida_diskio
import ida_funcs
import ida_hexrays
import ida_kernwin
import ida_loader
from PyQt5.QtCore import QCoreApplication, QFileInfo


def get_ida_dll(app_name=None):
    if app_name is None:
        app_path = QCoreApplication.applicationFilePath()
        app_name = QFileInfo(app_path).fileName()
    idaname = "ida64" if "64" in app_name else "ida"
    if sys.platform == "win32":
        dllname, dlltype = idaname + ".dll", ctypes.windll
    elif sys.platform == "linux2":
        dllname, dlltype = "lib" + idaname + ".so", ctypes.cdll
    elif sys.platform == "darwin":
        dllname, dlltype = "lib" + idaname + ".dylib", ctypes.cdll
    else:
        raise EnvironmentError("Not supported platform: " + sys.platform)
    dllpath = ida_diskio.idadir(None)
    if not os.path.exists(os.path.join(dllpath, dllname)):
        dllpath = dllpath.replace("ida64", "ida")
    return dlltype[os.path.join(dllpath, dllname)]


def get_hexdsp():
    dll = get_ida_dll()
    HEXRAYS_API_MAGIC = 0x00DEC0DE00000003
    ui_broadcast = 7
    calluitype = ctypes.WINFUNCTYPE(ctypes.c_void_p, ctypes.c_int, ctypes.c_longlong, ctypes.c_void_p, ctypes.c_int)
    callui = calluitype.in_dll(dll, "callui")
    hexdsp = ctypes.c_void_p()
    if callui(ui_broadcast, HEXRAYS_API_MAGIC, ctypes.byref(hexdsp), 0) == HEXRAYS_API_MAGIC >> 32:
        return hexdsp.value
    else:
        return None


def decompile_with_flags(hexdsp, ea, flag):
    dll = get_ida_dll()
    dll.get_func.argtypes = [ctypes.c_longlong]
    dll.get_func.restype = ctypes.c_void_p
    fun = dll.get_func(ea)

    class mba_ranges_t(ctypes.Structure):
        _fields_ = [("pfn", ctypes.c_void_p),
                    ("ranges", ctypes.c_int), ("ranges", ctypes.c_int), ("ranges", ctypes.c_int),
                    ("ranges", ctypes.c_int), ("ranges", ctypes.c_int),
                    ("ranges", ctypes.c_int)]

    # return cfuncptr_t((cfunc_t *)hexdsp(hx_decompile, &mbr, hf, flags));
    hexdsptype = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_int, mba_ranges_t, ctypes.c_void_p, ctypes.c_int)
    hexdsp = hexdsptype(hexdsp)
    return hexdsp(ida_hexrays.hx_decompile, mba_ranges_t(fun, 0, 0, 0, 0, 0, 0), None, flag)


def get_raw_pseudocode_view(hexdsp, widget):
    hexdsptype = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_int, ctypes.c_void_p)
    hexdsp = hexdsptype(hexdsp)
    return hexdsp(ida_hexrays.hx_get_widget_vdui, widget)


def raw_switch_to(hexdsp, raw_vu, raw_cfunc):
    hexdsp = get_hexdsp()

    class cfuncptr_t(ctypes.Structure):
        _fields_ = [("ptr", ctypes.c_void_p)]

    hexdsptype = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_int, ctypes.c_void_p, ctypes.POINTER(cfuncptr_t),
                                  ctypes.c_int)
    hexdsp = hexdsptype(hexdsp)
    return hexdsp(ida_hexrays.hx_vdui_t_switch_to, raw_vu, ctypes.byref(cfuncptr_t(raw_cfunc)), 0)


def refresh_pseudocode_view(ea):
    """Refreshes the pseudocode view in IDA."""
    names = ["Pseudocode-%c" % chr(ord("A") + i) for i in range(5)]
    hexdsp = None  # lazy initialize
    for name in names:
        widget = ida_kernwin.find_widget(name)
        if widget:
            vu = ida_hexrays.get_widget_vdui(widget)

            # Check if the address is in the same function
            func_ea = vu.cfunc.entry_ea
            func = ida_funcs.get_func(func_ea)
            if ida_funcs.func_contains(func, ea):
                # vu.refresh_view(True) <- this one will block everything
                # Old approach: ctypes & idapython
                #  clear ALL cache -> make our new decompiled one as cache -> switch to our new cache
                #   ida_hexrays.clear_cached_cfuncs()
                #   decompile_with_flags(func_ea, ida_hexrays.DECOMP_NO_WAIT | ida_hexrays.DECOMP_WARNINGS)
                #   vu.switch_to(ida_hexrays.decompile(func_ea), False)
                #   vu.cfunc = ida_hexrays.decompile(func_ea)

                # New approach: purely use ctypes to finish everything
                if hexdsp is None:  # lazy initialization to reduce call to hexrays
                    hexdsp = get_hexdsp()
                raw_func = decompile_with_flags(hexdsp, func_ea,
                                                ida_hexrays.DECOMP_NO_WAIT | ida_hexrays.DECOMP_NO_CACHE | ida_hexrays.DECOMP_WARNINGS)
                raw_vu = get_raw_pseudocode_view(hexdsp, long(widget))
                raw_switch_to(hexdsp, raw_vu, raw_func)
                vu.refresh_ctext(False)


def open_idb(file_path):
    # This is a very ugly hack used to open a database into IDA. We don't
    # have any function for this in the SDK, so I sorta hijacked the
    # snapshot functionality in this effect.
    app_path = QCoreApplication.applicationFilePath()
    app_name = QFileInfo(app_path).fileName()

    # Get the library to call functions not present in the bindings
    dll = get_ida_dll(app_name)

    # Close the old database using the term_database library function
    old_path = ida_loader.get_path(ida_loader.PATH_TYPE_IDB)
    if old_path:
        dll.term_database()

    # Open the new database using the init_database library function
    # This call only won't be enough because the user interface won't
    # be initialized, this is why the snapshot functionality is used for
    args = [app_name, file_path]
    argc = len(args)
    argv = (ctypes.POINTER(ctypes.c_char) * (argc + 1))()
    for i, arg in enumerate(args):
        arg = arg.encode("utf-8")
        argv[i] = ctypes.create_string_buffer(arg)

    v = ctypes.c_int(0)
    av = ctypes.addressof(v)
    pv = ctypes.cast(av, ctypes.POINTER(ctypes.c_int))
    dll.init_database(argc, argv, pv)

    # Create a temporary copy of the new database because we cannot use
    # the snapshot functionality to restore the currently opened database
    file_ext = ".i64" if "64" in app_name else ".idb"
    tmp_file, tmp_path = tempfile.mkstemp(suffix=file_ext)
    shutil.copyfile(file_path, tmp_path)

    # This hook is used to delete the temporary database when all done
    class UIHooks(ida_kernwin.UI_Hooks):
        def database_inited(self, is_new_database, idc_script):
            self.unhook()

            os.close(tmp_file)
            if os.path.exists(tmp_path):
                os.remove(tmp_path)

    hooks = UIHooks()
    hooks.hook()

    # Call the restore_database_snapshot library function
    # This will initialize the user interface, completing the process
    s = ida_loader.snapshot_t()
    s.filename = tmp_path  # Use the temporary database
    ida_kernwin.restore_database_snapshot(s, None, None)


DATABASE_SLICE_SIZE = 5 * 1024 * 1024      # 5MB
def get_database_data(chunk=False):
    if chunk:
        return get_database_data_chunk()
    else:
        return get_database_data_nochunk()

def get_database_data_nochunk():
    input_path = ida_loader.get_path(ida_loader.PATH_TYPE_IDB)
    ida_loader.save_database(input_path, 0)
    with open(input_path.decode('utf-8'), "rb") as input_file:
        return input_file.read()

def get_database_data_chunk():
    input_path = ida_loader.get_path(ida_loader.PATH_TYPE_IDB)
    ida_loader.save_database(input_path, 0)

    with open(input_path.decode('utf-8'), "rb") as input_file:
        while True:
            data = input_file.read(DATABASE_SLICE_SIZE)
            if not data:
                break
            yield data
        return