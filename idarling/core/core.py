from idarling.managers import *
from idarling.module import Module


class CoreModule(Module):
    def _install(self):
        HookManager().global_hook()

    def _uninstall(self):
        HookManager().global_unhook()