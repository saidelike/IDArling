# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import ctypes
import sys

import ida_bytes
import ida_enum
import ida_frame
import ida_funcs
import ida_idp
import ida_kernwin
import ida_lines
import ida_nalt
import ida_name
import ida_pro
import ida_range
import ida_segment
import ida_segregs
import ida_struct
import ida_typeinf
import ida_ua

from idarling.shared.idautils import refresh_pseudocode_view, get_ida_dll
from ..hook import _send_packet
from idarling.network.socketimpl.packets import DefaultEvent

import logging
logger = logging.getLogger("idarling.hook.events")

if sys.version_info > (3,):
    unicode = str


class Event(DefaultEvent):
    """
    This is a common class for all events that provides utility methods to
    encode/decode strings and raw bytes. Events should also implement __call__
    which is called when the event needs to be replayed into IDA.
    """

    @staticmethod
    def encode(s):
        """Encodes a unicode string into UTF-8 bytes."""
        if not isinstance(s, unicode):
            return s
        return s.encode("utf-8")

    @staticmethod
    def encode_bytes(s):
        """Encodes a unicode string into raw bytes."""
        if not isinstance(s, unicode):
            return s
        return s.encode("raw_unicode_escape")

    @staticmethod
    def decode(s):
        """Decodes UTF-8 bytes into a unicode string."""
        if not isinstance(s, str):
            return s
        return s.decode("utf-8")

    @staticmethod
    def decode_bytes(s):
        """Decodes raw bytes into a unicode string."""
        if not isinstance(s, str):
            return s
        return s.decode("raw_unicode_escape")

    def __call__(self):
        """Reproduce the underlying user event into IDA."""
        raise NotImplementedError("__call__() not implemented")

    @staticmethod
    def hook_handler(*args, **kwargs):
        raise NotImplementedError("hook_handler() not implemented")

class MakeCodeEvent(Event):
    __event__ = "make_code"

    def __init__(self, ea):
        super(MakeCodeEvent, self).__init__()
        self.ea = ea

    def __call__(self):
        ida_ua.create_insn(self.ea)

    @staticmethod
    def hook_handler(dispatcher_class, insn):
        _send_packet(MakeCodeEvent(insn.ea), True)
        return 0


class MakeDataEvent(Event):
    __event__ = "make_data"

    def __init__(self, ea, flags, size, tid):
        super(MakeDataEvent, self).__init__()
        self.ea = ea
        self.flags = flags
        self.size = size
        self.tid = tid

    def __call__(self):
        ida_bytes.create_data(self.ea, self.flags, self.size, self.tid)

    @staticmethod
    def hook_handler(dispatcher_class, ea, flags, tid, size):
        _send_packet(MakeDataEvent(ea, flags, size, tid), True)
        return 0


class RenamedEvent(Event):
    __event__ = "renamed"

    def __init__(self, ea, new_name, local_name):
        super(RenamedEvent, self).__init__()
        self.ea = ea
        self.new_name = Event.decode(new_name)
        self.local_name = local_name

    def __call__(self):
        flags = ida_name.SN_LOCAL if self.local_name else 0
        ida_name.set_name(
            self.ea, Event.encode(self.new_name), flags | ida_name.SN_NOWARN
        )
        ida_kernwin.request_refresh(ida_kernwin.IWID_DISASMS)

        refresh_pseudocode_view(self.ea)

    @staticmethod
    def hook_handler(dispatcher_class, ea, new_name, local_name):
        _send_packet(RenamedEvent(ea, new_name, local_name))
        return 0


class FuncAddedEvent(Event):
    __event__ = "func_added"

    def __init__(self, start_ea, end_ea):
        super(FuncAddedEvent, self).__init__()
        self.start_ea = start_ea
        self.end_ea = end_ea

    def __call__(self):
        ida_funcs.add_func(self.start_ea, self.end_ea)

    @staticmethod
    def hook_handler(dispatcher_class, func):
        _send_packet(FuncAddedEvent(func.start_ea, func.end_ea))
        return 0


class DeletingFuncEvent(Event):
    __event__ = "deleting_func"

    def __init__(self, start_ea):
        super(DeletingFuncEvent, self).__init__()
        self.start_ea = start_ea

    def __call__(self):
        ida_funcs.del_func(self.start_ea)

    @staticmethod
    def hook_handler(dispatcher_class, func):
        _send_packet(DeletingFuncEvent(func.start_ea))
        return 0


class SetFuncStartEvent(Event):
    __event__ = "set_func_start"

    def __init__(self, start_ea, new_start):
        super(SetFuncStartEvent, self).__init__()
        self.start_ea = start_ea
        self.new_start = new_start

    def __call__(self):
        ida_funcs.set_func_start(self.start_ea, self.new_start)

    @staticmethod
    def hook_handler(dispatcher_class, func, new_start):
        _send_packet(SetFuncStartEvent(func.start_ea, new_start))
        return 0


class SetFuncEndEvent(Event):
    __event__ = "set_func_end"

    def __init__(self, start_ea, new_end):
        super(SetFuncEndEvent, self).__init__()
        self.start_ea = start_ea
        self.new_end = new_end

    def __call__(self):
        ida_funcs.set_func_end(self.start_ea, self.new_end)

    @staticmethod
    def hook_handler(dispatcher_class, func, new_end):
        _send_packet(SetFuncEndEvent(func.start_ea, new_end))
        return 0


class FuncTailAppendedEvent(Event):
    __event__ = "func_tail_appended"

    def __init__(self, start_ea_func, start_ea_tail, end_ea_tail):
        super(FuncTailAppendedEvent, self).__init__()
        self.start_ea_func = start_ea_func
        self.start_ea_tail = start_ea_tail
        self.end_ea_tail = end_ea_tail

    def __call__(self):
        func = ida_funcs.get_func(self.start_ea_func)
        ida_funcs.append_func_tail(func, self.start_ea_tail, self.end_ea_tail)

    @staticmethod
    def hook_handler(dispatcher_class, func, tail):
        _send_packet(
            FuncTailAppendedEvent(
                func.start_ea, tail.start_ea, tail.end_ea
            )
        )
        return 0


class FuncTailDeletedEvent(Event):
    __event__ = "func_tail_deleted"

    def __init__(self, start_ea_func, tail_ea):
        super(FuncTailDeletedEvent, self).__init__()
        self.start_ea_func = start_ea_func
        self.tail_ea = tail_ea

    def __call__(self):
        func = ida_funcs.get_func(self.start_ea_func)
        ida_funcs.remove_func_tail(func, self.tail_ea)

    @staticmethod
    def hook_handler(dispatcher_class, func, tail_ea):
        _send_packet(FuncTailDeletedEvent(func.start_ea, tail_ea))
        return 0


class TailOwnerChangedEvent(Event):
    __event__ = "tail_owner_changed"

    def __init__(self, tail_ea, owner_func):
        super(TailOwnerChangedEvent, self).__init__()
        self.tail_ea = tail_ea
        self.owner_func = owner_func

    def __call__(self):
        tail = ida_funcs.get_fchunk(self.tail_ea)
        ida_funcs.set_tail_owner(tail, self.owner_func)

    @staticmethod
    def hook_handler(dispatcher_class, tail, owner_func, old_owner):
        _send_packet(TailOwnerChangedEvent(tail.start_ea, owner_func))
        return 0


class CmtChangedEvent(Event):
    __event__ = "cmt_changed"

    def __init__(self, ea, comment, rptble):
        super(CmtChangedEvent, self).__init__()
        self.ea = ea
        self.comment = Event.decode(comment)
        self.rptble = rptble

    def __call__(self):
        ida_bytes.set_cmt(self.ea, Event.encode(self.comment), self.rptble)

    @staticmethod
    def hook_handler(dispatcher_class, ea, repeatable_cmt):
        cmt = ida_bytes.get_cmt(ea, repeatable_cmt)
        cmt = "" if not cmt else cmt
        _send_packet(CmtChangedEvent(ea, cmt, repeatable_cmt))
        return 0


class RangeCmtChangedEvent(Event):
    __event__ = "range_cmt_changed"

    def __init__(self, kind, a, cmt, rptble):
        super(RangeCmtChangedEvent, self).__init__()
        self.kind = kind
        self.start_ea = a.start_ea
        self.end_ea = a.end_ea
        self.cmt = Event.decode(cmt)
        self.rptble = rptble

    def __call__(self):
        cmt = Event.encode(self.cmt)
        if self.kind == ida_range.RANGE_KIND_FUNC:
            func = ida_funcs.get_func(self.start_ea)
            ida_funcs.set_func_cmt(func, cmt, self.rptble)
        elif self.kind == ida_range.RANGE_KIND_SEGMENT:
            segment = ida_segment.getseg(self.start_ea)
            ida_segment.set_segment_cmt(segment, cmt, self.rptble)
        else:
            raise Exception("Unsupported range kind: %d" % self.kind)

    @staticmethod
    def hook_handler(dispatcher_class, kind, a, cmt, repeatable):
        _send_packet(RangeCmtChangedEvent(kind, a, cmt, repeatable))
        return 0


class ExtraCmtChangedEvent(Event):
    __event__ = "extra_cmt_changed"

    def __init__(self, ea, line_idx, cmt):
        super(ExtraCmtChangedEvent, self).__init__()
        self.ea = ea
        self.line_idx = line_idx
        self.cmt = Event.decode(cmt)

    def __call__(self):
        ida_lines.del_extra_cmt(self.ea, self.line_idx)
        isprev = 1 if self.line_idx - 1000 < 1000 else 0
        if not self.cmt:
            return 0
        ida_lines.add_extra_cmt(self.ea, isprev, Event.encode(self.cmt))

    @staticmethod
    def hook_handler(dispatcher_class, ea, line_idx, cmt):
        _send_packet(ExtraCmtChangedEvent(ea, line_idx, cmt))
        return 0


class TiChangedEvent(Event):
    __event__ = "ti_changed"

    def __init__(self, ea, py_type):
        super(TiChangedEvent, self).__init__()
        self.ea = ea
        self.py_type = []
        if py_type:
            self.py_type.extend(Event.decode_bytes(t) for t in py_type)

    def __call__(self):
        py_type = [Event.encode_bytes(t) for t in self.py_type]
        if len(py_type) == 3:
            py_type = py_type[1:]
        if len(py_type) >= 2:
            ida_typeinf.apply_type(
                None,
                py_type[0],
                py_type[1],
                self.ea,
                ida_typeinf.TINFO_DEFINITE,
            )

    @staticmethod
    def hook_handler(dispatcher_class, ea, type, fname):
        type = ida_typeinf.idc_get_type_raw(ea)
        _send_packet(TiChangedEvent(ea, type))
        return 0

# Syncing the local types are quite tricky, because the local_types_changed event
# not only occurs when we editing types from Local types window, but also occurs
# when we editing in the Structures window
# and multiple hooks will be triggered, as shown below:
#   when editing in Structure view:
#       local_types_changed -> local_types_changed -> struc_member_XXX
#   when creating types in Local types view:
#       local_types_changed -> local_types_changed
#   when editing types in Local types view:
#       local_types_changed
#
class LocalTypesChangedEvent(Event):
    __event__ = "local_types_changed"

    def __init__(self, local_types):
        super(LocalTypesChangedEvent, self).__init__()
        self.local_types = []
        for py_ord, name, type, fields, cmt, fieldcmts, sclass in local_types:
            name = Event.decode_bytes(name)
            type = Event.decode_bytes(type)
            fields = Event.decode_bytes(fields)
            cmt = Event.decode_bytes(cmt)
            fieldcmts = Event.decode_bytes(fieldcmts)
            self.local_types.append(
                (py_ord, name, type, fields, cmt, fieldcmts, sclass)
            )

    def __call__(self):
        dll = get_ida_dll()

        get_idati = dll.get_idati
        get_idati.argtypes = []
        get_idati.restype = ctypes.c_void_p

        # note that sclass parameter is also a pointer
        set_numbered_type = dll.set_numbered_type
        set_numbered_type.argtypes = [
            ctypes.c_void_p,
            ctypes.c_uint32,
            ctypes.c_int,
            ctypes.c_char_p,
            ctypes.c_char_p,
            ctypes.c_char_p,
            ctypes.c_char_p,
            ctypes.c_char_p,
            ctypes.POINTER(ctypes.c_int),
        ]
        set_numbered_type.restype = ctypes.c_int

        py_ti = ida_typeinf.get_idati()
        ordinal_qty = ida_typeinf.get_ordinal_qty(py_ti) - 1
        last_ordinal = self.local_types[-1][0]
        if ordinal_qty < last_ordinal:
            ida_typeinf.alloc_type_ordinals(py_ti, last_ordinal - ordinal_qty)
        #else:
        #    for py_ordinal in range(last_ordinal + 1, ordinal_qty + 1):
        #        ida_typeinf.del_numbered_type(py_ti, py_ordinal)

        local_types = self.local_types
        for py_ord, name, type, fields, cmt, fieldcmts, sclass in local_types:
            if type:
                ti = get_idati()
                ordinal = ctypes.c_uint32(py_ord)
                ntf_flags = ctypes.c_int(ida_typeinf.NTF_REPLACE)
                name = ctypes.c_char_p(Event.encode_bytes(name))
                type = ctypes.c_char_p(Event.encode_bytes(type))
                fields = ctypes.c_char_p(Event.encode_bytes(fields))
                cmt = ctypes.c_char_p(Event.encode_bytes(cmt))
                fieldcmts = ctypes.c_char_p(Event.encode_bytes(fieldcmts))
                sclass = ctypes.c_int(sclass)
                set_numbered_type(
                    ti,
                    ordinal,
                    ntf_flags,
                    name,
                    type,
                    fields,
                    cmt,
                    fieldcmts,
                    ctypes.byref(sclass),
                )

        ida_kernwin.request_refresh(ida_kernwin.IWID_LOCTYPS)

    last_local_types = None

    @staticmethod
    def hook_handler(dispatcher_class):
        # we don't handle events triggered by structures
        if ida_kernwin.get_widget_type(ida_kernwin.get_current_widget()) == ida_kernwin.BWN_STRUCTS:
            return 0

        dll = get_ida_dll()

        get_idati = dll.get_idati
        get_idati.argtypes = []
        get_idati.restype = ctypes.c_void_p

        get_numbered_type = dll.get_numbered_type
        get_numbered_type.argtypes = [
            ctypes.c_void_p,
            ctypes.c_uint32,
            ctypes.POINTER(ctypes.c_char_p),
            ctypes.POINTER(ctypes.c_char_p),
            ctypes.POINTER(ctypes.c_char_p),
            ctypes.POINTER(ctypes.c_char_p),
            ctypes.POINTER(ctypes.c_int),
        ]
        get_numbered_type.restype = ctypes.c_bool

        local_types = []
        py_ti = ida_typeinf.get_idati()
        for py_ord in range(1, ida_typeinf.get_ordinal_qty(py_ti)):
            name = ida_typeinf.get_numbered_type_name(py_ti, py_ord)

            ti = get_idati()
            ordinal = ctypes.c_uint32(py_ord)
            type = ctypes.c_char_p()
            fields = ctypes.c_char_p()
            cmt = ctypes.c_char_p()
            fieldcmts = ctypes.c_char_p()
            sclass = ctypes.c_int()
            get_numbered_type(
                ti,
                ordinal,
                ctypes.pointer(type),
                ctypes.pointer(fields),
                ctypes.pointer(cmt),
                ctypes.pointer(fieldcmts),
                ctypes.pointer(sclass),
            )
            local_types.append(
                (
                    py_ord,
                    name,
                    type.value,
                    fields.value,
                    cmt.value,
                    fieldcmts.value,
                    sclass.value,
                )
            )

        if LocalTypesChangedEvent.last_local_types is None:
            # do we need to send type now?
            #diff = local_types
            LocalTypesChangedEvent.last_local_types = local_types
            return 0
        else:
            def differ_local_types(types1, types2):
                # [(i, types1, types2), ...]
                ret_types = []
                for i in range(max([len(types1), len(types2)])):
                    if i >= len(types1):
                        ret_types.append((i, None, types2[i]))
                    elif i >= len(types2):
                        ret_types.append((i, types1[i], None))
                    else:
                        if types1[i] != types2[i]:
                            ret_types.append((i, types1[i], types2[i]))
                return ret_types

            diff = differ_local_types(LocalTypesChangedEvent.last_local_types, local_types)

            # When changing a type, IDA will delete type and re-create it
            if len(diff) == 1 and diff[0][2][1] is None: # diff[1].new.name is None
                return 0
            elif len(diff) == 0: # no type changed
                return 0

            LocalTypesChangedEvent.last_local_types = local_types
            diff = [c[2] for c in diff]
        _send_packet(LocalTypesChangedEvent(diff))
        return 0


class OpTypeChangedEvent(Event):
    __event__ = "op_type_changed"

    def __init__(self, ea, n, op, extra):
        super(OpTypeChangedEvent, self).__init__()
        self.ea = ea
        self.n = n
        self.op = op
        self.extra = extra

    def __call__(self):
        if self.op == "hex":
            ida_bytes.op_hex(self.ea, self.n)
        if self.op == "bin":
            ida_bytes.op_bin(self.ea, self.n)
        if self.op == "dec":
            ida_bytes.op_dec(self.ea, self.n)
        if self.op == "chr":
            ida_bytes.op_chr(self.ea, self.n)
        if self.op == "oct":
            ida_bytes.op_oct(self.ea, self.n)
        if self.op == "enum":
            id = ida_enum.get_enum(Event.encode(self.extra["ename"]))
            ida_bytes.op_enum(self.ea, self.n, id, self.extra["serial"])
        if self.op == "struct":
            path_len = len(self.extra["spath"])
            path = ida_pro.tid_array(path_len)
            for i in range(path_len):
                sname = Event.encode(self.extra["spath"][i])
                path[i] = ida_struct.get_struc_id(sname)
            insn = ida_ua.insn_t()
            ida_ua.decode_insn(insn, self.ea)
            ida_bytes.op_stroff(
                insn, self.n, path.cast(), path_len, self.extra["delta"]
            )
        if self.op == "stkvar":
            ida_bytes.op_stkvar(self.ea, self.n)
        # FIXME: No hooks are called when inverting sign
        # if self.op == 'invert_sign':
        #     idc.toggle_sign(ea, n)

    @staticmethod
    def hook_handler(dispatcher_class, ea, n):
        def gather_enum_info(ea, n):
            id = ida_bytes.get_enum_id(ea, n)[0]
            serial = ida_enum.get_enum_idx(id)
            return id, serial

        extra = {}
        mask = ida_bytes.MS_0TYPE if not n else ida_bytes.MS_1TYPE
        flags = ida_bytes.get_full_flags(ea) & mask

        def is_flag(type):
            return flags == mask & type

        if is_flag(ida_bytes.hex_flag()):
            op = "hex"
        elif is_flag(ida_bytes.dec_flag()):
            op = "dec"
        elif is_flag(ida_bytes.char_flag()):
            op = "chr"
        elif is_flag(ida_bytes.bin_flag()):
            op = "bin"
        elif is_flag(ida_bytes.oct_flag()):
            op = "oct"
        elif is_flag(ida_bytes.enum_flag()):
            op = "enum"
            id, serial = gather_enum_info(ea, n)
            ename = ida_enum.get_enum_name(id)
            extra["ename"] = Event.decode(ename)
            extra["serial"] = serial
        elif is_flag(flags & ida_bytes.stroff_flag()):
            op = "struct"
            path = ida_pro.tid_array(1)
            delta = ida_pro.sval_pointer()
            path_len = ida_bytes.get_stroff_path(
                path.cast(), delta.cast(), ea, n
            )
            spath = []
            for i in range(path_len):
                sname = ida_struct.get_struc_name(path[i])
                spath.append(Event.decode(sname))
            extra["delta"] = delta.value()
            extra["spath"] = spath
        elif is_flag(ida_bytes.stkvar_flag()):
            op = "stkvar"
        # FIXME: No hooks are called when inverting sign
        # elif ida_bytes.is_invsign(ea, flags, n):
        #     op = 'invert_sign'
        else:
            return 0  # FIXME: Find a better way to do this
        _send_packet(OpTypeChangedEvent(ea, n, op, extra))
        return 0


class EnumCreatedEvent(Event):
    __event__ = "enum_created"

    def __init__(self, enum, name):
        super(EnumCreatedEvent, self).__init__()
        self.enum = enum
        self.name = Event.decode(name)

    def __call__(self):
        ida_enum.add_enum(self.enum, Event.encode(self.name), 0)

    @staticmethod
    def hook_handler(dispatcher_class, enum):
        name = ida_enum.get_enum_name(enum)
        _send_packet(EnumCreatedEvent(enum, name))
        return 0


class EnumDeletedEvent(Event):
    __event__ = "enum_deleted"

    def __init__(self, ename):
        super(EnumDeletedEvent, self).__init__()
        self.ename = Event.decode(ename)

    def __call__(self):
        ida_enum.del_enum(ida_enum.get_enum(Event.encode(self.ename)))

    @staticmethod
    def hook_handler(dispatcher_class, id):
        _send_packet(EnumDeletedEvent(ida_enum.get_enum_name(id)))
        return 0


class EnumRenamedEvent(Event):
    __event__ = "enum_renamed"

    def __init__(self, oldname, newname, is_enum):
        super(EnumRenamedEvent, self).__init__()
        self.oldname = Event.decode(oldname)
        self.newname = Event.decode(newname)
        self.is_enum = is_enum

    def __call__(self):
        if self.is_enum:
            enum = ida_enum.get_enum(Event.encode(self.oldname))
            ida_enum.set_enum_name(enum, Event.encode(self.newname))
        else:
            emem = ida_enum.get_enum_member_by_name(Event.encode(self.oldname))
            ida_enum.set_enum_member_name(emem, Event.encode(self.newname))

    @staticmethod
    def hook_handler(dispatcher_class, id, is_enum, newname):
        if is_enum:
            oldname = ida_enum.get_enum_name(id)
        else:
            oldname = ida_enum.get_enum_member_name(id)
        _send_packet(EnumRenamedEvent(oldname, newname, is_enum))
        return 0


class EnumBfChangedEvent(Event):
    __event__ = "enum_bf_changed"

    def __init__(self, ename, bf_flag):
        super(EnumBfChangedEvent, self).__init__()
        self.ename = Event.decode(ename)
        self.bf_flag = bf_flag

    def __call__(self):
        enum = ida_enum.get_enum(Event.encode(self.ename))
        ida_enum.set_enum_bf(enum, self.bf_flag)

    @staticmethod
    def hook_handler(dispatcher_class, id):
        bf_flag = 1 if ida_enum.is_bf(id) else 0
        ename = ida_enum.get_enum_name(id)
        _send_packet(EnumBfChangedEvent(ename, bf_flag))
        return 0


class EnumCmtChangedEvent(Event):
    __event__ = "enum_cmt_changed"

    def __init__(self, emname, cmt, repeatable_cmt):
        super(EnumCmtChangedEvent, self).__init__()
        self.emname = Event.decode(emname)
        self.cmt = Event.decode(cmt)
        self.repeatable_cmt = repeatable_cmt

    def __call__(self):
        emem = ida_enum.get_enum_member_by_name(Event.encode(self.emname))
        cmt = Event.encode(self.cmt if self.cmt else "")
        ida_enum.set_enum_cmt(emem, cmt, self.repeatable_cmt)

    @staticmethod
    def hook_handler(dispatcher_class, tid, repeatable_cmt):
        cmt = ida_enum.get_enum_cmt(tid, repeatable_cmt)
        emname = ida_enum.get_enum_name(tid)
        _send_packet(EnumCmtChangedEvent(emname, cmt, repeatable_cmt))
        return 0


class EnumMemberCreatedEvent(Event):
    __event__ = "enum_member_created"

    def __init__(self, ename, name, value, bmask):
        super(EnumMemberCreatedEvent, self).__init__()
        self.ename = Event.decode(ename)
        self.name = name
        self.value = value
        self.bmask = bmask

    def __call__(self):
        enum = ida_enum.get_enum(Event.encode(self.ename))
        ida_enum.add_enum_member(
            enum, Event.encode(self.name), self.value, self.bmask
        )

    @staticmethod
    def hook_handler(dispatcher_class, id, cid):
        ename = ida_enum.get_enum_name(id)
        name = ida_enum.get_enum_member_name(cid)
        value = ida_enum.get_enum_member_value(cid)
        bmask = ida_enum.get_enum_member_bmask(cid)
        _send_packet(
            EnumMemberCreatedEvent(ename, name, value, bmask)
        )
        return 0


class EnumMemberDeletedEvent(Event):
    __event__ = "enum_member_deleted"

    def __init__(self, ename, value, serial, bmask):
        super(EnumMemberDeletedEvent, self).__init__()
        self.ename = Event.decode(ename)
        self.value = value
        self.serial = serial
        self.bmask = bmask

    def __call__(self):
        enum = ida_enum.get_enum(Event.encode(self.ename))
        ida_enum.del_enum_member(enum, self.value, self.serial, self.bmask)

    @staticmethod
    def hook_handler(dispatcher_class, id, cid):
        ename = ida_enum.get_enum_name(id)
        value = ida_enum.get_enum_member_value(cid)
        serial = ida_enum.get_enum_member_serial(cid)
        bmask = ida_enum.get_enum_member_bmask(cid)
        _send_packet(
            EnumMemberDeletedEvent(ename, value, serial, bmask)
        )
        return 0


class StrucCreatedEvent(Event):
    __event__ = "struc_created"

    def __init__(self, struc, name, is_union):
        super(StrucCreatedEvent, self).__init__()
        self.struc = struc
        self.name = Event.decode(name)
        self.is_union = is_union

    def __call__(self):
        ida_struct.add_struc(
            self.struc, Event.encode(self.name), self.is_union
        )

    @staticmethod
    def hook_handler(dispatcher_class, tid):
        name = ida_struct.get_struc_name(tid)
        is_union = ida_struct.is_union(tid)
        _send_packet(StrucCreatedEvent(tid, name, is_union))
        return 0


class StrucDeletedEvent(Event):
    __event__ = "struc_deleted"

    def __init__(self, sname):
        super(StrucDeletedEvent, self).__init__()
        self.sname = Event.decode(sname)

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        ida_struct.del_struc(ida_struct.get_struc(struc))

    @staticmethod
    def hook_handler(dispatcher_class, sptr):
        sname = ida_struct.get_struc_name(sptr.id)
        _send_packet(StrucDeletedEvent(sname))
        return 0


class StrucRenamedEvent(Event):
    __event__ = "struc_renamed"

    def __init__(self, oldname, newname):
        super(StrucRenamedEvent, self).__init__()
        self.oldname = Event.decode(oldname)
        self.newname = Event.decode(newname)

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.oldname))
        ida_struct.set_struc_name(struc, Event.encode(self.newname))

    @staticmethod
    def hook_handler(dispatcher_class, id, oldname, newname):
        _send_packet(StrucRenamedEvent(oldname, newname))
        return 0


class StrucCmtChangedEvent(Event):
    __event__ = "struc_cmt_changed"

    def __init__(self, sname, smname, cmt, repeatable_cmt):
        super(StrucCmtChangedEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.smname = Event.decode(smname)
        self.cmt = Event.decode(cmt)
        self.repeatable_cmt = repeatable_cmt

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        cmt = Event.encode(self.cmt if self.cmt else "")
        if self.smname:
            mptr = ida_struct.get_member_by_name(
                sptr, Event.encode(self.smname)
            )
            ida_struct.set_member_cmt(mptr, cmt, self.repeatable_cmt)
        else:
            ida_struct.set_struc_cmt(sptr.id, cmt, self.repeatable_cmt)

    @staticmethod
    def hook_handler(dispatcher_class, sptr, mptr):
        extra = {}
        sname = ida_struct.get_struc_name(sptr.id)
        fieldname = ida_struct.get_member_name(mptr.id)
        offset = 0 if mptr.unimem() else mptr.soff
        flag = mptr.flag
        nbytes = mptr.eoff if mptr.unimem() else mptr.eoff - mptr.soff
        mt = ida_nalt.opinfo_t()
        is_not_data = ida_struct.retrieve_member_info(mt, mptr)
        if is_not_data:
            if flag & ida_bytes.off_flag():
                extra["target"] = mt.ri.target
                extra["base"] = mt.ri.base
                extra["tdelta"] = mt.ri.tdelta
                extra["flags"] = mt.ri.flags
                _send_packet(
                    StrucMemberCreatedEvent(
                        sname, fieldname, offset, flag, nbytes, extra
                    )
                )
            # Is it really possible to create an enum?
            elif flag & ida_bytes.enum_flag():
                extra["serial"] = mt.ec.serial
                _send_packet(
                    StrucMemberCreatedEvent(
                        sname, fieldname, offset, flag, nbytes, extra
                    )
                )
            elif flag & ida_bytes.stru_flag():
                extra["id"] = mt.tid
                if flag & ida_bytes.strlit_flag():
                    extra["strtype"] = mt.strtype
                _send_packet(
                    StrucMemberCreatedEvent(
                        sname, fieldname, offset, flag, nbytes, extra
                    )
                )
        else:
            _send_packet(
                StrucMemberCreatedEvent(
                    sname, fieldname, offset, flag, nbytes, extra
                )
            )
        return 0


class StrucMemberCreatedEvent(Event):
    __event__ = "struc_member_created"

    def __init__(self, sname, fieldname, offset, flag, nbytes, extra):
        super(StrucMemberCreatedEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.fieldname = Event.decode(fieldname)
        self.offset = offset
        self.flag = flag
        self.nbytes = nbytes
        self.extra = extra

    def __call__(self):
        mt = ida_nalt.opinfo_t()
        if ida_bytes.is_struct(self.flag):
            mt.tid = self.extra["id"]
        if ida_bytes.is_off0(self.flag) or ida_bytes.is_off1(self.flag):
            mt.ri = ida_nalt.refinfo_t(
                self.extra["flags"],
                self.extra["base"],
                self.extra["target"],
                self.extra["tdelta"],
            )
        if ida_bytes.is_strlit(self.flag):
            mt.strtype = self.extra["strtype"]
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        ida_struct.add_struc_member(
            sptr,
            Event.encode(self.fieldname),
            self.offset,
            self.flag,
            mt,
            self.nbytes,
        )

    @staticmethod
    def hook_handler(dispatcher_class, sptr, off1, off2):
        sname = ida_struct.get_struc_name(sptr.id)
        _send_packet(StrucMemberDeletedEvent(sname, off2))
        return 0


class StrucMemberChangedEvent(Event):
    __event__ = "struc_member_changed"

    def __init__(self, sname, soff, eoff, flag, extra):
        super(StrucMemberChangedEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.soff = soff
        self.eoff = eoff
        self.flag = flag
        self.extra = extra

    def __call__(self):
        mt = ida_nalt.opinfo_t()
        if ida_bytes.is_struct(self.flag):
            mt.tid = self.extra["id"]
        if ida_bytes.is_off0(self.flag) or ida_bytes.is_off1(self.flag):
            mt.ri = ida_nalt.refinfo_t(
                self.extra["flags"],
                self.extra["base"],
                self.extra["target"],
                self.extra["tdelta"],
            )
        if ida_bytes.is_strlit(self.flag):
            mt.strtype = self.extra["strtype"]
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        ida_struct.set_member_type(
            sptr, self.soff, self.flag, mt, self.eoff - self.soff
        )

    @staticmethod
    def hook_handler(dispatcher_class, sptr, mptr, newname):
        sname = ida_struct.get_struc_name(sptr.id)
        offset = mptr.soff
        _send_packet(StrucMemberRenamedEvent(sname, offset, newname))
        return 0


class StrucMemberDeletedEvent(Event):
    __event__ = "struc_member_deleted"

    def __init__(self, sname, offset):
        super(StrucMemberDeletedEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.offset = offset

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        ida_struct.del_struc_member(sptr, self.offset)

    @staticmethod
    def hook_handler(dispatcher_class, id, repeatable_cmt):
        fullname = ida_struct.get_struc_name(id)
        if "." in fullname:
            sname, smname = fullname.split(".", 1)
        else:
            sname = fullname
            smname = ""
        cmt = ida_struct.get_struc_cmt(id, repeatable_cmt)
        _send_packet(
            StrucCmtChangedEvent(sname, smname, cmt, repeatable_cmt)
        )
        return 0


class StrucMemberRenamedEvent(Event):
    __event__ = "struc_member_renamed"

    def __init__(self, sname, offset, newname):
        super(StrucMemberRenamedEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.offset = offset
        self.newname = Event.decode(newname)

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        ida_struct.set_member_name(
            sptr, self.offset, Event.encode(self.newname)
        )

    @staticmethod
    def hook_handler(dispatcher_class, sptr, mptr):
        extra = {}

        sname = ida_struct.get_struc_name(sptr.id)
        soff = 0 if mptr.unimem() else mptr.soff
        flag = mptr.flag
        mt = ida_nalt.opinfo_t()
        is_not_data = ida_struct.retrieve_member_info(mt, mptr)
        if is_not_data:
            if flag & ida_bytes.off_flag():
                extra["target"] = mt.ri.target
                extra["base"] = mt.ri.base
                extra["tdelta"] = mt.ri.tdelta
                extra["flags"] = mt.ri.flags
                _send_packet(
                    StrucMemberChangedEvent(
                        sname, soff, mptr.eoff, flag, extra
                    )
                )
            elif flag & ida_bytes.enum_flag():
                extra["serial"] = mt.ec.serial
                _send_packet(
                    StrucMemberChangedEvent(
                        sname, soff, mptr.eoff, flag, extra
                    )
                )
            elif flag & ida_bytes.stru_flag():
                extra["id"] = mt.tid
                if flag & ida_bytes.strlit_flag():
                    extra["strtype"] = mt.strtype
                _send_packet(
                    StrucMemberChangedEvent(
                        sname, soff, mptr.eoff, flag, extra
                    )
                )
        else:
            _send_packet(
                StrucMemberChangedEvent(
                    sname, soff, mptr.eoff, flag, extra
                )
            )
        return 0


class ExpandingStrucEvent(Event):
    __event__ = "expanding_struc"

    def __init__(self, sname, offset, delta):
        super(ExpandingStrucEvent, self).__init__()
        self.sname = Event.decode(sname)
        self.offset = offset
        self.delta = delta

    def __call__(self):
        struc = ida_struct.get_struc_id(Event.encode(self.sname))
        sptr = ida_struct.get_struc(struc)
        ida_struct.expand_struc(sptr, self.offset, self.delta)

    @staticmethod
    def hook_handler(dispatcher_class, sptr, offset, delta):
        sname = ida_struct.get_struc_name(sptr.id)
        _send_packet(ExpandingStrucEvent(sname, offset, delta))
        return 0


class SegmAddedEvent(Event):
    __event__ = "segm_added_event"

    def __init__(
            self,
            name,
            class_,
            start_ea,
            end_ea,
            orgbase,
            align,
            comb,
            perm,
            bitness,
            flags,
    ):
        super(SegmAddedEvent, self).__init__()
        self.name = Event.decode(name)
        self.class_ = Event.decode(class_)
        self.start_ea = start_ea
        self.end_ea = end_ea
        self.orgbase = orgbase
        self.align = align
        self.comb = comb
        self.perm = perm
        self.bitness = bitness
        self.flags = flags

    def __call__(self):
        seg = ida_segment.segment_t()
        seg.start_ea = self.start_ea
        seg.end_ea = self.end_ea
        seg.orgbase = self.orgbase
        seg.align = self.align
        seg.comb = self.comb
        seg.perm = self.perm
        seg.bitness = self.bitness
        seg.flags = self.flags
        ida_segment.add_segm_ex(
            seg,
            Event.encode(self.name),
            Event.encode(self.class_),
            ida_segment.ADDSEG_QUIET | ida_segment.ADDSEG_NOSREG,
        )

    @staticmethod
    def hook_handler(dispatcher_class, s):
        _send_packet(
            SegmAddedEvent(
                ida_segment.get_segm_name(s),
                ida_segment.get_segm_class(s),
                s.start_ea,
                s.end_ea,
                s.orgbase,
                s.align,
                s.comb,
                s.perm,
                s.bitness,
                s.flags,
            )
        )
        return 0


class SegmDeletedEvent(Event):
    __event__ = "segm_deleted_event"

    def __init__(self, ea):
        super(SegmDeletedEvent, self).__init__()
        self.ea = ea

    def __call__(self):
        ida_segment.del_segm(
            self.ea, ida_segment.SEGMOD_KEEP | ida_segment.SEGMOD_SILENT
        )

    # This hook lack of disable addresses option
    @staticmethod
    def hook_handler(dispatcher_class, start_ea, end_ea):
        _send_packet(SegmDeletedEvent(start_ea))
        return 0


class SegmStartChangedEvent(Event):
    __event__ = "segm_start_changed_event"

    def __init__(self, newstart, ea):
        super(SegmStartChangedEvent, self).__init__()
        self.newstart = newstart
        self.ea = ea

    def __call__(self):
        ida_segment.set_segm_start(self.ea, self.newstart, 0)

    @staticmethod
    def hook_handler(dispatcher_class, s, oldstart):
        _send_packet(SegmStartChangedEvent(s.start_ea, oldstart))
        return 0


class SegmEndChangedEvent(Event):
    __event__ = "segm_end_changed_event"

    def __init__(self, newend, ea):
        super(SegmEndChangedEvent, self).__init__()
        self.newend = newend
        self.ea = ea

    def __call__(self):
        ida_segment.set_segm_end(self.ea, self.newend, 0)

    @staticmethod
    def hook_handler(dispatcher_class, s, oldend):
        _send_packet(SegmEndChangedEvent(s.end_ea, s.start_ea))
        return 0


class SegmNameChangedEvent(Event):
    __event__ = "segm_name_changed_event"

    def __init__(self, ea, name):
        super(SegmNameChangedEvent, self).__init__()
        self.ea = ea
        self.name = Event.decode(name)

    def __call__(self):
        seg = ida_segment.getseg(self.ea)
        ida_segment.set_segm_name(seg, Event.encode(self.name))

    @staticmethod
    def hook_handler(dispatcher_class, s, name):
        _send_packet(SegmNameChangedEvent(s.start_ea, name))
        return 0


class SegmClassChangedEvent(Event):
    __event__ = "segm_class_changed_event"

    def __init__(self, ea, sclass):
        super(SegmClassChangedEvent, self).__init__()
        self.ea = ea
        self.sclass = Event.decode(sclass)

    def __call__(self):
        seg = ida_segment.getseg(self.ea)
        ida_segment.set_segm_class(seg, Event.encode(self.sclass))

    @staticmethod
    def hook_handler(dispatcher_class, s, sclass):
        _send_packet(SegmClassChangedEvent(s.start_ea, sclass))
        return 0


class SegmAttrsUpdatedEvent(Event):
    __event__ = "segm_attrs_updated_event"

    def __init__(self, ea, perm, bitness):
        super(SegmAttrsUpdatedEvent, self).__init__()
        self.ea = ea
        self.perm = perm
        self.bitness = bitness

    def __call__(self):
        s = ida_segment.getseg(self.ea)
        s.perm = self.perm
        s.bitness = self.bitness
        s.update()

    @staticmethod
    def hook_handler(dispatcher_class, s):
        _send_packet(
            SegmAttrsUpdatedEvent(s.start_ea, s.perm, s.bitness)
        )
        return 0


class SegmMoved(Event):
    __event__ = "segm_moved_event"

    def __init__(self, from_ea, to_ea, changed_netmap):
        super(SegmMoved, self).__init__()
        self.from_ea = from_ea
        self.to_ea = to_ea
        self.changed_netmap = changed_netmap

    def __call__(self):
        flags = ida_segment.MFS_NETMAP if self.changed_netmap else 0
        s = ida_segment.getseg(self.from_ea)
        ida_segment.move_segm(s, self.to_ea, flags)

    @staticmethod
    def hook_handler(dispatcher_class, from_ea, to_ea, size, changed_netmap):
        _send_packet(SegmMoved(from_ea, to_ea, changed_netmap))
        return 0


class UndefinedEvent(Event):
    __event__ = "undefined"

    def __init__(self, ea):
        super(UndefinedEvent, self).__init__()
        self.ea = ea

    def __call__(self):
        ida_bytes.del_items(self.ea)

    @staticmethod
    def hook_handler(dispatcher_class, ea):
        _send_packet(UndefinedEvent(ea), True)
        return ida_idp.IDP_Hooks.ev_undefine(dispatcher_class, ea)


class BytePatchedEvent(Event):
    __event__ = "byte_patched"

    def __init__(self, ea, value):
        super(BytePatchedEvent, self).__init__()
        self.ea = ea
        self.value = value

    def __call__(self):
        ida_bytes.patch_byte(self.ea, self.value)

    @staticmethod
    def hook_handler(dispatcher_class, ea, old_value):
        _send_packet(
            BytePatchedEvent(ea, ida_bytes.get_wide_byte(ea))
        )
        return 0


class SgrChanged(Event):
    __event__ = "sgr_changed"

    @staticmethod
    def get_sreg_ranges(rg):
        sreg_ranges = []
        sreg_ranges_qty = ida_segregs.get_sreg_ranges_qty(rg)
        for n in range(sreg_ranges_qty):
            sreg_range = ida_segregs.sreg_range_t()
            ida_segregs.getn_sreg_range(sreg_range, rg, n)
            sreg_ranges.append(
                (
                    sreg_range.start_ea,
                    sreg_range.end_ea,
                    sreg_range.val,
                    sreg_range.tag,
                )
            )
        return sreg_ranges

    def __init__(self, rg, sreg_ranges):
        super(SgrChanged, self).__init__()
        self.rg = rg
        self.sreg_ranges = sreg_ranges

    def __call__(self):
        new_ranges = {r[0]: r for r in self.sreg_ranges}
        old_ranges = {r[0]: r for r in SgrChanged.get_sreg_ranges(self.rg)}

        start_eas = sorted(
            set(list(new_ranges.keys()) + list(old_ranges.keys()))
        )
        for start_ea in start_eas:
            new_range = new_ranges.get(start_ea, None)
            old_range = old_ranges.get(start_ea, None)

            if new_range and not old_range:
                _, __, val, tag = new_range
                ida_segregs.split_sreg_range(start_ea, self.rg, val, tag, True)

            if not new_range and old_range:
                ida_segregs.del_sreg_range(start_ea, self.rg)

            if new_range and old_range:
                _, __, new_val, new_tag = new_range
                _, __, old_val, old_tag = old_range
                if new_val != old_val or new_tag != old_tag:
                    ida_segregs.split_sreg_range(
                        start_ea, self.rg, new_val, new_tag, True
                    )

        ida_kernwin.request_refresh(ida_kernwin.IWID_SEGREGS)

    @staticmethod
    def hook_handler(dispatcher_class, start_ea, end_ea, regnum, value, old_value, tag):
        # FIXME: sgr_changed is not triggered when a segment register is
        # being deleted by the user, so we need to sent the complete list
        sreg_ranges = SgrChanged.get_sreg_ranges(regnum)
        _send_packet(SgrChanged(regnum, sreg_ranges))
        return 0


class GenRegvarDefEvent(Event):
    __event__ = "gen_regvar_def"

    def __init__(self, ea, canonical_name, new_name, cmt):
        super(GenRegvarDefEvent, self).__init__()
        self.ea = ea
        self.canonical_name = Event.decode(canonical_name)
        self.new_name = Event.decode(new_name)
        self.cmt = Event.decode(cmt)

    def __call__(self):
        func = ida_funcs.get_func(self.ea)
        ida_frame.add_regvar(
            func,
            func.start_ea,
            func.end_ea,
            Event.encode(self.canonical_name),
            Event.encode(self.new_name),
            Event.encode(self.cmt),
        )

    @staticmethod
    def hook_handler(dispatcher_class, outctx, v):
        _send_packet(
            GenRegvarDefEvent(outctx.bin_ea, v.canon, v.user, v.cmt)
        )
        return ida_idp.IDP_Hooks.ev_gen_regvar_def(dispatcher_class, outctx, v)


