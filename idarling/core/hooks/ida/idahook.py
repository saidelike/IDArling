import logging

logger = logging.getLogger("idarling.hook.ida")

from .idaevents import *
HOOK_DISPATCH_TABLE = {
    "make_code": MakeCodeEvent,
    "make_data": MakeDataEvent,
    "renamed": RenamedEvent,
    "func_added": FuncAddedEvent,
    "deleting_func": DeletingFuncEvent,
    "set_func_start": SetFuncStartEvent,
    "set_func_end": SetFuncEndEvent,
    "func_tail_appended": FuncTailAppendedEvent,
    "func_tail_deleted": FuncTailDeletedEvent,
    "tail_owner_changed": TailOwnerChangedEvent,
    "cmt_changed": CmtChangedEvent,
    "range_cmt_changed": RangeCmtChangedEvent,
    "extra_cmt_changed": ExtraCmtChangedEvent,
    "ti_changed": TiChangedEvent,
    "local_types_changed": LocalTypesChangedEvent,
    "op_type_changed": OpTypeChangedEvent,
    "enum_created": EnumCreatedEvent,
    "deleting_enum": EnumDeletedEvent,
    "renaming_enum": EnumRenamedEvent,
    "enum_bf_changed": EnumBfChangedEvent,
    "enum_cmt_changed": EnumCmtChangedEvent,
    "enum_member_created": EnumMemberCreatedEvent,
    "deleting_enum_member": EnumMemberDeletedEvent,
    "struc_created": StrucCreatedEvent,
    "deleting_struc": StrucDeletedEvent,
    "renaming_struc": StrucRenamedEvent,
    "struc_member_created": StrucCmtChangedEvent,
    "struc_member_deleted": StrucMemberCreatedEvent,
    "renaming_struc_member": StrucMemberChangedEvent,
    "struc_cmt_changed": StrucMemberDeletedEvent,
    "struc_member_changed": StrucMemberRenamedEvent,
    "expanding_struc": ExpandingStrucEvent,
    "segm_added": SegmAddedEvent,
    "segm_deleted": SegmDeletedEvent,
    "segm_start_changed": SegmStartChangedEvent,
    "segm_end_changed": SegmEndChangedEvent,
    "segm_name_changed": SegmNameChangedEvent,
    "segm_class_changed": SegmClassChangedEvent,
    "segm_attrs_updated": SegmAttrsUpdatedEvent,
    "segm_moved": SegmMoved,
    "byte_patched": BytePatchedEvent,
    "sgr_changed": SgrChanged,
    "ev_undefine": UndefinedEvent,
    #"ev_adjust_argloc": AdjustArglocEvent,
    "ev_gen_regvar_def": GenRegvarDefEvent,
}


class IDAHooks(ida_idp.IDB_Hooks, ida_idp.IDP_Hooks, object):
    def __init__(self):
        ida_idp.IDB_Hooks.__init__(self)
        ida_idp.IDP_Hooks.__init__(self)

    def __getattribute__(self, item):
        if item in type(self).__dict__:
            return object.__getattribute__(self, item)
        elif item in HOOK_DISPATCH_TABLE:
            evt = HOOK_DISPATCH_TABLE[item]
            def _hook_wrapper(*args, **kwargs):
                return evt.hook_handler(self, *args, **kwargs)

            return _hook_wrapper
        else:
            try:
                return super(ida_idp.IDB_Hooks, self).__getattribute__(item)
            except KeyError:
                return super(ida_idp.IDP_Hooks, self).__getattribute__(item)

    def hook(self, *args):
        ida_idp.IDB_Hooks.hook(self)
        ida_idp.IDP_Hooks.hook(self)

    def unhook(self, *args):
        ida_idp.IDB_Hooks.unhook(self)
        ida_idp.IDP_Hooks.unhook(self)


