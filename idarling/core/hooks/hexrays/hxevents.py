import ida_funcs
import ida_hexrays
import ida_kernwin
import ida_pro
import ida_typeinf

from idarling.core.hooks.ida.idaevents import Event
from idarling.shared.idautils import refresh_pseudocode_view


class HexRaysEvent(Event):
    pass


class UserLabelsEvent(HexRaysEvent):
    __event__ = "user_labels"

    def __init__(self, ea, labels):
        super(UserLabelsEvent, self).__init__()
        self.ea = ea
        self.labels = labels

    def __call__(self):
        labels = ida_hexrays.user_labels_new()
        for org_label, name in self.labels:
            name = Event.encode(name)
            ida_hexrays.user_labels_insert(labels, org_label, name)
        ida_hexrays.save_user_labels(self.ea, labels)
        refresh_pseudocode_view(self.ea)


class UserCmtsEvent(HexRaysEvent):
    __event__ = "user_cmts"

    def __init__(self, ea, cmts):
        super(UserCmtsEvent, self).__init__()
        self.ea = ea
        self.cmts = cmts

    def __call__(self):
        cmts = ida_hexrays.user_cmts_new()
        for (tl_ea, tl_itp), cmt in self.cmts:
            tl = ida_hexrays.treeloc_t()
            tl.ea = tl_ea
            tl.itp = tl_itp
            cmts.insert(tl, ida_hexrays.citem_cmt_t(Event.encode(cmt)))
        ida_hexrays.save_user_cmts(self.ea, cmts)
        refresh_pseudocode_view(self.ea)


class UserIflagsEvent(HexRaysEvent):
    __event__ = "user_iflags"

    def __init__(self, ea, iflags):
        super(UserIflagsEvent, self).__init__()
        self.ea = ea
        self.iflags = iflags

    def __call__(self):
        # FIXME: Hey-Rays bindings are currently broken
        # iflags = ida_hexrays.user_iflags_new()
        # for (cl_ea, cl_op), f in self.iflags:
        #     cl = ida_hexrays.citem_locator_t(cl_ea, cl_op)
        #     iflags.insert(cl, f)
        # ida_hexrays.save_user_iflags(self.ea, iflags)

        ida_hexrays.save_user_iflags(self.ea, ida_hexrays.user_iflags_new())
        refresh_pseudocode_view(self.ea)

        cfunc = ida_hexrays.decompile(self.ea)
        for (cl_ea, cl_op), f in self.iflags:
            cl = ida_hexrays.citem_locator_t(cl_ea, cl_op)
            cfunc.set_user_iflags(cl, f)
        cfunc.save_user_iflags()
        refresh_pseudocode_view(self.ea)


class UserLvarSettingsEvent(HexRaysEvent):
    __event__ = "user_lvar_settings"

    def __init__(self, ea, lvar_settings):
        super(UserLvarSettingsEvent, self).__init__()
        self.ea = ea
        self.lvar_settings = lvar_settings

    def __call__(self):
        lvinf = ida_hexrays.lvar_uservec_t()
        lvinf.lvvec = ida_hexrays.lvar_saved_infos_t()
        for lv in self.lvar_settings["lvvec"]:
            lvinf.lvvec.push_back(
                UserLvarSettingsEvent._get_lvar_saved_info(lv)
            )
        lvinf.sizes = ida_pro.intvec_t()
        if "sizes" in self.lvar_settings:
            for i in self.lvar_settings["sizes"]:
                lvinf.sizes.push_back(i)
        lvinf.lmaps = ida_hexrays.lvar_mapping_t()
        for key, val in self.lvar_settings["lmaps"]:
            key = UserLvarSettingsEvent._get_lvar_locator(key)
            val = UserLvarSettingsEvent._get_lvar_locator(val)
            ida_hexrays.lvar_mapping_insert(lvinf.lmaps, key, val)
        lvinf.stkoff_delta = self.lvar_settings["stkoff_delta"]
        lvinf.ulv_flags = self.lvar_settings["ulv_flags"]
        ida_hexrays.save_user_lvar_settings(self.ea, lvinf)
        refresh_pseudocode_view(self.ea)

    @staticmethod
    def _get_lvar_saved_info(dct):
        lv = ida_hexrays.lvar_saved_info_t()
        lv.ll = UserLvarSettingsEvent._get_lvar_locator(dct["ll"])
        lv.name = Event.encode(dct["name"])
        lv.type = UserLvarSettingsEvent._get_tinfo(dct["type"])
        lv.cmt = Event.encode(dct["cmt"])
        lv.flags = dct["flags"]
        return lv

    @staticmethod
    def _get_tinfo(dct):
        type, fields, fldcmts = dct
        type = Event.encode_bytes(type)
        fields = Event.encode_bytes(fields)
        fldcmts = Event.encode_bytes(fldcmts)

        type_ = ida_typeinf.tinfo_t()
        if type is not None:
            type_.deserialize(None, type, fields, fldcmts)
        return type_

    @staticmethod
    def _get_lvar_locator(dct):
        ll = ida_hexrays.lvar_locator_t()
        ll.location = UserLvarSettingsEvent._get_vdloc(dct["location"])
        ll.defea = dct["defea"]
        return ll

    @staticmethod
    def _get_vdloc(dct):
        location = ida_hexrays.vdloc_t()
        if dct["atype"] == ida_typeinf.ALOC_NONE:
            pass
        elif dct["atype"] == ida_typeinf.ALOC_STACK:
            location.set_stkoff(dct["stkoff"])
        elif dct["atype"] == ida_typeinf.ALOC_DIST:
            pass  # FIXME: Not supported
        elif dct["atype"] == ida_typeinf.ALOC_REG1:
            location.set_reg1(dct["reg1"])
        elif dct["atype"] == ida_typeinf.ALOC_REG2:
            location.set_reg2(dct["reg1"], dct["reg2"])
        elif dct["atype"] == ida_typeinf.ALOC_RREL:
            pass  # FIXME: Not supported
        elif dct["atype"] == ida_typeinf.ALOC_STATIC:
            location.set_ea(dct["ea"])
        elif dct["atype"] == ida_typeinf.ALOC_CUSTOM:
            pass  # FIXME: Not supported
        return location


class UserNumformsEvent(HexRaysEvent):
    __event__ = "user_numforms"

    def __init__(self, ea, numforms):
        super(UserNumformsEvent, self).__init__()
        self.ea = ea
        self.numforms = numforms

    def __call__(self):
        numforms = ida_hexrays.user_numforms_new()
        for _ol, _nf in self.numforms:
            ol = ida_hexrays.operand_locator_t(_ol["ea"], _ol["opnum"])
            nf = ida_hexrays.number_format_t()
            nf.flags = _nf["flags"]
            nf.opnum = Event.encode(_nf["opnum"])
            nf.props = Event.encode(_nf["props"])
            nf.serial = _nf["serial"]
            nf.org_nbytes = Event.encode(_nf["org_nbytes"])
            nf.type_name = Event.encode(_nf["type_name"])
            ida_hexrays.user_numforms_insert(numforms, ol, nf)
        ida_hexrays.save_user_numforms(self.ea, numforms)
        refresh_pseudocode_view(self.ea)
