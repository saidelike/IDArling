import ctypes
import logging

import ida_auto
import ida_idp
import ida_kernwin

from idarling.managers import *

from idarling.shared.utils import Singleton

logger = logging.getLogger("idarling.hook.manager")

def _send_packet(event, force=False):
    """Sends a packet to the server."""
    # Check if it comes from the auto-analyzer
    if force or ida_auto.get_auto_state() == ida_auto.AU_NONE:
        NetworkManager().send_packet(event)
    else:
        logger.debug("Ignoring a packet")


@Singleton
class HookManager(object):
    def __init__(self):
        self._hooked = False

        class IDBHooksCore(ida_idp.IDB_Hooks):
            def closebase(self):
                logger.trace("Closebase hook")

                SessionManager().leave_session()
                SessionManager().project = None
                SessionManager().database = None
                SessionManager().tick = 0
                return 0

        class IDPHooksCore(ida_idp.IDP_Hooks):
            def ev_get_bg_color(self, color, ea):
                logger.trace("Get bg color hook")
                value = InterfaceManager().painter.get_bg_color(ea)
                if value is not None:
                    ctypes.c_uint.from_address(long(color)).value = value
                    return 1
                return 0

            def auto_queue_empty(self, _):
                logger.debug("Auto queue empty hook")
                if ida_auto.get_auto_state() == ida_auto.AU_NONE:
                    client = NetworkManager().client
                    if client:
                        client.call_events()

        class UIHooksCore(ida_kernwin.UI_Hooks):
            def ready_to_run(self):
                logger.trace("Ready to run hook")
                SessionManager().join_session()
                InterfaceManager().painter.ready_to_run()

            def get_ea_hint(self, ea):
                logger.trace("Get ea hint hook")
                return InterfaceManager().painter.get_ea_hint(ea)

            def widget_visible(self, widget):
                logger.trace("Widget visible")
                InterfaceManager().painter.widget_visible(widget)

            def current_widget_changed(self, widget, prev_widget):
                logger.trace("Current widget changed")
                InterfaceManager().painter.current_widget_changed(widget, prev_widget)

            def create_desktop_widget(self, ttl, cfg):
                logger.trace("Create desktop widget")
                InterfaceManager().painter.create_desktop_widget(ttl, cfg)

        class ViewHooksCore(ida_kernwin.View_Hooks):
            def view_loc_changed(self, view, now, was):
                logger.trace("View loc changed hook")
                if now.plce.toea() != was.plce.toea():
                    # don't update location when following
                    if InterfaceManager().followed is None:
                        UserManager().change_color(now.plce.toea())

        self._global_hooks = [IDBHooksCore(), IDPHooksCore(), UIHooksCore(), ViewHooksCore()]

        from idarling.core.hooks.ida.idahook import IDAHooks
        from idarling.core.hooks.hexrays.hxhook import HexRaysHooks
        self._session_hooks = [IDAHooks(), HexRaysHooks()]

    def global_hook(self):
        # Instantiate the hooks
        logger.debug("Installing core hooks")
        for core in self._global_hooks:
            core.hook()
        return True

    def global_unhook(self):
        logger.debug("Uninstalling core hooks")
        self.session_unhook()
        for core in reversed(self._global_hooks):
            core.unhook()
        return True

    def session_hook(self):
        """Install all the user events hooks."""
        if self._hooked:
            return

        logger.debug("Installing hooks")
        for hook in self._session_hooks:
            hook.hook()
        self._hooked = True

    def session_unhook(self):
        """Uninstall all the user events hooks."""
        if not self._hooked:
            return

        logger.debug("Uninstalling hooks")
        for hook in reversed(self._session_hooks):
            hook.unhook()
        self._hooked = False
