from idarling.network.socketimpl.packets import (
    Command,
    Container,
    DefaultCommand,
    ParentCommand,
    Query as IQuery,
    Reply as IReply,
)
from idarling.shared.proto import NODETYPE_PROJECT


def encode_node(node, target):
    ret = node.build(target)
    target["subnodes"] = []
    return ret

def decode_node(dct):
    from idarling.core.project.project import TreeNode, Project
    ret = TreeNode.new(dct)
    if ret.nodetype == NODETYPE_PROJECT:
        ret = Project.new(dct)
    return ret

class AddTreeNode(ParentCommand):
    __command__ = 'add_tree_node'

    class Query(IQuery, Command):
        def __init__(self, parentnode, newnode):
            super(AddTreeNode.Query, self).__init__()
            self.parentnode = parentnode
            self.newnode = newnode

        def build_command(self, dct):
            encode_node(self.newnode, dct["newnode"])
            encode_node(self.parentnode, dct["parentnode"])

        def parse_command(self, dct):
            self.newnode = decode_node(dct['newnode'])
            self.parentnode = decode_node(dct['parentnode'])

    class Reply(IReply, Command):
        def __init__(self, query, newnode, err):
            super(AddTreeNode.Reply, self).__init__(query)
            self.newnode = newnode
            self.err = err

        def build_command(self, dct):
            if self.newnode:
                encode_node(self.newnode, dct["newnode"])
            else:
                dct["newnode"] = None
            dct["err"] = self.err

        def parse_command(self, dct):
            if not dct['newnode']:
                self.newnode = None
            else:
                self.newnode = decode_node(dct['newnode'])
            self.err = dct["err"]

class MoveTreeNode(ParentCommand):
    __command__ = 'move_tree_node'

    class Query(IQuery, DefaultCommand):
        def __init__(self, node, targetnode, newname):
            super(MoveTreeNode.Query, self).__init__()
            self.node = node
            self.targetnode = targetnode
            self.newname = newname

        def build_command(self, dct):
            super(MoveTreeNode.Query, self).build_command(dct)
            dct["node"] = {}
            encode_node(self.node, dct["node"])
            if self.targetnode:
                dct["targetnode"] = {}
                encode_node(self.targetnode, dct["targetnode"])
            else:
                dct["targetnode"] = None

        def parse_command(self, dct):
            super(MoveTreeNode.Query, self).parse_command(dct)
            self.node = decode_node(dct['node'])
            self.targetnode = None
            if dct['targetnode']:
                self.targetnode = decode_node(dct['targetnode'])

    class Reply(IReply, Command):
        def __init__(self, query, newnode):
            super(MoveTreeNode.Reply, self).__init__(query)
            self.newnode = newnode

        def build_command(self, dct):
            if self.newnode:
                encode_node(self.newnode, dct["newnode"])

        def parse_command(self, dct):
            from idarling.core.project.project import TreeNode, Project
            if not dct['newnode']:
                self.newnode = None
                return
            self.newnode = decode_node(dct['newnode'])

DELSTATUS_SUCCESS = "success"
DELSTATUS_FAIL = "fail"

class DelTreeNode(ParentCommand):
    __command__ = 'del_tree_node'

    class Query(IQuery, DefaultCommand):
        def __init__(self, node):
            super(DelTreeNode.Query, self).__init__()
            self.node = node

        def build_command(self, dct):
            super(DelTreeNode.Query, self).build_command(dct)
            dct["node"] = {}
            encode_node(self.node, dct["node"])

        def parse_command(self, dct):
            super(DelTreeNode.Query, self).parse_command(dct)
            self.node = decode_node(dct['node'])

    class Reply(IReply, Command):
        def __init__(self, query, status):
            super(DelTreeNode.Reply, self).__init__(query)
            self.status = status

class ListTree(ParentCommand):
    __command__ = "list_tree"

    class Query(IQuery, Command):
        def __init__(self, node):
            super(ListTree.Query, self).__init__()
            self.node = node

        def build_command(self, dct):
            encode_node(self.node, dct["node"])

        def parse_command(self, dct):
            self.node = decode_node(dct['node'])

    class Reply(IReply, Command):
        def __init__(self, query, tree):
            super(ListTree.Reply, self).__init__(query)
            self.tree = tree

        def build_command(self, dct):
            dct["tree"] = [encode_node(node, {}) for node in self.tree]

        def parse_command(self, dct):
            from idarling.core.project.project import TreeNode, Project
            self.tree = []
            for nodedata in dct["tree"]:
                node = decode_node(nodedata)
                self.tree.append(node)
#'''
class ListProjects(ParentCommand):
    __command__ = "list_projects"

    class Query(IQuery, DefaultCommand):
        pass

    class Reply(IReply, Command):
        def __init__(self, query, projects):
            super(ListProjects.Reply, self).__init__(query)
            self.projects = projects

        def build_command(self, dct):
            dct["projects"] = [project.build({}) for project in self.projects]

        def parse_command(self, dct):
            from idarling.core.project.project import Project
            self.projects = [
                Project.new(project) for project in dct["projects"]
            ]

class CreateProject(ParentCommand):
    __command__ = "create_project"

    class Query(IQuery, Command):
        def __init__(self, project):
            super(CreateProject.Query, self).__init__()
            self.project = project

        def build_command(self, dct):
            self.project.build(dct["project"])

        def parse_command(self, dct):
            from idarling.core.project.project import Project
            self.project = Project.new(dct["project"])

    class Reply(IReply, Command):
        pass
#'''

class ListDatabases(ParentCommand):
    __command__ = "list_databases"

    class Query(IQuery, DefaultCommand):
        def __init__(self, project):
            super(ListDatabases.Query, self).__init__()
            self.project = project

        def build_command(self, dct):
            encode_node(self.project, dct["project"])

        def parse_command(self, dct):
            self.project = decode_node(dct["project"])

    class Reply(IReply, Command):
        def __init__(self, query, databases):
            super(ListDatabases.Reply, self).__init__(query)
            self.databases = databases

        def build_command(self, dct):
            dct["databases"] = [
                database.build({}) for database in self.databases
            ]

        def parse_command(self, dct):
            from idarling.core.project.project import Database
            self.databases = [
                Database.new(database) for database in dct["databases"]
            ]


class CreateDatabase(ParentCommand):
    __command__ = "create_database"

    class Query(IQuery, Container, DefaultCommand):
        def __init__(self, project, database_name):
            super(CreateDatabase.Query, self).__init__()
            self.project = project
            self.database_name = database_name

        def build_command(self, dct):
            super(CreateDatabase.Query, self).build_command(dct)
            dct["project"] = {}
            encode_node(self.project, dct["project"])

        def parse_command(self, dct):
            super(CreateDatabase.Query, self).parse_command(dct)
            self.project = decode_node(dct["project"])
            #self.database_name = dct["project"]

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, database):
            super(CreateDatabase.Reply, self).__init__(query)
            self.database = database

        def build_command(self, dct):
            super(CreateDatabase.Reply, self).build_command(dct)
            if self.database:
                dct["database"] = {}
                self.database.build(dct["database"])

        def parse_command(self, dct):
            super(CreateDatabase.Reply, self).parse_command(dct)
            from idarling.core.project.project import Database
            self.database = None
            if dct["database"]:
                self.database = Database.new(dct["database"])


class UpdateFile(ParentCommand):
    __command__ = "update_file"

    class Query(IQuery, Container, DefaultCommand):
        def __init__(self, project, database_id):
            super(UpdateFile.Query, self).__init__()
            self.project = project
            self.database_id = database_id

        def build_command(self, dct):
            super(UpdateFile.Query, self).build_command(dct)
            dct["project"] = {}
            self.project.build(dct["project"])

        def parse_command(self, dct):
            super(UpdateFile.Query, self).parse_command(dct)
            from idarling.core.project.project import Project
            self.project = Project.new(dct["project"])

    class Reply(IReply, Command):
        pass


class DownloadFile(ParentCommand):
    __command__ = "download_file"

    class Query(IQuery, DefaultCommand):
        def __init__(self, project, database_id):
            super(DownloadFile.Query, self).__init__()
            self.project = project
            self.database_id = database_id

        def build_command(self, dct):
            super(DownloadFile.Query, self).build_command(dct)
            dct["project"] = {}
            self.project.build(dct["project"])

        def parse_command(self, dct):
            super(DownloadFile.Query, self).parse_command(dct)
            from idarling.core.project.project import Project
            self.project = Project.new(dct["project"])

    class Reply(IReply, Container, Command):
        pass
