from idarling.managers import *
from idarling.shared.proto import DatabaseProto, ProjectProto, TreeNodeProto, NODETYPE_DIRECTORY, PathProto
from idarling.shared.utils import Singleton, explode_path
from idarling.core.project.projectcommands import *
import os
import logging
logger = logging.getLogger("idarling.core.project.manager")

class Path(PathProto):
    def __init__(self, disppath, idpath):
        super(TreeNode, self).__init__(disppath, idpath)

    def get_display_path(self):
        return '/' + '/'.join(self.disppath)

class TreeNodeMixIn(object):
    def get_name(self):
        return os.path.basename(self.nodepath)

    def get_dir(self):
        return os.path.dirname(self.nodepath)

    def rename(self, newname, cb):
        self.move(None, newname , cb)

    def move(self, targetnode, newname, cb):
        d = NetworkManager().send_packet(MoveTreeNode.Query(self, targetnode, newname))

        def _moved(reply):
            cb(reply)

        d.add_callback(_moved)
        d.add_errback(logger.exception)

    def delete(self, cb):
        d = NetworkManager().send_packet(DelTreeNode.Query(self))

        def _deleted(reply):
            cb(reply)

        d.add_callback(_deleted)
        d.add_errback(logger.exception)

class TreeNode(TreeNodeProto, TreeNodeMixIn):
    def __init__(self, nodepath, nodeid, mdate, subnodes, nodetype):
        super(TreeNode, self).__init__(nodepath, nodeid, mdate, subnodes, nodetype)
        self.is_synced = False

    def add_node(self, parentnode, newnode, cb):
        d = NetworkManager().send_packet(AddTreeNode.Query(parentnode, newnode))

        def _directory_added(reply):
            cb(reply)

        d.add_callback(_directory_added)
        d.add_errback(logger.exception)
        pass

    def add_directory(self, dirname, cb):
        newnode = TreeNode(self.nodepath + '/' + dirname, None, None, [], NODETYPE_DIRECTORY)
        self.add_node(self, newnode, cb)
        pass

    def add_project(self, projname, file, hash, type, cb):
        newnode = Project(self.nodepath + '/' + projname, None, None, file, hash, type)
        self.add_node(self, newnode, cb)
        pass

    def list_tree(self, cb=None):
        d = NetworkManager().send_packet(ListTree.Query(self))

        def _tree_listed(reply):
            self.subnodes = reply.tree
            ret_tree = reply.tree
            if cb:
                cb(ret_tree)

        d.add_callback(_tree_listed)
        d.add_errback(logger.exception)
        pass


class Database(DatabaseProto):
    """
    IDBs are organized into projects and databases. A database corresponds to
    a revision of an IDB. It has a project, a name, a date of creation, and a
    current tick (events) count.
    """

    def __init__(self, id, name, date, tick=-1):
        super(Database, self).__init__(id, name, date, tick)


class Project(ProjectProto, TreeNodeMixIn):
    """
    IDBs are organized into projects and databases. A project regroups
    multiples revisions of an IDB. It has a name, the hash of the input file,
    the path to the input file, the type of the input file and the date of the
    database creation.
    """

    def __init__(self, nodepath, nodeid, mdate, filename, filehash, filetype):
        """
        Init a project instance
        :param name: input file name
        """
        super(Project, self).__init__(nodepath, nodeid, mdate, filename, filehash, filetype)

    def list_database(self, cb=None):
        def callback_wrap(reply):
            ret_db = reply.databases
            return cb(ret_db)

        d = NetworkManager().send_packet(
            ListDatabases.Query(self)
        )
        if d:
            d.add_callback(callback_wrap)
            d.add_errback(logger.exception)

    def create_database(self, database_name, upback=None, callback=None):
        from idarling.shared.idautils import get_database_data
        database_data = get_database_data()
        # Create the packet that will hold the file
        packet = CreateDatabase.Query(self, database_name)
        packet.content = database_data
        # Send the packet to upload the file
        packet.upback = upback
        d = NetworkManager().send_packet(packet)
        if d:
            d.add_callback(callback)
            d.add_errback(logger.exception)
            return True
        else:
            return False

    def upload_database(self, database_id, upback=None, callback=None, errback=logger.exception):
        from idarling.shared.idautils import get_database_data
        database_data = get_database_data()
        # Create the packet that will hold the file
        packet = UpdateFile.Query(self, database_id)
        packet.content = database_data
        # Send the packet to upload the file
        packet.upback = upback
        d = NetworkManager().send_packet(packet)
        if d:
            d.add_callback(callback)
            d.add_errback(errback)
            return True
        else:
            return False

    def reply_autosave_database(self, query):
        # Create the packet that will hold the file
        if query.project.nodeid != SessionManager().project.nodeid or query.database_id != SessionManager().database.id:
            logger.warning("Server queried project: %s database: %s, but we only have project: %s database: %s",
                           query.project, query.database_id,
                           SessionManager().project, SessionManager().database)
            return

        reply = DownloadFile.Reply(query)

        from idarling.shared.idautils import get_database_data
        database_data = get_database_data()
        reply.content = database_data
        NetworkManager().send_packet(reply)

    def download_database(self, database_id, downback=None, callback=None, errback=logger.exception):
        # Send a packet to download the file
        packet = DownloadFile.Query(self, database_id)
        def set_download_callback(reply):
            reply.downback = downback
        d = NetworkManager().send_packet(packet)
        d.add_initback(set_download_callback)
        d.add_callback(callback)
        d.add_errback(errback)

    def delete_database(self):
        pass

@Singleton
class ProjectManager(object):
    def __init__(self):
        self.root_node = TreeNode("/", None, None, [], NODETYPE_DIRECTORY)

    def get_project(self, project_path):
        # FIXME: return a dummy project, or retrive the project from server every time?
        # Well we should retrive from server, as the path may change, and we want to notify the user ASAP
        #return Project(project_name)
        return None

    def remove_project(self, project_path):
        pass

    def get_root_node(self):
        return self.root_node

    def get_node_at_path(self, targetpath, handleCb):
        # from https://stackoverflow.com/questions/3167154/how-to-split-a-dos-path-into-its-components-in-python
        folders = explode_path(targetpath)

        global currentLevel, curNode
        curNode = ProjectManager().get_root_node()
        currentLevel = 0
        def folderCb(subnodes):
            global currentLevel, curNode
            for node in subnodes:
                if node.get_name() == folders[currentLevel]:
                    curNode = node
                    break
            else:
                return
            currentLevel += 1
            if currentLevel == len(folders):
                handleCb(curNode)
                return
            curNode.list_tree(folderCb)

        ProjectManager().get_root_node().list_tree(folderCb)

    '''
    def add_project(self, name, hash, file, type, date, cb):
        proj = Project(name, hash, file, type, date)
        d = NetworkManager().send_packet(CreateProject.Query(proj))
        def _project_added(reply):
            cb(proj)
        d.add_callback(_project_added)
        d.add_errback(logger.exception)
    '''

    '''
    # ProjectManager().add_project(project_name, hash, file, type, \
    # date, project_id, project_path, create_flag, project_comments,self._project_created)
    def add_project(self, project_path, file, hash, type, cb):
        proj = Project(project_path, None, None, file, hash, type)
        d = NetworkManager().send_packet(AddTreeNode.Query(proj))

        def _project_added(reply):
            cb(reply)

        d.add_callback(_project_added)
        d.add_errback(logger.exception)
    '''

    '''
    def list_project(self, cb):
        d = NetworkManager().send_packet(ListProjects.Query())

        def _projects_listed(reply):
            ret_projects = reply.projects
            cb(ret_projects)

        d.add_callback(_projects_listed)
        d.add_errback(logger.exception)
    '''

    def move_project(self):
        pass

    def del_project(self):
        pass
