from idarling.network.socketimpl.packets import (
    DefaultCommand,
)

class InviteToLocation(DefaultCommand):
    __command__ = "invite_to_location"

    def __init__(self, name, loc):
        super(InviteToLocation, self).__init__()
        self.name = name
        self.loc = loc

class UpdateLocation(DefaultCommand):
    __command__ = "update_location"

    def __init__(self, name, ea, color):
        super(UpdateLocation, self).__init__()
        self.name = name
        self.ea = ea
        self.color = color

class UpdateUserName(DefaultCommand):
    __command__ = "update_user_name"

    def __init__(self, old_name, new_name):
        super(UpdateUserName, self).__init__()
        self.old_name = old_name
        self.new_name = new_name


class UpdateUserColor(DefaultCommand):
    __command__ = "update_user_color"

    def __init__(self, name, old_color, new_color):
        super(UpdateUserColor, self).__init__()
        self.name = name
        self.old_color = old_color
        self.new_color = new_color
