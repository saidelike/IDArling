from idarling.managers import *
from idarling.shared.utils import Singleton

'''
class User(object):
    def __init__(self, name, color, ea=None):
        self.name = name
        self.color = color
        self.ea = ea
'''

@Singleton
class UserManager(object):
    def __init__(self):
        self._users = {}

    def add_user(self, name, user):
        self._users[name] = user
        InterfaceManager().painter.refresh()
        InterfaceManager().widget.refresh()

    def remove_user(self, name):
        # type: (object) -> object
        user = self._users.pop(name, None)
        InterfaceManager().painter.refresh()
        InterfaceManager().widget.refresh()
        return user

    def get_user(self, name):
        return self._users.get(name)

    def get_users(self):
        return self._users

    def clear_users(self):
        self._users.clear()

    def change_color(self, ea):
        name = ConfigManager().user.name
        color = ConfigManager().user.color

        from idarling.core.user.usercommands import UpdateLocation
        NetworkManager().send_packet(
            UpdateLocation(name, ea, color)
        )

