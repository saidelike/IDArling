from idarling.network.socketimpl.packets import (
    Command,
    Container,
    DefaultCommand,
    ParentCommand,
    Query as IQuery,
    Reply as IReply,
)

LOGINSTATUS_SUCCESS = "success"
LOGINSTATUS_FAIL = "fail"

class LoginHello(ParentCommand):
    __command__ = "login_hello"

    class Query(IQuery, DefaultCommand):
        pass

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, supported_mode):
            super(LoginHello.Reply, self).__init__(query)
            self.supported_mode = supported_mode

class LoginNegotiate(ParentCommand):
    __command__ = "login_negotiate"

    class Query(IQuery, DefaultCommand):
        def __init__(self, selected_mode):
            super(LoginNegotiate.Query, self).__init__()
            self.selected_mode = selected_mode

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, status):
            super(LoginNegotiate.Reply, self).__init__(query)
            self.status = status

class LoginRequest(ParentCommand):
    __command__ = "login_request"

    class Query(IQuery, DefaultCommand):
        def __init__(self, login_req_data):
            super(LoginRequest.Query, self).__init__()
            self.login_req_data = login_req_data

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, login_resp_data):
            super(LoginRequest.Reply, self).__init__(query)
            self.login_resp_data = login_resp_data

'''
class LoginResponse(ParentCommand):
    class Query(IQuery, DefaultCommand):
        def __init__(self, name, resp):
            super(LoginResponse.Query, self).__init__()
            self.name = name
            self.resp = resp

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, status):
            super(LoginResponse.Reply, self).__init__(query)
            self.status = status
'''

class JoinSession(ParentCommand):
    __command__ = "join_session"

    class Query(IQuery, DefaultCommand):
        def __init__(self, project_path, project_id, database_id, tick, name, color, ea, silent=True):
            # name is used only when forwarding packet
            super(JoinSession.Query, self).__init__()
            self.project_path = project_path
            self.project_id = project_id
            self.database_id = database_id
            self.tick = tick
            self.name = name
            self.color = color
            self.ea = ea
            self.silent = silent
        '''
        def build_command(self, dct):
            super(JoinSession.Query, self).build_command(dct)
            dct["project"] = {}
            self.project.build(dct["project"])
            dct["database"] = {}
            self.database.build(dct["database"])

        def parse_command(self, dct):
            super(JoinSession.Query, self).parse_command(dct)
            from idarling.core.project.project import Project, Database
            self.project = Project.new(dct['project'])
            self.database = Database.new(dct['database'])
        '''

    class Reply(IReply, DefaultCommand):
        def __init__(self, query, project, database):
            super(JoinSession.Reply, self).__init__(query)
            self.project = project
            self.database = database

        def build_command(self, dct):
            super(JoinSession.Reply, self).build_command(dct)
            if self.project:
                dct["project"] = {}
                self.project.build(dct["project"])

            if self.database:
                dct["database"] = {}
                self.database.build(dct["database"])

        def parse_command(self, dct):
            super(JoinSession.Reply, self).parse_command(dct)
            from idarling.core.project.project import Project, Database
            self.project = None
            if dct['project']:
                self.project = Project.new(dct['project'])
            self.database = None
            if dct['database']:
                self.database = Database.new(dct['database'])


class LeaveSession(DefaultCommand):
    __command__ = "leave_session"

    def __init__(self, name, silent=True):
        super(LeaveSession, self).__init__()
        self.name = name
        self.silent = silent

