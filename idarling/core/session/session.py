import ida_kernwin
import ida_netnode

#import idarling.managers as managers
from idarling.managers import *

from .sessioncommands import *
from idarling.shared.utils import Singleton

import logging
logger = logging.getLogger("idarling.session")

@Singleton
class SessionManager(object):
    """
    Manages the only session.
    Session refers to an collaboration on a project
    """
    NETNODE_NAME = "$ idarling"

    def __init__(self):
        self._project = None
        self._database = None
        self._tick = 0

    @property
    def project(self):
        return self._project

    @project.setter
    def project(self, project):
        self._project = project
        self.save_netnode()

    @property
    def database(self):
        return self._database

    @database.setter
    def database(self, database):
        self._database = database
        self.save_netnode()

    @property
    def tick(self):
        return self._tick

    @tick.setter
    def tick(self, tick):
        self._tick = tick
        self.save_netnode()

    def load_netnode(self):
        """
        Load data from our custom netnode. Netnodes are the mechanism used by
        IDA to load and save information into a database. IDArling uses its own
        netnode to remember which project and database a database belongs to.
        """
        node = ida_netnode.netnode(self.NETNODE_NAME, 0, True)

        project_path = node.hashval("project_path") or None
        project_id = node.hashval("project_id") or None
        database_id = node.hashval("database") or None
        self._tick = int(node.hashval("tick") or "0")

        logger.debug(
            "Loaded netnode: project_path=%s, project_id=%s, database_id=%s, tick=%d"
            % (project_path, project_id, database_id, self._tick)
        )
        return project_path, project_id, database_id, self._tick

    def save_netnode(self):
        """Save data into our custom netnode."""
        node = ida_netnode.netnode(self.NETNODE_NAME, 0, True)

        if self._project:
            node.hashset("project_path", str(self._project.nodepath))
            node.hashset("project_id", str(self._project.nodeid))
        if self._database:
            node.hashset("database", str(self._database.id))
        if self._tick:
            node.hashset("tick", str(self._tick))

        if self._project and self._database:
            logger.debug(
                "Saved netnode: project_path=%s, project_id=%s, database=%s, tick=%d"
                % (self._project.nodepath, self._project.nodeid, self._database, self._tick)
            )

    def login(self, authmode, name, cb, authargs):
        def finish_cb(success):
            if success:
                logger.info("Successful logged in with user: %s", name)
            else:
                logger.info("Failed to log in with user: %s", name)
                NetworkManager().disconnect()
            if cb:
                cb(success)
        try:
            if authmode == "none":
                from idarling.network.auth.none import NoneLogin
                login = NoneLogin(name, *authargs)
            elif authmode == 'chap':
                from idarling.network.auth.chap import CHAPLogin
                login = CHAPLogin(name, *authargs)
            elif authmode == 'pubkey':
                from idarling.network.auth.pubkey import PubkeyLogin
                login = PubkeyLogin(name, *authargs)
            else:
                logger.warning("Unknown auth mode %s", authmode)
                NetworkManager().disconnect()
                return
            login.do_auth(finish_cb)
        except Exception as e:
            logger.warning("Error when doing authentication", exc_info=True)

    def join_session(self):
        """Join the collaborative session."""
        logger.debug("Try joining session")
        project_path, project_id, database_id, _ = self.load_netnode()
        if project_path and project_id and database_id:
            name = ConfigManager().user.name
            color = ConfigManager().user.color
            ea = ida_kernwin.get_screen_ea()
            d = NetworkManager().send_packet(
                JoinSession.Query(
                    project_path,
                    project_id,
                    database_id,
                    self._tick,
                    name,
                    color,
                    ea,
                )
            )
            if not d: # the network may be still not connected, so we have to check
                return
            def joined(reply):
                if reply.project and reply.database:
                    logger.debug("Successfully joined session")
                    SessionManager().project = reply.project
                    SessionManager().database = reply.database
                    HookManager().session_hook()
                    UserManager().clear_users()
                else:
                    logger.debug("Failed to join session")

            d.add_callback(joined)
            d.add_errback(logger.exception)

    def leave_session(self):
        """Leave the collaborative session."""
        logger.debug("Leaving session")
        if self._project and self._database:
            name = ConfigManager().user.name
            NetworkManager().send_packet(LeaveSession(name))
            UserManager().clear_users()
            HookManager().session_unhook()