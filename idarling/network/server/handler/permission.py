import os
from ..serverfs import PERMDATA

PERMISSION_ALLOW = "allow"
PERMISSION_DENY = "deny"

class PermissionFSWrapper(object):
    def __init__(self, parent):
        self.parent = parent

        # Hot patch
        #self._fs.check_permission = self.check_permission
        #self._fs.get_allow_roots = self.get_allow_roots

    @property
    def _storage(self):
        return self.parent.parent().storage

    @property
    def _fs(self):
        return self.parent.parent().fs

    def __getattr__(self, item):
        if item in self.__dict__:
            return getattr(self, item)
        PERMDATA.handler = self
        return getattr(self._fs, item)

    def check_permission(self, serverpath=None, idpath=None):
        if serverpath:
            idpath = self._fs.path_manager.get_idpath(serverpath)
        if not idpath:
            return False
        if self.parent._name:
            ret_perm = self._storage.select_permission(self.parent._name, idpath)
            perm_dict = {}
            for c in ret_perm:
                nodeid = c["nodeid"]
                if nodeid in perm_dict:
                    if not perm_dict[nodeid]["permission"] == PERMISSION_DENY:
                        perm_dict[nodeid] = c
                else:
                    perm_dict[nodeid] = c

            allow = False
            for nodeid in idpath:
                if nodeid in perm_dict:
                    if perm_dict[nodeid]["permission"] == PERMISSION_ALLOW:
                        allow = True
                    elif perm_dict[nodeid]["permission"] == PERMISSION_DENY:
                        return False

            return allow

    def get_allow_roots(self):
        perms = self._storage.select_all_permission(username=self.parent._name, permtype=PERMISSION_ALLOW)
        allowids = [c["nodeid"] for c in perms]
        if None in allowids:
            return [self._fs.root]
        ret_paths = []
        def handleDir(currentDir):
            try:
                dirpath, dirs, _ = next(os.walk(currentDir))
            except:
                return

            ret = self._fs.path_manager.parse_name(os.path.basename(dirpath))
            if ret:
                nodename, nodeid, nodetype = ret
                if nodeid in allowids:
                    ret_paths.append(dirpath)
                    return  # here we return to avoid its subtree nodes
            for dir in dirs:
                handleDir(os.path.join(dirpath, dir))

        handleDir(self._fs.root)
        return ret_paths