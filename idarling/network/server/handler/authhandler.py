from idarling.network.server.handler.basehandler import BaseHandler
from idarling.core.session.sessioncommands import LoginNegotiate, LoginHello, LOGINSTATUS_FAIL, LOGINSTATUS_SUCCESS
from idarling.network.socketimpl.packets import Command


class AuthHandler(BaseHandler):
    from idarling.network.auth.none import NoneAuthenticator
    from idarling.network.auth.chap import CHAPAuthenticator
    from idarling.network.auth.pubkey import PubkeyAuthenticator
    AUTH_CLS = {
        "none": NoneAuthenticator,
        "chap": CHAPAuthenticator,
        "pubkey": PubkeyAuthenticator
    }

    def __init__(self, conn, modes):
        super(AuthHandler, self).__init__(conn)
        self.modes = modes
        self._handlers = {
            LoginHello.Query: self._handle_login_hello,
            LoginNegotiate.Query: self._handle_login_negotiate,
        }
        self.name = None
        self.authenticator = None

    def finished(self):
        if not self.authenticator:
            return False
        if self.authenticator.success:
            return True
        return False

    def recv_packet(self, packet):
        if self.authenticator:
            return self.authenticator.recv_packet(packet)
        elif isinstance(packet, Command):
            # Call the corresponding handler
            if not packet.__class__ in self._handlers:
                return False
            self._handlers[packet.__class__](packet)
            return True
        return False

    def _handle_login_hello(self, query):
        auth_names = list(self.AUTH_CLS.keys())
        self.send_packet(LoginHello.Reply(query, auth_names))
        return True

    def _handle_login_negotiate(self, query):
        mode = query.selected_mode
        if mode in self.AUTH_CLS:
            self.authenticator = self.AUTH_CLS[mode](self)
            self.send_packet(LoginNegotiate.Reply(query, LOGINSTATUS_SUCCESS))
        else:
            self.send_packet(LoginNegotiate.Reply(query, LOGINSTATUS_FAIL))
            self.disconnect()