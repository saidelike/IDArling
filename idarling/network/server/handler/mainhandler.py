import time

from idarling.core.project.project import Database, TreeNode
from idarling.core.project.projectcommands import CreateDatabase, CreateProject, DownloadFile, ListDatabases, \
    ListProjects, \
    UpdateFile, ListTree, AddTreeNode, MoveTreeNode, DelTreeNode, DELSTATUS_SUCCESS, DELSTATUS_FAIL
from idarling.core.session.sessioncommands import JoinSession, LeaveSession, LoginRequest, \
    LOGINSTATUS_FAIL, LOGINSTATUS_SUCCESS
from idarling.core.user.usercommands import UpdateLocation, InviteToLocation, UpdateUserName, UpdateUserColor
from idarling.network.server.handler.basehandler import BaseHandler
from idarling.network.server.handler.permission import PermissionFSWrapper
from idarling.network.server.serverutil import convert_proto_to_node, list_database

from idarling.network.socketimpl.packets import Command, Event
from idarling.shared.proto import NODETYPE_DIRECTORY, NODETYPE_PROJECT, ProjectInfoProto, TreeNodeProto, ProjectProto

import logging
logger = logging.getLogger("idarling.network.server.handler.mainhandler")

class MainHandler(BaseHandler):
    def __init__(self, conn):
        super(MainHandler, self).__init__(conn)

        self._project = None
        self._database = None
        self._name = None
        self._color = None
        self._ea = None

        # Setup command handlers
        self._handlers = {
            ListTree.Query: self._handle_list_tree,
            AddTreeNode.Query: self._handle_add_tree_node,
            MoveTreeNode.Query: self._handle_move_tree_node,
            DelTreeNode.Query: self._handle_del_tree_node,
            ListProjects.Query: self._handle_list_projects,
            ListDatabases.Query: self._handle_list_databases,
            CreateProject.Query: self._handle_create_project,
            CreateDatabase.Query: self._handle_create_database,
            UpdateFile.Query: self._handle_upload_file,
            DownloadFile.Query: self._handle_download_file,
            JoinSession.Query: self._handle_join_session,
            LeaveSession: self._handle_leave_session,
            UpdateLocation: self._handle_update_location,
            InviteToLocation: self._handle_invite_to_location,
            #UpdateUserName: self._handle_update_user_name,
            UpdateUserColor: self._handle_update_user_color,
        }
        #self._perm_cheker = PermissionChecker(self)
        self._permfs = PermissionFSWrapper(self)

    @property
    def project(self):
        return self._project

    @property
    def database(self):
        return self._database

    @property
    def name(self):
        return self._name

    @property
    def color(self):
        return self._color

    @property
    def ea(self):
        return self._ea

    def after_disconnect(self):
        if self._project and self._database:
            self.parent().forward_users(self, LeaveSession(self.name, False))

    def recv_packet(self, packet):
        if isinstance(packet, Command):
            # Call the corresponding handler
            if not packet.__class__ in self._handlers:
                return False
            self._handlers[packet.__class__](packet)

        elif isinstance(packet, Event):
            if not self._project or not self._database:
                logger.warning(
                    "Received a packet from an unsubscribed client"
                )
                return True

            # Check for de-synchronization
            tick = self.parent().storage.last_tick(
                self._project.nodeid, self._database.id
            )
            if tick >= packet.tick:
                logger.warning("De-synchronization detected!")
                packet.tick = tick + 1

            # Save the event into the database
            self.parent().storage.insert_event(self, packet)
            # Forward the event to the other users
            self.parent().forward_users(self, packet)

            # Ask for a snapshot of the database if needed
            interval = self.parent().SNAPSHOT_INTERVAL
            if packet.tick and interval and packet.tick % interval == 0:
                def file_downloaded(reply):
                    proj_root = self._permfs.find_project(self._project.nodepath, self._project.nodeid)
                    self._permfs.write_database(proj_root, self._database.id, reply.content)
                    logger.info("Auto-saved database %s for project %s (%s)" % (self._database.id, self._project.nodepath, self._project.nodeid))

                d = self.send_packet(
                    DownloadFile.Query(self._project, self._database.id)
                )
                d.add_callback(file_downloaded)
                d.add_errback(logger.exception)
        else:
            return False
        return True

    def _handle_add_tree_node(self, query):
        parentnode = query.parentnode
        newnode = query.newnode # type: ProjectProto
        #targetpath = newnode.nodepath

        newname = TreeNode(newnode.nodepath, None, None, None, None).get_name() # ugly
        if newnode.nodetype == NODETYPE_DIRECTORY:
            newnode.nodeid = self._permfs.create_directory(parentnode.nodeid, newname)
            if newnode.nodeid is None:
                self.send_packet(AddTreeNode.Reply(query, None, "Failed to create directory at %s (maybe permission denied)" % newnode.nodeid))
            self.send_packet(AddTreeNode.Reply(query, newnode, None))
        elif newnode.nodetype == NODETYPE_PROJECT:
            newnode.nodeid = self._permfs.create_project(parentnode.nodeid, newname, newnode.file)
            if newnode.nodeid is None:
                self.send_packet(AddTreeNode.Reply(query, None, "Failed to create project at %s (maybe permission denied)" % newnode.nodeid))
            newnode.mdate = int(time.time())
            # Force casting into one of the subclass
            dct = newnode.build({})
            info = ProjectInfoProto.new(dct)
            self.parent().storage.insert_project(info)
            self.send_packet(AddTreeNode.Reply(query, newnode, None))
        else:
            self.send_packet(AddTreeNode.Reply(query, None, "Invalid new node type: %s" % newnode.nodetype))

    def _handle_move_tree_node(self, query):
        orinode = query.node # type: ProjectProto
        newParentId = query.targetnode.id if query.targetnode else None
        newname = query.newname
        newnode = self._permfs.move_node(orinode.nodeid, newParentId, newname)
        if not newnode:
            self.send_packet(MoveTreeNode.Reply(query, None))
        else:
            orinode.nodepath = newnode.nodepath
            self.send_packet(MoveTreeNode.Reply(query, orinode))

    def _handle_del_tree_node(self, query):
        '''
        orinode = query.node # type: ProjectProto
        if orinode.nodetype == NODETYPE_DIRECTORY:
            ret = self._permfs.del_dir(orinode.nodepath, orinode.nodepath)
        elif orinode.nodetype == NODETYPE_PROJECT:
            ret = self._permfs.del_proj(orinode.nodepath, orinode.nodeid)
        else:
            self.send_packet(MoveTreeNode.Reply(query, DELSTATUS_FAIL))
            return
        if ret:
            self.send_packet(MoveTreeNode.Reply(query, DELSTATUS_SUCCESS))
        else:
            self.s`end_packet(MoveTreeNode.Reply(query, DELSTATUS_FAIL))
        '''
        raise Exception("Delete function is disabled")

    def _handle_list_tree(self, query):
        dircontent = self._permfs.list_tree(query.node.nodeid)
        ret = []
        for node in dircontent:
            ret.append(convert_proto_to_node(node, self.parent().storage))

        self.send_packet(ListTree.Reply(query, ret))
        pass

    def _handle_list_projects(self, query):
        '''projects = self.parent().storage.select_projects()
        self.send_packet(ListProjects.Reply(query, projects))'''
        raise Exception("This packet is deprecated now!")


    def _handle_list_databases(self, query):
        ret = list_database(query.project.nodeid, self.parent().fs, self.parent().storage)
        self.send_packet(ListDatabases.Reply(query, ret))

    def _handle_create_project(self, query):
        '''
        self.parent().storage.insert_project(query.project)
        self.send_packet(CreateProject.Reply(query))
        '''
        raise Exception("This packet is deprecated now!")

    def _handle_create_database(self, query):
        proj_root = self._permfs.find_project(query.project.nodepath, query.project.nodeid)
        dbinfo = self._permfs.create_database(proj_root, query.project.nodeid, query.database_name, query.content)
        if not dbinfo:
            self.send_packet(CreateDatabase.Reply(query, None))
        else:
            db_name, dbId, mtime = dbinfo
            self.send_packet(CreateDatabase.Reply(query, Database(dbId, db_name, mtime, 0)))
        #self.parent().storage.insert_database(query.database_name)

    def _handle_upload_file(self, query):
        proj_root = self._permfs.find_project(query.project.nodepath, query.project.nodeid)
        self._permfs.write_database(proj_root, query.database_id, query.content)
        self.send_packet(UpdateFile.Reply(query))

    def _handle_download_file(self, query):
        projId = query.project.nodeid
        dbId = query.database_id
        projAbsPath = self._permfs.find_project(query.project.nodepath, projId)
        ret = self._permfs.get_database_info(projAbsPath, dbId=dbId)
        if ret:
            _, _, _, dbAbsPath = ret
        else:
            reply = DownloadFile.Reply(query)
            reply.content = None
            self.send_packet(reply)
            return
        # Read file from disk and sent it
        reply = DownloadFile.Reply(query)
        with open(dbAbsPath, "rb") as input_file:
            reply.content = input_file.read()
        logger.info("Loaded file %s" % dbAbsPath)
        self.send_packet(reply)

    def _handle_join_session(self, packet):
        database_id = packet.database_id
        proj = self._permfs.get_project(packet.project_path, packet.project_id)
        if not proj:
            self.send_packet(JoinSession.Reply(packet, None, None))
            return
        projnode, projAbsPath = proj
        projId = projnode.nodeid
        projs = self.parent().storage.select_project_with_id(projId, 1)
        if len(projs) != 1:
            logger.warning("Project exists in dir but not found in database")
            self.send_packet(JoinSession.Reply(packet, None, None))
            return
        proj = projs[0]  # type: ProjectInfoProto

        from idarling.core.project.project import Project
        self._project = Project(projnode.nodepath, projId, projnode.mdate, proj.file, proj.hash, proj.filetype)

        dbinfo = self._permfs.get_database_info(projAbsPath, packet.database_id)
        if not dbinfo:
            self.send_packet(JoinSession.Reply(packet, None, None))
            return

        _, dbName, dbmtime, _ = dbinfo
        self._database = Database(database_id, dbName, dbmtime)

        #self._name = packet.name
        self._color = packet.color
        self._ea = packet.ea

        # Inform the other users that we joined
        packet.silent = False
        self.parent().forward_users(self, packet)

        self.send_packet(JoinSession.Reply(packet, self._project, self._database))

        # Inform ourselves about the other users
        for user in self.parent().get_users(self):
            self.send_packet(
                JoinSession.Query(
                    packet.project_path,
                    packet.project_id,
                    packet.database_id,
                    packet.tick,
                    user.name,
                    user.color,
                    user.ea,
                )
            )

        # Send all missed events
        events = self.parent().storage.select_events(
            self._project.nodeid, self._database.id, packet.tick
        )
        logger.debug("Sending %d missed events" % len(events))
        for event in events:
            self.send_packet(event)

    def _handle_leave_session(self, packet):
        # Inform others users that we are leaving
        packet.silent = False
        self.parent().forward_users(self, packet)

        # Inform ourselves that the other users leaved
        for user in self.parent().get_users(self):
            self.send_packet(LeaveSession(user.name))

        self._project = None
        self._database = None
        self._name = None
        self._color = None

    def _handle_update_location(self, packet):
        self.parent().forward_users(self, packet)

    def _handle_invite_to_location(self, packet):
        source = packet.name
        def matches(other):
            return other.name == source or source == "everyone"

        packet.name = self._name
        self.parent().forward_users(self, packet, matches)

    def _handle_update_user_name(self, packet):
        # FXIME: ensure the name isn't already taken
        self._name = packet.new_name
        self.parent().forward_users(self, packet)

    def _handle_update_user_color(self, packet):
        self.parent().forward_users(self, packet)