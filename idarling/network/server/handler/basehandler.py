class BaseHandler(object):
    def __init__(self, conn):
        self._conn = conn

    def __getattr__(self, item):
        return getattr(self._conn, item)

    def after_disconnect(self):
        pass