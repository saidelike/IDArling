import os
import random
import shutil
import time
import hashlib

from idarling.core.project.project import TreeNode, Project, Path
from idarling.shared.proto import NODETYPE_DIRECTORY, NODETYPE_PROJECT, TreeNodeProto
from idarling.shared.utils import LRUCache, explode_path

import logging
logger = logging.getLogger("idarling.network.server.serverfs")

import threading
PERMDATA = threading.local()
PERMDATA.handler = None

DIR_SEPARATOR = "`"
PROJECT_SEPARATOR = "#"
DB_SEPARATOR = "^"
PATH_SEPARATOR = "/"
BADCHARS = set(DIR_SEPARATOR + PROJECT_SEPARATOR + DB_SEPARATOR)

class PathManager(object):
    def __init__(self, fs):
        self.fs = fs

    @classmethod
    def check_path(cls, path):
        for c in BADCHARS:
            if c in path:
                return False
        return True

    @classmethod
    def _parse_raw_dirname(cls, dirpath):
        #dirpath = os.path.basename(path)
        dirname, _, dirid = dirpath.rpartition(DIR_SEPARATOR)
        if not dirname or not dirid:
            return None
        else:
            return (dirname, dirid)

    @classmethod
    def _parse_raw_projname(cls, dirpath):
        #dirpath = os.path.basename(path)
        dirname, _, dirid = dirpath.rpartition(PROJECT_SEPARATOR)
        if not dirname or not dirid:
            return None
        else:
            return (dirname, dirid)

    @classmethod
    def _parse_raw_dbname(cls, dbfilename):
        if dbfilename.endswith(".idb"):
            dbfilename = dbfilename[:-4]
        dbname, _, dbId = dbfilename.rpartition(DB_SEPARATOR)
        if not dbname or not dbId:
            return None
        else:
            return (dbname, dbId)

    @classmethod
    def _encode_raw_dbname(cls, dbname, dbId):
        return dbname + DB_SEPARATOR + dbId + ".idb"

    @classmethod
    def _encode_raw_dirname(cls, dirname, dirid):
        return dirname + DIR_SEPARATOR + dirid

    @classmethod
    def _encode_raw_projname(cls, dirname, dirid):
        return dirname + PROJECT_SEPARATOR + dirid

    @classmethod
    def parse_name(cls, name):
        ret = cls._parse_raw_dirname(name)
        if ret:
            return ret + (NODETYPE_DIRECTORY,)
        ret = cls._parse_raw_projname(name)
        if ret:
            return ret + (NODETYPE_PROJECT,)
        return None

    @classmethod
    def encode_name(cls, dirname, dirid, nodetype):
        if nodetype == NODETYPE_PROJECT:
            return cls._encode_raw_projname(dirname, dirid)
        elif nodetype == NODETYPE_DIRECTORY:
            return cls._encode_raw_dirname(dirname, dirid)
        return None

    def get_server_path(self, pathid):
        fs = self.fs
        if pathid is None: # root node
            return fs.root
        if pathid in fs._dircache:
            path = fs._dircache[pathid]
            if os.path.exists(path):
                return path
        for dirpath, dirs, _ in os.walk(fs.root):
            for dir in dirs:
                dirinfo = self.parse_name(dir)
                if dirinfo is None:
                    continue
                _, dirid, dirtype = dirinfo
                if dirid == pathid:
                    newpath = os.path.join(dirpath, dir)
                    fs._dircache[pathid] = newpath
                    return newpath
        return None

    def parse_server_path(self, serverpath):
        root = self.fs.root
        server_relpath = "/" + os.path.relpath(serverpath, root)

        if os.path.exists(serverpath):
            ret_mtime = os.path.getmtime(serverpath)
        else:
            ret_mtime = None

        pathparts = explode_path(server_relpath) # in case there's "../"
        if len(pathparts) == 0:
            return TreeNodeProto("/", None, ret_mtime, None, NODETYPE_DIRECTORY) # it's root

        nodeid = None # make pycharm happy
        nodetype = None # make pycharm happy
        ret_path = PATH_SEPARATOR
        for i, part in enumerate(pathparts):
            ret = self.parse_name(part)
            if not ret:
                return None
            nodename, nodeid, nodetype = ret
            if nodetype != NODETYPE_DIRECTORY and i + 1 != len(pathparts):
                return None
            ret_path += nodename
            ret_path += PATH_SEPARATOR

        ret_path = ret_path.rstrip(PATH_SEPARATOR)
        ret_nodeid = nodeid
        ret_nodetype = nodetype

        return TreeNodeProto(ret_path, ret_nodeid, ret_mtime, None, ret_nodetype)

    def get_idpath(self, serverpath):
        root = self.fs.root
        server_relpath = "/" + os.path.relpath(serverpath, root)

        pathparts = explode_path(server_relpath)  # in case there's "../"
        if len(pathparts) == 0:
            return [None] # root

        ids = [None]
        for i, part in enumerate(pathparts):
            ret = self.parse_name(part)
            if not ret:
                return None
            nodename, nodeid, nodetype = ret
            if nodetype != NODETYPE_DIRECTORY and i + 1 != len(pathparts):
                return None
            ids.append(nodeid)

        return ids

    @classmethod
    def find_database(cls, proj_root, dbId=None, dbName=None):
        for dirpath, _, files in os.walk(proj_root):
            for file in files:
                dbinfo = PathManager._parse_raw_dbname(file)
                if dbinfo is None:
                    continue
                cur_dbname, cur_dbId = dbinfo
                if (dbName and dbName == cur_dbname) or (dbId and dbId == cur_dbId):
                    return os.path.join(dirpath, file)
        return None

class ServerFS(object):
    def __init__(self, serverRoot):
        self.root = serverRoot
        #self._projcache = LRUCache(100)
        self.path_manager = PathManager(self)
        self._dircache = LRUCache(200)

    def check_path(self, path):
        for c in BADCHARS:
            if c in path:
                return False
        return True

    def normpath(self, path):
        return '/' + os.path.normpath("/" + path).replace('\\', '/').lstrip("/")  # in case path traversal

    def _check_permission(self, serverpath=None, idpath=None):
        # Stub function - overrided by wrapper
        # True - Allow
        # False - Deny
        '''
        if serverpath:
            idpath = self.path_manager.get_idpath(serverpath)
        if not idpath:
            return False
        '''
        return True  # allow anything by default

    def _get_allow_roots(self):
        # Stub functions - overrided by wrapper
        dirpath, dirs, _ = next(os.walk(self.root))
        return [os.path.join(dirpath, c) for c in dirs]

    def check_permission(self, *args, **kwargs):
        if getattr(PERMDATA, "handler", None):
            return PERMDATA.handler.check_permission(*args, **kwargs)
        else:
            return self._check_permission(*args, **kwargs)

    def get_allow_roots(self, *args, **kwargs):
        if getattr(PERMDATA, "handler", None):
            return PERMDATA.handler.get_allow_roots(*args, **kwargs)
        else:
            return self._get_allow_roots(*args, **kwargs)

    def list_tree(self, pathid):
        if pathid is None:
            # root dir
            # DFS find any allowed folder
            allowroots = self.get_allow_roots()
            if allowroots != [self.root]:
                ret = []
                for root in allowroots:
                    retNode = self.path_manager.parse_server_path(root)
                    if retNode:
                        ret.append(retNode)
                return ret

            # we have full access to whole server tree, then we should return all

        # serverpath = self.root + self.normpath(path)
        serverpath = self.path_manager.get_server_path(pathid)
        if not serverpath:
            return []

        if not self.check_permission(serverpath=serverpath):
            return []

        dirpath, dirnames, filenames = next(os.walk(serverpath))
        ret = []
        for dir in dirnames:
            curdir = os.path.join(serverpath, dir)
            if self.check_permission(serverpath=curdir):
                retNode = self.path_manager.parse_server_path(curdir)
                if retNode:
                    ret.append(retNode)
        return ret

    def get_project(self, proj_nodepath, proj_nodeid):
        serverpath = self.path_manager.get_server_path(proj_nodeid)
        if not self.check_permission(serverpath=serverpath):
            return None
        if not serverpath:
            return None
        ret = self.path_manager.parse_server_path(serverpath)
        if ret.nodetype != NODETYPE_PROJECT:
            return None
        else:
            return ret, serverpath

    def find_project(self, proj_nodepath, proj_nodeid):
        ret = self.get_project(proj_nodepath, proj_nodeid)
        if ret:
            if ret[0].nodetype == NODETYPE_PROJECT:
                return ret[1]
        return None

    def create_project(self, parent_nodeid, projname, projinfo):
        serverpath = self.path_manager.get_server_path(parent_nodeid)
        if not serverpath:
            return None
        if not self.check_permission(serverpath=serverpath):
            return None
        proj_message = str(time.time() * 1000) + projname + str(projinfo) + str(random.random())
        projid = hashlib.md5(proj_message.encode('utf-8')).hexdigest()[8:24]
        projfile = PathManager._encode_raw_projname(projname, projid)
        projdir = os.path.join(serverpath, projfile)
        self._create_dir(projdir)
        self._dircache[projid] = projdir
        return projid

    def create_directory(self, parent_nodeid, dirname):
        serverpath = self.path_manager.get_server_path(parent_nodeid)
        if not serverpath:
            return None
        if not self.check_permission(serverpath=serverpath):
            return None
        dir_message = str(time.time() * 1000) + dirname + str(random.random())
        dirid = hashlib.md5(dir_message.encode('utf-8')).hexdigest()[8:24]
        dirfile = PathManager._encode_raw_dirname(dirname, dirid)
        dirpath = os.path.join(serverpath, dirfile)
        self._create_dir(dirpath)
        self._dircache[dirid] = dirpath
        return dirid

    def create_database(self, proj_root, projId, db_name, dbContent):
        db_name = os.path.basename(db_name) # in case "../"

        if not self.check_path(db_name):
            return

        if self.path_manager.find_database(proj_root, dbName=db_name):
            # already have this name
            return None

        db_message = str(time.time() * 1000) + projId + db_name + str(random.random())

        dbId = hashlib.md5(db_message.encode('utf-8')).hexdigest()[8:24]
        newpath = os.path.join(proj_root, PathManager._encode_raw_dbname(db_name, dbId))
        with open(newpath, 'wb') as f:
            pass  # create an empty file
        self.write_database(proj_root, dbId, dbContent)
        return (db_name, dbId, os.path.getmtime(newpath))

    def list_database(self, proj_root):
        dirpath, dirnames, filenames = next(os.walk(proj_root))
        ret = []
        for fname in filenames:
            dbinfo = PathManager._parse_raw_dbname(fname)
            if dbinfo is None:
                continue
            dbname, cur_dbId = dbinfo
            ret.append((dbname, cur_dbId, os.path.getmtime(os.path.join(dirpath, fname))))
        return ret

    def get_database_info(self, proj_root, dbId):
        newpath = self.path_manager.find_database(proj_root, dbId=dbId)
        if not newpath:
            return None
        dbfilename = os.path.basename(newpath)
        dbName, _ = PathManager._parse_raw_dbname(dbfilename)
        return (dbId, dbName, os.path.getmtime(newpath), newpath)

    def write_database(self, proj_root, dbId, dbContent):
        newpath = self.path_manager.find_database(proj_root, dbId=dbId)
        with open(newpath, "wb") as output_file:
            output_file.write(dbContent)
        logger.info("Saved file %s" % newpath)

    def _create_dir(self, new_path):
        serverpath = new_path
        if not os.path.exists(serverpath):
            os.makedirs(serverpath)

    def move_node(self, nodeid, newParentId, newname):
        oriserverpath = self.path_manager.get_server_path(nodeid)
        if not oriserverpath:
            return None
        if not self.check_permission(serverpath=oriserverpath):
            return None

        orinodename = os.path.basename(oriserverpath)
        _, nodeid, nodetype = self.path_manager.parse_name(orinodename)
        if newParentId:
            newparent = self.path_manager.get_server_path(newParentId)
            if not newparent:
                return None
            if not self.check_permission(serverpath=newparent):
                return None
        else:
            # move in the same directory
            newparent = os.path.dirname(oriserverpath)
        newname = self.path_manager.encode_name(newname, nodeid, nodetype)
        newserverpath = os.path.join(newparent, newname)
        newnode = self.path_manager.parse_server_path(newserverpath)
        if os.path.exists(newserverpath):
            return None
        try:
            shutil.move(oriserverpath, newserverpath)
            return newnode
        except Exception as e:
            logger.warning("Failed to move from %s to %s", oriserverpath, newserverpath, exc_info=1)
            return None

    def del_dir(self, nodeid):
        serverpath = self.path_manager.get_server_path(nodeid)
        if not serverpath:
            return False
        if not self.check_permission(serverpath=serverpath):
            return None
        try:
            os.unlink(serverpath)
            return True
        except:
            return False