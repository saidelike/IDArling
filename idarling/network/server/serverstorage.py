# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import json
import sqlite3

from idarling.core.project.project import Project
from idarling.network.server.handler.permission import PERMISSION_ALLOW
from idarling.shared.proto import DatabaseProto, ProjectProto, ProjectInfoProto, UserProto
from idarling.network.socketimpl.packets import Default, DefaultEvent
from idarling.shared.authutils import calc_passhash

import logging

logger = logging.getLogger("idarling.network.ServerStorage")

class ServerStorage(object):
    """
    This object is used to access the SQL database used by the server. It
    also defines some utility methods. Currently, only SQLite3 is implemented.
    """

    def __init__(self, dbpath):
        self._conn = sqlite3.connect(dbpath, check_same_thread=False)
        self._conn.isolation_level = None  # No need to commit
        self._conn.row_factory = sqlite3.Row  # Use Row objects

    def initialize(self):
        """Create all the default tables."""
        self._create(
            "projects",
            [
                "nodeid text not null",
                "file text not null",
                "hash text not null",
                "filetype text not null",
                #"date text",
                "primary key (nodeid)",
            ],
        )
        '''
        self._create(
            "databases",
            [
                "nodeid text not null",
                "name text not null",
                "date text not null",
                "foreign key(nodeid) references projects(nodeid)",
                "primary key(nodeid, name)",
            ],
        )
        '''
        self._create(
            "events",
            [
                "nodeid text not null",
                "dbId text not null",
                "tick integer not null",
                "dict text not null",
                "foreign key(nodeid) references projects(nodeid)",
                "primary key(nodeid, dbId, tick)",
            ],
        )
        self._create(
            "users",
            [
                #"id integer primary key autoincrement",
                "name text not null",
                "passhash text",
                "pubkey text",
                "primary key(name)",
            ],
        )

        self._create(
            "permission",
            [
                "username text not null",
                "nodeid text not null",
                "permission text not null",
                "primary key(username, nodeid)",
                "foreign key(username) references users(name)",
            ]
        )

        # set default admin
        if self.select_user("admin") is None:
            self.insert_user(UserProto("admin", calc_passhash("idarling"), None))
            self.insert_permission("admin", "", PERMISSION_ALLOW)
            logger.info("create admin with username: admin and password: idarling")
        else:
            logger.info("admin already exists.")

    def insert_project(self, project):
        """Insert a new project into the database."""
        self._insert("projects", Default.attrs(project.__dict__))

    def select_project(self, name):
        """Select the project with the given name."""
        objects = self.select_projects(name, 1)
        return objects[0] if objects else None

    def select_project_with_id(self, nodeid=None, limit=None):
        """Select the projects with the given name."""
        results = self._select("projects", {"nodeid": nodeid}, limit)
        return [ProjectInfoProto(**result) for result in results]

    def select_projects(self, name=None, limit=None):
        """Select the projects with the given name."""
        results = self._select("projects", {"name": name}, limit)
        return [ProjectInfoProto(**result) for result in results]

    def insert_database(self, database):
        """Insert a new database into the database."""
        attrs = Default.attrs(database.__dict__)
        attrs.pop("tick")
        self._insert("databases", attrs)

    def select_database(self, project, name):
        """Select the database with the given project and name."""
        objects = self.select_databases(project, name, 1)
        return objects[0] if objects else None

    def select_databases(self, project=None, name=None, limit=None):
        """Select the databases with the given project and name."""
        results = self._select(
            "databases", {"project": project, "name": name}, limit
        )
        return [DatabaseProto(**result) for result in results]

    def insert_event(self, client, event):
        """Insert a new event into the database."""
        dct = DefaultEvent.attrs(event.__dict__)
        self._insert(
            "events",
            {
                "nodeid": client.project.nodeid,
                "dbId": client.database.id,
                "tick": event.tick,
                "dict": json.dumps(dct),
            },
        )

    def select_permission(self, username, nodeids):
        nodeids = ["" if c is None else c for c in nodeids]
        c = self._conn.cursor()
        sql = "select * from permission where username = ? and nodeid in ({}) ".format(','.join(len(nodeids) * ["?"]))
        c.execute(sql, [username] + nodeids)
        ret = [dict(row) for row in c.fetchall()]
        for r in ret:
            if r["nodeid"] == "":
                r["nodeid"] = None
        return ret

    def select_all_permission(self, username=None, nodeid=None, permtype=None):
        c = self._conn.cursor()
        sql = "select * from permission"
        args = []
        states = []
        if username:
            states.append("username = ?")
            args.append(username)
        if nodeid is not None:
            states.append("nodeid = ?")
            args.append(nodeid)
        if permtype:
            states.append("permission = ?")
            args.append(permtype)
        sql = "select * from permission" \
              + ((" where " + " and ".join(states)) if len(states) else "") \
              + ";"
        c.execute(sql, args)
        ret = [dict(row) for row in c.fetchall()]
        for r in ret:
            if r["nodeid"] == "":
                r["nodeid"] = None
        return ret

    def insert_permission(self, username, nodeid, permission):
        self._insert("permission", {
            "username": username,
            "nodeid": nodeid,
            "permission": permission,
        })

    def delete_permission(self, username=None, nodeid=None, permission=None):
        dct = {}
        if username:
            dct["username"] = username
        if nodeid:
            dct["nodeid"] = nodeid
        if permission:
            dct["permission"] = permission
        self._delete("permission", dct)

    def select_events(self, nodeid, dbId, tick):
        """Get all events sent after the given tick count."""
        c = self._conn.cursor()
        sql = "select * from events where nodeid = ? and dbId = ?"
        sql += "and tick > ? order by tick asc;"
        c.execute(sql, [nodeid, dbId, tick])
        events = []
        for result in c.fetchall():
            dct = json.loads(result["dict"])
            dct["tick"] = result["tick"]
            events.append(DefaultEvent.new(dct))
        return events

    def delete_user(self, user):
        self._delete("users", Default.attrs(user.__dict__))

    def update_user(self, username, passhash=None, pubkey=None):
        updaterows = {}
        if passhash is not None:
            updaterows["passhash"] = passhash
        if pubkey is not None:
            updaterows["pubkey"] = pubkey
        self._update("users", {"name": username}, updaterows)

    def insert_user(self, user):
        self._insert("users", Default.attrs(user.__dict__))

    def select_users(self, name=None, limit=None):
        """Select the project with the given name."""
        args = []
        if name is None:
            args.append({})
        else:
            args.append({"name": name})
        if limit is not None:
            args.append(limit)
        results = self._select("users", *args)
        return [UserProto(**result) for result in results]

    def select_user(self, name):
        ret = self.select_users(name, 1)
        return ret[0] if ret else None

    def last_tick(self, nodeid, dbId):
        """Get the last tick of the specified project and database."""
        c = self._conn.cursor()
        sql = "select tick from events where nodeid = ? and dbId = ? "
        sql += "order by tick desc limit 1;"
        c.execute(sql, [nodeid, dbId])
        result = c.fetchone()
        return result["tick"] if result else 0

    def _create(self, table, cols):
        """Create a table with the given name and columns."""
        c = self._conn.cursor()
        sql = "create table if not exists {} ({});"
        c.execute(sql.format(table, ", ".join(cols)))

    def _select(self, table, fields, limit=None):
        """Select the rows of a table matching the given values."""
        c = self._conn.cursor()
        sql = "select * from {}".format(table)
        fields = {key: val for key, val in fields.items() if val}
        if len(fields):
            cols = ["{} = ?".format(col) for col in fields.keys()]
            sql = (sql + " where {}").format(" and ".join(cols))
        sql += " limit {};".format(limit) if limit else ";"
        c.execute(sql, list(fields.values()))
        return c.fetchall()

    def _insert(self, table, fields):
        """Insert a row into a table with the given values."""
        c = self._conn.cursor()
        sql = "insert into {} ({}) values ({});"
        keys = ", ".join(fields.keys())
        vals = ", ".join(["?"] * len(fields))
        c.execute(sql.format(table, keys, vals), list(fields.values()))

    def _delete(self, table, fields):
        """Select the rows of a table matching the given values."""
        c = self._conn.cursor()
        sql = "delete from {}".format(table)
        fields = {key: val for key, val in fields.items() if val}
        if len(fields):
            cols = ["{} = ?".format(col) for col in fields.keys()]
            sql = (sql + " where {}").format(" and ".join(cols))
        sql += ";"
        c.execute(sql, list(fields.values()))

    def _update(self, table, wheres, fields):
        c = self._conn.cursor()
        sql = "update {}".format(table)
        fields = {key: val for key, val in fields.items() if val is not None}
        if len(fields):
            cols = ["{} = ?".format(col) for col in fields.keys()]
            sql = (sql + " set {}").format(" , ".join(cols))
        wheres = {key: val for key, val in wheres.items() if val}
        if len(wheres):
            cols = ["{} = ?".format(col) for col in wheres.keys()]
            sql = (sql + " where {}").format(" and ".join(cols))
        sql += ";"
        c.execute(sql, list(fields.values()) + list(wheres.values()))