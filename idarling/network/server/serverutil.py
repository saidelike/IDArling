from idarling.core.project.project import Database
from idarling.shared.proto import ProjectInfoProto, NODETYPE_DIRECTORY, NODETYPE_PROJECT

import logging
logger = logging.getLogger("idarling.network.server.serverutil")

def convert_proto_to_node(node, storage):
    if node.nodetype == NODETYPE_DIRECTORY:
        from idarling.core.project.project import TreeNode
        return TreeNode(node.nodepath, node.nodeid, node.mdate, [], node.nodetype)
    elif node.nodetype == NODETYPE_PROJECT:
        projs = storage.select_project_with_id(node.nodeid, 1)
        if len(projs) != 1:
            logger.warning("Project exists in dir but not found in database")
            return None
        proj = projs[0]  # type: ProjectInfoProto
        from idarling.core.project.project import Project
        return Project(node.nodepath, node.nodeid, node.mdate, proj.file, proj.hash, proj.filetype)
    return None

def list_database(projId, fs, storage):
    proj_root = fs.find_project(None, projId)
    dbs = fs.list_database(proj_root)
    ret = []
    for dbname, dbId, mtime in dbs:
        database = Database(dbId, dbname, mtime)
        database.tick = storage.last_tick(projId, dbId)
        ret.append(database)

    return ret