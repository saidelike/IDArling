from idarling.network.server.serverutil import convert_proto_to_node, list_database
from idarling.shared.authutils import calc_passhash

from idarling.shared.proto import NODETYPE_PROJECT, NODETYPE_DIRECTORY, ProjectInfoProto, UserProto

import logging
logger = logging.getLogger("idarling.network.server.restserver")

try:
    import cherrypy
except:
    logger.warning("Error occurred when importing CherryPy", exc_info=True)
    cherrypy = None

if cherrypy is not None:
    class RestServer(object):
        def __init__(self, parent):
            self._parent = parent
            cherrypy.config.update({'server.socket_host': '0.0.0.0',
                                    'server.socket_port': 56329,
                                    })

        def start(self):
            cherrypy.tree.mount(self)
            cherrypy.engine.start()

        def stop(self):
            cherrypy.engine.stop()

        @property
        def storage(self):
            return self._parent._storage

        @property
        def fs(self):
            return self._parent._fs

        @cherrypy.expose
        def index(self):
            return "Rest Server for IDArling, DO NOT EXPOSE THIS SERVER TO OUTSIDE NETWORK"

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def authenticate(self):
            """
            This interface is for convenience, you can simply pass the input from user to authenticate them
            Example:
                POST /authenticate HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXX", "pass": "XXXXXX"}

                >>> r = requests.post("http://127.0.0.1:56329/authenticate", json={"user":"test", "pass": "123456"})
                >>> r.json()
                {u'status': u'ok'}
                >>> r = requests.post("http://127.0.0.1:56329/authenticate", json={"user":"test", "pass": "wrong_pass"})
                >>> r.json()
                {u'status': u'fail'}

            :return:
            """
            username = cherrypy.request.json["user"]
            if username != 'admin':
                return {"status": "fail"}
            password = cherrypy.request.json["pass"]
            user = self.storage.select_user(username)
            if user and calc_passhash(password) == user.passhash:
                return {"status": "ok"}
            else:
                return {"status": "fail"}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def list_tree(self):
            """
            This interface will get children of a node in the server filesystem. If you want to
            query the root node's children, you should pass pathid as null
            Note: this interface will not check the permission.
                The server filesystem is a virtual filesystem, in which
                the project and folder will all be nodes of different types.
                In order to locate a specific node after it's been renamed,
                every node has an unique id, and the name of node is only an
                attribute attached to the node, so in every operation, you
                should pass the nodeid to the interface.
                The root node is a virtual node, which has a special nodeid None,
                 and cannot be used in any operations other than list_tree.
                Currently, there are two types of node:
                    1. Folder node: NODETYPE_DIRECTORY = 1
                        represents a folder in server filesystem. The folder node is where we store project node.
                        Node of this type can have subnodes, and can be passed to list_tree
                    2. Project node: NODETYPE_PROJECT = 2
                        represents a project in server filesystem. The project node represents
                         a binary file, and will contain different version of IDA database. Note the
                          returned information of project node is different from folder node
                        Node of this type can't have subnodes, and its subnodes will be
                         ignored every where, you shouldn't pass this to list_tree



            Example:
            1. Query a specific node
                POST /list_tree HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"pathid": "XXXXXXXX"}
                >>> r = requests.post("http://127.0.0.1:56329/list_tree", json={"pathid": "ac4cd9fd61fab8e6"})
                >>> pprint(r.json())
                {u'content': [{u'mdate': 1568665641.1213102,
                               u'nodeid': u'erhjerkjhg34gut6',
                               u'nodepath': u'/222/234/deny',
                               u'nodetype': 1,
                               u'subnodes': []},
                              {u'file': u'byte_o3',
                               u'filetype': u'ELF64 for x86-64 (Shared object)',
                               u'hash': u'063c37a26963da14ccc25f303edc5831',
                               u'mdate': 1568668411.2351937,
                               u'nodeid': u'4c234c54cf47bcd1',
                               u'nodepath': u'/222/234/lbzsb111',
                               u'nodetype': 2,
                               u'subnodes': []}],
                 u'status': u'ok'}
            2. Query the root node
                POST /list_tree HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"pathid": null}

                >>> r = requests.post("http://127.0.0.1:56329/list_tree", json={"pathid": None})
                >>> pprint(r.json())
                {u'content': [{u'mdate': 1568665641.1213102,
                               u'nodeid': u'bf4c99fd71450826',
                               u'nodepath': u'/123',
                               u'nodetype': 1,
                               u'subnodes': []},
                              {u'mdate': 1568821707.8221035,
                               u'nodeid': u'c7ae87217e271c52',
                               u'nodepath': u'/222',
                               u'nodetype': 1,
                               u'subnodes': []},
                              {u'mdate': 1568823852.6423087,
                               u'nodeid': u'c36ff475b8945664',
                               u'nodepath': u'/333',
                               u'nodetype': 1,
                               u'subnodes': []}],
                 u'status': u'ok'}

            :return:
            """
            pathid = cherrypy.request.json["pathid"]

            dircontent = self.fs.list_tree(pathid)
            ret = []
            for node in dircontent:
                ret.append(convert_proto_to_node(node, self.storage).build({}))
            return {"status": "ok", "content": ret}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def move_node(self):
            """
            This interface will move a node to a target node and rename it.
            Note: the rename operation is special situation of move, so we
             don't have a rename_node interface.
            PS: there should be safety check here, but we don't have time
             to implement it, just don't care
            Example:
                POST /move_node HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"orinodeid": "XXXXXX", "newParentId":"XXXX", "newname": "XXXX"}

                ret: Can be ignoredd
            :return:
            """
            orinodeid = cherrypy.request.json["orinodeid"]
            newParentId = cherrypy.request.json["newParentId"]
            newname = cherrypy.request.json["newname"]

            newnode = self.fs.move_node(orinodeid, newParentId, newname)
            if newnode:
                return {"status": "ok", "node": newnode.build({})}
            else:
                return {"status": "fail", "node": None}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def del_node(self):
            """
            This interface will simply delete a node, which can be used on both types of node.
            Currently this interface is disabled.

            Example:
                POST /del_node HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"nodeid": "XXXXXX"}

            :return:
            """
            nodeid = cherrypy.request.json["nodeid"]
            raise Exception("Delete function is disabled")

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def list_database(self):
            """
            List the databases in a project. For more detail about
             project and database, see list_tree

            Example:
                POST /list_database HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"projnodeid": "XXXXXX"}

                >>> r = requests.post("http://127.0.0.1:56329/list_database", json={"projnodeid": "4c234c54cf47bcd1"})
                >>> pprint(r.json())
                {u'content': [{u'date': 1568668429.3203127,
                               u'id': u'b360596ef95e09e1',
                               u'name': u'lbzsb',
                               u'tick': 114}],
                 u'status': u'ok'}
            :return:
            """
            projnodeid = cherrypy.request.json["projnodeid"]
            ret = list_database(projnodeid, self.fs, self.storage)
            return {"status": "ok", "content": [c.build({}) for c in ret]}

        @cherrypy.expose
        @cherrypy.tools.json_out()
        def get_users(self):
            """
            Get users in the database, which contains user's name and its passhash.

            Example:
                GET /get_users HTTP/1.1
                Host: XXX

                >>> r = requests.get("http://127.0.0.1:56329/get_users")
                >>> pprint(r.json())
                {u'content': [{u'name': u'test',
                               u'passhash': u'e10adc3949ba59abbe56e057f20f883e',
                               u'pubkey': u'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQClkggj2HWjgn3Gw5PYs/mqok6xQQWSzuaRYcI1lUfyyQBQeoEapZ75GavBXqX9uLlea06F7fttvKZhYH3CbriywxHva9cm3MQaqp+4oYTZ+1LWhkyiChwWK5aW/uhOxr1rDCxyg74kWKSEe/jnw9j+yNkfBPNDaZUBwJyi6KI7MSvk8lcuJmjlZVF/WPZo8qo+7O70T9tuETYeY8sQRIhVSDxDahb9jmBLVX4Th0fWIRtW676cyOFxoqJauanQIN+P4ZhiSgvGsv8WpvjoPmORJ49jwWTa/bg9gRmGB4aFN5rmrDVsSKnMJsxtCa5aX3uOGlrAYSLCYT6OYGthPbjv misty@DESKTOP-HO0I1HQ
    '}],
                 u'status': u'ok'}
            :return:
            """
            users = self.storage.select_users()
            ret = [user.build({}) for user in users]
            return {"status": "ok", "content": ret}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def add_user(self):
            """
            Add a user.

            Example:
                POST /add_user HTTP/1.1
                Host: XXX
                Content-Length: XXX

                    {"user": "XXXXXX", "password": "XXXXXXXX", "pubkey": null}

                >>> r = requests.post("http://127.0.0.1:56329/add_user", json={"user": "test1", "password": "123456", "pubkey": None})
                >>> pprint(r.json())
                {u'status': u'ok'}
                >>> r = requests.get("http://127.0.0.1:56329/get_users")
                >>> pprint(r.json())
                {u'content': [{u'name': u'test',
                               u'passhash': u'e10adc3949ba59abbe56e057f20f883e'},
                              {u'name': u'test1',
                               u'passhash': u'e10adc3949ba59abbe56e057f20f883e'}],
                 u'status': u'ok'}
            :return:
            """
            username = cherrypy.request.json["user"]
            password = cherrypy.request.json["password"]
            pubkey = cherrypy.request.json["pubkey"]
            try:
                self.storage.insert_user(UserProto(username, calc_passhash(password), pubkey))
                return {"status": "ok"}
            except:
                import traceback
                traceback.print_exc()
                return {"status": "fail"}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def del_user(self):
            """
            Delete a user.

            Example:
                POST /add_user HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXXXXX"}

                >>> r = requests.post("http://127.0.0.1:56329/del_user", json={"user": "test1"})
                >>> pprint(r.json())
                {u'status': u'ok'}
                >>> r = requests.get("http://127.0.0.1:56329/get_users")
                >>> pprint(r.json())
                {u'content': [{u'name': u'test',
                               u'passhash': u'e10adc3949ba59abbe56e057f20f883e'}],
                 u'status': u'ok'}
            :return:
            """
            username = cherrypy.request.json["user"]
            try:
                users = self.storage.select_users(username)
                for user in users:
                    self.storage.delete_user(user)
                return {"status": "ok"}
            except:
                import traceback
                traceback.print_exc()
                return {"status": "fail"}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def update_user(self):
            """
            Update a user.
            Note: if you want to keep a parameter, use null
                if you want to cancel a password or a pubkey, use ""
            Example:
                POST /update_user HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXXXXX", "password": null, "pubkey": null}

                >>> r = requests.post("http://127.0.0.1:56329/del_user", json={"user": "test1"})
                >>> pprint(r.json())
                {u'status': u'ok'}
                >>> r = requests.get("http://127.0.0.1:56329/get_users")
                >>> pprint(r.json())
                {u'content': [{u'name': u'test',
                               u'passhash': u'e10adc3949ba59abbe56e057f20f883e'}],
                 u'status': u'ok'}
            :return:
            """
            username = cherrypy.request.json["user"]
            password = cherrypy.request.json["password"]
            pubkey = cherrypy.request.json["pubkey"]
            if not password:
                newpass = None
            else:
                newpass = calc_passhash(password)
            self.storage.update_user(username, newpass, pubkey)
            return {"status": "ok"}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def list_permission(self):
            """
            List the permissions. If you don't want filter a specific attribute, just pass null to that attribute.
            A permission record consists of user, nodeid and permtype, which refers to the
            enforced target user, the enforced target node, and the type of permission granted.
            The record will apply to itself and all its subnodes.
            Currently there're two permission types:
                PERMISSION_ALLOW = "allow"
                PERMISSION_DENY = "deny"
            For example, user=test, nodeid=XXXX, permtype=PERMTYPE_ALLOW means test can access XXX and all XXX's subnodes
                         user=test, nodeid=YYYY, permtype=PERMTYPE_DENY means test can't access YYY and all YYY's subnodes
            Currently, DENY record's priority is higher, if there's any DENY records applied to a node, then the user won't
             have access even there's any allow records.

            Example:
            1.
                POST /list_permission HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXXXXX", "nodeid":"XXX", "permtype": "allow"}
            2. don't filter nodeid
                POST /list_permission HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": null, "nodeid":"XXX", "permtype": "allow"}

                >>> r = requests.post("http://127.0.0.1:56329/list_permission", json={"user": "test", "nodeid":None, "permtype": "allow"})
                >>> pprint(r.json())
                [{u'nodeid': u'ac4cd9fd61fab8e6',
                  u'permission': u'allow',
                  u'username': u'test'},
                 {u'nodeid': u'c36ff475b8945664',
                  u'permission': u'allow',
                  u'username': u'test'}]

            :return:
            """
            username = cherrypy.request.json["user"]
            nodeid = cherrypy.request.json["nodeid"]
            permtype = cherrypy.request.json["permtype"]
            allperm = self.storage.select_all_permission(username=username, nodeid=nodeid, permtype=permtype)
            for c in allperm:
                serverpath = self.fs.path_manager.get_server_path(None if c["nodeid"] == "" else c["nodeid"])
                node = self.fs.path_manager.parse_server_path(serverpath)
                c["nodepath"] = node.nodepath
            return {"status": "ok", "content": allperm}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def add_permission(self):
            """
            Adds a permission.

            Example:
                POST /add_permission HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXXXXX", "nodeid":"XXX", "permtype": "allow"}

            :return:
            """
            username = cherrypy.request.json["user"]
            nodeid = cherrypy.request.json["nodeid"]
            permtype = cherrypy.request.json["permtype"]
            try:
                self.storage.insert_permission(username, nodeid, permtype)
                return {"status": "ok"}
            except:
                import traceback
                traceback.print_exc()
                return {"status": "fail"}

        @cherrypy.expose
        @cherrypy.tools.json_in()
        @cherrypy.tools.json_out()
        def remove_permission(self):
            """
            Remove a permission record. If you don't want to filter a attribute, you can simply pass null to that attribute.
            Example:
                POST /remove_permission HTTP/1.1
                Host: XXX
                Content-Length: XXX

                {"user": "XXXXXX", "nodeid":"XXX", "permtype": "allow"}
            :return:
            """
            username = cherrypy.request.json["user"]
            nodeid = cherrypy.request.json["nodeid"]
            permtype = cherrypy.request.json["permtype"]
            try:
                self.storage.delete_permission(username, nodeid, permtype)
                return {"status": "ok"}
            except:
                import traceback
                traceback.print_exc()
                return {"status": "fail"}

def create_restserver(parent):
    if cherrypy is not None:
        return RestServer(parent)
    else:
        logger.warning("Failed to import cherrypy, not starting REST server")
        return None