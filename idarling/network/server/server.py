# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import socket
import ssl

from idarling.network.discovery import ClientDiscoverer
from idarling.network.server.handler.authhandler import AuthHandler
from idarling.network.server.handler.mainhandler import MainHandler
from idarling.network.server.restserver import create_restserver
from idarling.network.server.serverfs import ServerFS
from idarling.network.socketimpl.sockets import ClientSocket, ServerSocket
from idarling.network.server.serverstorage import ServerStorage

import logging

logger = logging.getLogger("idarling.network.server")

class ServerClient(ClientSocket):
    """
    This class represents a client socketimpl for the server. It implements all the
    handlers for the packet the client is susceptible to send.
    """

    def __init__(self, auth_modes, parent=None):
        ClientSocket.__init__(self, parent)
        self._main_handler = MainHandler(self)
        self._auth_handler = AuthHandler(self, auth_modes)
        self.auth_ok = False

    def _get_current_handler(self):
        if not self._auth_handler.finished():
            return self._auth_handler
        else:
            return self._main_handler

    def __getattr__(self, item):
        return getattr(self._get_current_handler(), item)
    
    def __eq__(self, other):
        if isinstance(other, ServerClient):
            return other == self
        return self._get_current_handler() == other

    def wrap_socket(self, sock):
        sock.settimeout(0)  # No timeout
        sock.setblocking(0)  # No blocking
        ClientSocket.wrap_socket(self, sock)

        # Add host and port as a prefix to our logger
        prefix = "%s:%d" % sock.getpeername()

        class CustomAdapter(logging.LoggerAdapter):
            def process(self, msg, kwargs):
                return "(%s) %s" % (prefix, msg), kwargs

        global logger
        logger = CustomAdapter(logger, {})
        logger.info("Connected")

    def disconnect(self, err=None, notify=True):
        # Notify other users that we disconnected
        '''
        project, database = self.get_session_info()
        if project and database and notify:
            from idarling.core.session.sessioncommands import LeaveSession
            self.parent().forward_users(self, LeaveSession(self.name, False))
        '''
        self.parent().reject(self)
        ClientSocket.disconnect(self, err)
        self._get_current_handler().after_disconnect()
        logger.info("Disconnected")

    def recv_packet(self, packet):
        if not self._auth_handler.finished():
            return self._auth_handler.recv_packet(packet)
        else:
            if self._main_handler._name is None:
                self._main_handler._name = self._auth_handler.name
            return self._main_handler.recv_packet(packet)

    def get_session_info(self):
        return self._main_handler.project, self._main_handler.database

class Server(ServerSocket):
    """
    This class represents a server socketimpl for the server. It is used by both
    the integrated and dedicated server implementations. It doesn't do much.
    """

    SNAPSHOT_INTERVAL = 0  # ticks

    def __init__(self, parent=None):
        ServerSocket.__init__(self, parent)
        self._ssl = None
        self._clients = []
    
        # Initialize the storage
        self._storage = ServerStorage(self.server_file("database.db"))
        self._storage.initialize()

        projroot = self.server_file('projects')
        if not os.path.exists(projroot):
            os.makedirs(projroot)
        else:
            if not os.path.isdir(projroot):
                raise Exception("Project root exists and not a directory: %s" % projroot)
        self._fs = ServerFS(projroot)
        self._discovery = ClientDiscoverer()
        self._restserver = create_restserver(self)

    @property
    def storage(self):
        return self._storage

    @property
    def fs(self):
        return self._fs

    @property
    def host(self):
        return self._socket.getsockname()[0]

    @property
    def port(self):
        return self._socket.getsockname()[1]

    def start(self, host, port=0, ssl_=None):
        """Starts the server on the specified host and port."""
        logger.info("Starting the server on %s:%d" % (host, port))

        # Load the system certificate chain
        self._ssl = ssl_
        if self._ssl:
            cert, key = self._ssl
            self._ssl = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
            self._ssl.load_cert_chain(certfile=cert, keyfile=key)

        # Create, bind and set the socketimpl options
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            sock.bind((host, port))
        except socket.error as e:
            logger.warning("Could not start the server")
            logger.exception(e)
            return False
        sock.settimeout(0)  # No timeout
        sock.setblocking(0)  # No blocking
        sock.listen(5)
        self.connect(sock)

        # Start discovering clients
        host, port = sock.getsockname()
        self._discovery.start(host, port, self._ssl, self.server_authmode())
        if self._restserver is not None:
            self._restserver.start()
        return True

    def stop(self):
        """Terminates all the connections and stops the server."""
        logger.info("Stopping the server")
        self._discovery.stop()
        # Disconnect all clients
        for client in list(self._clients):
            #client.disconnect(notify=False)
            client.disconnect()
        self.disconnect()
        if self._restserver is not None:
            self._restserver.stop()
        return True

    def _accept(self, sock):
        """Called when an user connects."""
        client = ServerClient(self.server_authmode(), self)

        if self._ssl:
            # Wrap the socketimpl in an SSL tunnel
            sock = self._ssl.wrap_socket(
                sock, server_side=True, do_handshake_on_connect=False
            )

        client.wrap_socket(sock)
        self._clients.append(client)

    def reject(self, client):
        """Called when a user disconnects."""
        self._clients.remove(client)

    def get_users(self, client, matches=None):
        """Get the other users on the same database."""
        client_project, client_database = client.get_session_info()
        users = []
        for user in self._clients:
            project, database = user.get_session_info()
            if project is None or database is None:
                continue
            if (
                project.nodeid != client_project.nodeid
                or database.id != client_database.id
            ):
                continue
            if user == client or (matches and not matches(user)):
                continue
            users.append(user)
        return users

    def forward_users(self, client, packet, matches=None):
        """Sends the packet to the other users on the same database."""
        for user in self.get_users(client, matches):
            user.send_packet(packet)

    def server_file(self, filename):
        """Get the absolute path of a local resource."""
        raise NotImplementedError("server_file() not implemented")

    def server_authmode(self):
        raise NotImplementedError("server_authmode() not implemented")