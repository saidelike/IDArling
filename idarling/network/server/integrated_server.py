from idarling.network.server.server import Server

from idarling.managers import *


class IntegratedServer(Server):
    """
    The integrated server inherits the logic from Server. It simply needs to
    define the server_file method to indicate where to save the databases.
    """

    def __init__(self, parent=None):
        Server.__init__(self, parent)

    def server_file(self, filename):
        return ResourceManager().user_resource("files", filename)

    def server_authmode(self):
        return ["none"]