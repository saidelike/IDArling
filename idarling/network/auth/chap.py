import logging
import random
import string

from idarling.shared.authutils import challenge_calc
from idarling.core.session.sessioncommands import LoginHello, LoginNegotiate, LoginRequest, LOGINSTATUS_FAIL, LOGINSTATUS_SUCCESS
from idarling.network.server.handler.basehandler import BaseHandler

from idarling.managers import NetworkManager


logger = logging.getLogger("idarling.network.auth.chap")

class CHAPLogin(object):
    def __init__(self, username, passhash):
        self.username = username
        self.passhash = passhash

    def do_auth(self, finish_cb):
        def do_hello():
            d = NetworkManager().send_packet(
                LoginHello.Query()
            )
            d.add_callback(do_negotiate)
            d.add_errback(logger.exception)

        def do_negotiate(reply):
            if "chap" not in reply.supported_mode:
                logger.warning("Server does not support \"chap\" mode, supported modes are %s",
                               str(reply.supported_mode))
            d = NetworkManager().send_packet(
                LoginNegotiate.Query("chap")
            )
            d.add_callback(do_auth_hello)
            d.add_errback(logger.exception)

        def do_auth_hello(reply):
            if reply.status != LOGINSTATUS_SUCCESS:
                logger.warning("Failed to negotiate the auth mode")
                return
            d = NetworkManager().send_packet(
                LoginRequest.Query({"phase": "hello"})
            )
            d.add_callback(do_auth_challenge)
            d.add_errback(logger.exception)

        def do_auth_challenge(reply):
            resp = reply.login_resp_data
            if resp["phase"] != "challenge":
                logger.warning("Unexpected phase from server, should be challenge, received %s", resp["phase"])
                return
            challenge = resp["challenge"]
            challenge_resp = challenge_calc(self.username, self.passhash, challenge)
            d = NetworkManager().send_packet(
                LoginRequest.Query({"phase": "response", "name": self.username, "resp": challenge_resp})
            )
            d.add_callback(finish_auth)
            d.add_errback(logger.exception)

        def finish_auth(reply):
            resp = reply.login_resp_data
            if resp["phase"] != "check":
                logger.warning("Unexpected phase from server, should be check, received %s", resp["phase"])
                return
            if resp["status"] == LOGINSTATUS_SUCCESS:
                finish_cb(True)
            else:
                finish_cb(False)
        do_hello()

class CHAPAuthenticator(BaseHandler):
    AUTH_NAME = "base"

    def __init__(self, handler):
        super(CHAPAuthenticator, self).__init__(handler)
        self._login_challenge = None
        self._handlers = {
            LoginRequest.Query: self._login_handler,
        }
        self.success = False

    CHALLENGE_LENGTH = 256

    def recv_packet(self, packet):
        self._handlers[packet.__class__](packet)
        return True

    def _login_handler(self, query):
        login_data = query.login_req_data
        if login_data["phase"] == "hello":
            letters = string.ascii_letters # for python3 compatibility8
            challenge = ''.join(random.choice(letters) for i in range(self.CHALLENGE_LENGTH))
            self._login_challenge = challenge
            self.send_packet(LoginRequest.Reply(query, {"phase": "challenge", "challenge": challenge}))
        elif login_data["phase"] == "response":
            if not self._login_challenge:
                self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_FAIL}))
                self.disconnect()
                return
            name = login_data["name"]
            resp = login_data["resp"]
            user = self.parent().storage.select_user(name)
            if user and challenge_calc(name, user.passhash, self._login_challenge) == resp:
                self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_SUCCESS}))
                self._conn.name = name
                self.success = True
                return
            self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_FAIL}))
            self.disconnect()
        else:
            logger.warning("Unexpected LoginRequest received, disconnecting")
            self.disconnect()