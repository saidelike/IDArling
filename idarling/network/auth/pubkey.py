import logging
import io
import random
import codecs

import paramiko
from paramiko import Message

from idarling.network.server.handler.basehandler import BaseHandler
from idarling.core.session.sessioncommands import LOGINSTATUS_SUCCESS, LoginHello, LoginNegotiate, LoginRequest, \
    LOGINSTATUS_FAIL
from idarling.managers import NetworkManager

logger = logging.getLogger("idarling.network.client.auth.pubkey")


def _get_session_blob(sessionid, username, key):
    ret = bytes(str(sessionid).encode('utf-8') \
           + username.encode('utf-8') \
           + b"publickey")
    if key.public_blob:
        ret += bytes(key.public_blob.key_type) \
               + bytes(key.public_blob.key_blob)
    else:
        ret += bytes(key.get_name().encode('utf-8')) \
               + bytes(key.asbytes())

    return ret

class PubkeyLogin(object):
    def __init__(self, username, keytype, keydata):
        self._username = username
        self._key = paramiko.Transport._key_info[keytype](file_obj=io.BytesIO(bytes(keydata)))

    def do_auth(self, finish_cb):
        def do_hello():
            d = NetworkManager().send_packet(
                LoginHello.Query()
            )
            d.add_callback(do_negotiate)
            d.add_errback(logger.exception)

        def do_negotiate(reply):
            if "pubkey" not in reply.supported_mode:
                logger.warning("Server does not support \"pubkey\" mode, supported modes are %s",
                               str(reply.supported_mode))
            d = NetworkManager().send_packet(
                LoginNegotiate.Query("pubkey")
            )
            d.add_callback(do_auth_hello)
            d.add_errback(logger.exception)

        def do_auth_hello(reply):
            if reply.status != LOGINSTATUS_SUCCESS:
                logger.warning("Failed to negotiate the auth mode")
                return
            d = NetworkManager().send_packet(
                LoginRequest.Query({"phase": "hello", "username": self._username})
            )
            d.add_callback(do_auth_challenge)
            d.add_errback(logger.exception)

        def do_auth_challenge(reply):
            resp = reply.login_resp_data
            if resp["phase"] != "requestsig":
                logger.warning("Unexpected phase from server, should be challenge, received %s", resp["phase"])
                NetworkManager().disconnect()
                return
            self._sessionid = resp["sessionid"]
            blob = _get_session_blob(self._sessionid, self._username, self._key)
            sig = self._key.sign_ssh_data(blob)
            if self._key.public_blob:
                keytype = self._key.public_blob.key_type
                keyblob = self._key.public_blob.key_blob
            else:
                keytype = self._key.get_name()
                keyblob = bytes(self._key.asbytes())
            d = NetworkManager().send_packet(
                LoginRequest.Query({
                    "phase": "response",
                    "keytype": keytype,
                    "keyblob": codecs.encode(keyblob, 'hex'),
                    "sig": codecs.encode(sig.asbytes(), 'hex')
                })
            )
            d.add_callback(finish_auth)
            d.add_errback(logger.exception)

        def finish_auth(reply):
            resp = reply.login_resp_data
            if resp["phase"] != "check":
                logger.warning("Unexpected phase from server, should be check, received %s", resp["phase"])
                return
            if resp["status"] == LOGINSTATUS_SUCCESS:
                finish_cb(True)
            else:
                finish_cb(False)
        do_hello()

class PubkeyAuthenticator(BaseHandler):
    AUTH_NAME = "base"

    class StubQuery:
        id = -1

    def __init__(self, handler):
        super(PubkeyAuthenticator, self).__init__(handler)
        self._handlers = {
            LoginRequest.Query: self._login_handler,
        }
        self.success = False

        self._sessionid = None
        self._username = None

    def recv_packet(self, packet):
        self._handlers[packet.__class__](packet)
        return True

    def _login_handler(self, query):
        login_data = query.login_req_data
        if login_data["phase"] == "hello":
            self._sessionid = str(random.random())
            self._username = login_data["username"]
            self.send_packet(LoginRequest.Reply(query, {"phase": "requestsig", "sessionid": self._sessionid}))
        elif login_data["phase"] == "response":
            keytype = login_data["keytype"]
            keyblob = codecs.decode(login_data["keyblob"], 'hex')
            sig = codecs.decode(login_data["sig"], 'hex')
            _key = paramiko.Transport._key_info[keytype](data=bytes(keyblob))
            user = self.parent().storage.select_user(self._username)
            if user and user.pubkey:
                _keyinfo = user.pubkey.split(" ")
                keytype = _keyinfo[0]
                keyblob = paramiko.py3compat.decodebytes(_keyinfo[1].encode('utf-8'))
                serverkey = paramiko.Transport._key_info[keytype](data=keyblob)
                if serverkey == _key:
                    blob = _get_session_blob(self._sessionid, self._username, _key)
                    if not _key.verify_ssh_sig(blob, Message(sig)):
                        logger.info("Auth rejected: invalid signature")
                    else:
                        self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_SUCCESS}))
                        self.success = True
                        self._conn.name = self._username
                        return
            self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_FAIL}))
        else:
            logger.warning("Unexpected LoginRequest received, disconnecting")
            self.disconnect()