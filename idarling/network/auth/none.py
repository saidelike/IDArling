import logging
import random
import string

from idarling.shared.authutils import challenge_calc
from idarling.core.session.sessioncommands import LoginHello, LoginNegotiate, LoginRequest, LOGINSTATUS_FAIL, LOGINSTATUS_SUCCESS
from idarling.network.server.handler.basehandler import BaseHandler

from idarling.managers import NetworkManager


logger = logging.getLogger("idarling.network.auth.none")

class NoneLogin(object):
    def __init__(self, username):
        self.username = username

    def do_auth(self, finish_cb):
        def do_hello():
            d = NetworkManager().send_packet(
                LoginHello.Query()
            )
            d.add_callback(do_negotiate)
            d.add_errback(logger.exception)

        def do_negotiate(reply):
            if "chap" not in reply.supported_mode:
                logger.warning("Server does not support \"none\" mode, supported modes are %s",
                               str(reply.supported_mode))
            d = NetworkManager().send_packet(
                LoginNegotiate.Query("none")
            )
            d.add_callback(do_auth_hello)
            d.add_errback(logger.exception)

        def do_auth_hello(reply):
            if reply.status != LOGINSTATUS_SUCCESS:
                logger.warning("Failed to negotiate the auth mode")
                return
            d = NetworkManager().send_packet(
                LoginRequest.Query({"phase": "hello", "name": self.username})
            )
            d.add_callback(finish_auth)
            d.add_errback(logger.exception)

        def finish_auth(reply):
            resp = reply.login_resp_data
            if resp["phase"] != "check":
                logger.warning("Unexpected phase from server, should be check, received %s", resp["phase"])
                return
            if resp["status"] == LOGINSTATUS_SUCCESS:
                finish_cb(True)
            else:
                finish_cb(False)
        do_hello()

class NoneAuthenticator(BaseHandler):
    AUTH_NAME = "base"

    def __init__(self, handler):
        super(NoneAuthenticator, self).__init__(handler)
        self._handlers = {
            LoginRequest.Query: self._login_handler,
        }
        self.success = False

    CHALLENGE_LENGTH = 256

    def recv_packet(self, packet):
        self._handlers[packet.__class__](packet)
        return True

    def _login_handler(self, query):
        login_data = query.login_req_data
        if login_data["phase"] == "hello":
            self.success = True
            self._conn.name = login_data["name"]
            self.send_packet(LoginRequest.Reply(query, {"phase": "check", "status": LOGINSTATUS_SUCCESS}))
        else:
            logger.warning("Unexpected LoginRequest received, disconnecting")
            self.disconnect()