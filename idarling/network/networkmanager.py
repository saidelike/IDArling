# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import errno
import logging
import socket
import ssl

from PyQt5.QtCore import QTimer

from idarling.managers import *
from idarling.shared.utils import Singleton
from ..module import Module

logger = logging.getLogger("idarling.config")

@Singleton
class NetworkManager(Module):
    """
    This is the interface module. It is responsible for interacting with the
    server over the network. It manages the three sockets used with the plugin
    (client, discovery client, integrated server).
    """

    def __init__(self):
        #super(NetworkManager, self).__init__()
        Module.__init__(self)

        from idarling.network.discovery import ServerDiscoverer
        self._discoverer = ServerDiscoverer()

        self._client = None
        self._server = None
        self._integrated = None

    @property
    def client(self):
        return self._client

    @property
    def server(self):
        return self._server

    @property
    def discovery(self):
        return self._discoverer

    @property
    def connected(self):
        return self._client.connected if self._client else False

    @property
    def started(self):
        return bool(self._integrated)

    def _install(self):
        self._discoverer.start()
        return True

    def _uninstall(self):
        self._discoverer.stop()
        self.disconnect()
        self.stop_server()
        return True

    def connect(self, server):
        # type: (dict) -> None
        """Connect to the specified server."""

        # Make sure we're not already connected
        if self._client:
            return

        from .client import Client
        self._client = Client()
        self._server = server.copy()  # Make a copy
        host = self._server["host"]
        if host == "0.0.0.0":  # Windows can't connect to 0.0.0.0
            host = "127.0.0.1"
        port = self._server["port"]
        no_ssl = self._server["no_ssl"]

        # Update the user interface
        InterfaceManager().update()
        logger.info("Connecting to %s:%d..." % (host, port))

        # Create a new socketimpl
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        # Wrap the socketimpl in a SSL tunnel
        if not no_ssl:
            ctx = ssl.create_default_context()
            sock = ctx.wrap_socket(
                sock, server_hostname=host, do_handshake_on_connect=False
            )

        # Connect the socketimpl
        sock.settimeout(0)  # No timeout
        sock.setblocking(0)  # No blocking
        ret = sock.connect_ex((host, port))
        self._client.wrap_socket(sock)
        if ret != 0 and ret != errno.EINPROGRESS and ret != errno.EWOULDBLOCK:
            self._client.disconnect()
            return

        def setKeepAlive():
            # Set TCP keep-alive options
            cnt = ConfigManager().keep.cnt
            intvl = ConfigManager().keep.intvl
            idle = ConfigManager().keep.idle
            self._client.set_keep_alive(cnt, intvl, idle)

        QTimer.singleShot(2000, setKeepAlive)

    def login(self, cb):
        name = ConfigManager().user.name
        authmode = self._server["authmode"]
        authargs = self._server["authargs"]
        SessionManager().login(authmode, name, cb, authargs)

    def disconnect(self):
        """Disconnect from the current server."""
        # Make sure we aren't already disconnected
        if not self._client:
            return

        SessionManager().leave_session()
        logger.info("Disconnecting...")
        self._client.disconnect()

    def send_packet(self, packet):
        """Send a packet to the server."""
        if self.connected:
            return self._client.send_packet(packet)
        return None

    def start_server(self):
        """Start the integrated server."""
        if self._integrated:
            return

        from idarling.network.server.integrated_server import IntegratedServer
        logger.info("Starting the integrated server...")
        server = IntegratedServer()
        if not server.start("0.0.0.0"):
            return  # Couldn't start the server
        self._integrated = server
        integrated_arg = {
            "host": "0.0.0.0",
            "port": server.port,
            "no_ssl": True,
            "authmode": ["none"],
            "authargs": []
        }
        # Connect the client to the server
        self.disconnect()
        self.connect(integrated_arg)

    def stop_server(self):
        """Stop the integrated server."""
        if not self._integrated:
            return

        logger.info("Stopping the integrated server...")
        self.disconnect()
        self._integrated.stop()
        self._integrated = None
