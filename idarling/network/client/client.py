# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import ida_auto
import ida_idaapi
import ida_kernwin

from PyQt5.QtGui import QImage, QPixmap  # noqa: I202

from idarling.managers import *
from idarling.core.project.projectcommands import *
from idarling.core.session.sessioncommands import *
from idarling.core.user.usercommands import *
from idarling.interface.project.save import SaveActionHandler

from idarling.interface.statuswidget import StatusWidget

from idarling.network.socketimpl.packets import Command, Event
from idarling.network.socketimpl.sockets import ClientSocket

import logging
logger = logging.getLogger("idarling.network.client")

class Client(ClientSocket):
    """
    This class represents a client socketimpl for the client. It implements all the
    handlers for the packet the server is susceptible to send.
    """

    def __init__(self, parent=None):
        ClientSocket.__init__(self, parent)
        self._events = []

        # Setup command handlers
        self._handlers = {
            JoinSession.Query: self._handle_join_session,
            LeaveSession: self._handle_leave_session,
            UpdateLocation: self._handle_update_location,
            InviteToLocation: self._handle_invite_to_location,
            #UpdateUserName: self._handle_update_user_name,
            UpdateUserColor: self._handle_update_user_color,
            DownloadFile.Query: self._handle_download_file,
        }

    def call_events(self):
        while self._events and ida_auto.get_auto_state() == ida_auto.AU_NONE:
            packet = self._events.pop(0)
            self._call_event(packet)

    def _call_event(self, packet):
        HookManager().session_unhook()

        try:
            packet()
        except Exception as e:
            logger.warning("Error while calling event")
            logger.exception(e)

        HookManager().session_hook()

        # Check for de-synchronization
        if SessionManager().tick >= packet.tick:
            logger.warning("De-synchronization detected!")
            packet.tick = SessionManager().tick
        SessionManager().tick = packet.tick
        logger.debug("returning from call_event")

    def recv_packet(self, packet):
        if isinstance(packet, Command):
            # Call the corresponding handler
            self._handlers[packet.__class__](packet)

        elif isinstance(packet, Event):
            # If we already have some events queued
            if self._events or ida_auto.get_auto_state() != ida_auto.AU_NONE:
                self._events.append(packet)
            else:
                self._call_event(packet)

        else:
            return False
        return True

    def send_packet(self, packet):
        if isinstance(packet, Event):
            SessionManager().tick += 1
            packet.tick = SessionManager().tick
        return ClientSocket.send_packet(self, packet)

    def disconnect(self, err=None):
        ret = ClientSocket.disconnect(self, err)
        NetworkManager()._client = None
        NetworkManager()._server = None

        # Update the user interface
        InterfaceManager().update()
        InterfaceManager().clear_invites()
        return ret

    def _check_socket(self):
        was_connected = self._connected
        ret = ClientSocket._check_socket(self)
        if not was_connected and self._connected:
            # Update the user interface
            InterfaceManager().update()
            def loggedin(status):
                if status:
                    # Subscribe to the events
                    logger.info("Successfully logged in, try joining session")
                    SessionManager().join_session()

            NetworkManager().login(loggedin)

        return ret

    def _handle_join_session(self, packet):
        # Update the users list
        user = {"color": packet.color, "ea": packet.ea}
        UserManager().add_user(packet.name, user)

        # Show a toast notification
        if packet.silent:
            return
        text = "%s joined the session" % packet.name
        template = QImage(ResourceManager().static_resource("user.png"))
        icon = StatusWidget.make_icon(template, packet.color)
        InterfaceManager().show_invite(text, icon)

    def _handle_leave_session(self, packet):
        # Update the users list
        user = UserManager().remove_user(packet.name)
        if not user:
            return
        # Refresh the users count
        InterfaceManager().widget.refresh()

        # Show a toast notification
        if packet.silent:
            return
        text = "%s left the session" % packet.name
        template = QImage(ResourceManager().static_resource("user.png"))
        icon = StatusWidget.make_icon(template, user["color"])
        InterfaceManager().show_invite(text, icon)

    def _handle_invite_to_location(self, packet):
        # Show a toast notification
        text = "%s - Jump to %#x" % (packet.name, packet.loc)
        icon = ResourceManager().static_resource("location.png")

        def callback():
            ida_kernwin.jumpto(packet.loc)

        InterfaceManager().show_invite(text, QPixmap(icon), callback)

    def _handle_update_user_name(self, packet):
        # Update the users list
        user = UserManager().remove_user(packet.old_name)
        if not user:
            return
        UserManager().add_user(packet.new_name, user)

    # TODO: fix the user
    def _handle_update_user_color(self, packet):
        # Update the users list
        user = UserManager().get_user(packet.name)
        user["color"] = packet.new_color
        UserManager().add_user(packet.name, user)

    def _handle_update_location(self, packet):
        # Update the users list
        user = UserManager().get_user(packet.name)
        if not user:
            return

        user["ea"] = packet.ea
        UserManager().add_user(packet.name, user)

        InterfaceManager().painter.refresh_pseudocode_window()

        followed = InterfaceManager().followed
        if followed == packet.name or followed == "everyone":
            if packet.ea != ida_idaapi.BADADDR:
                twidget = ida_kernwin.get_current_viewer()
                widget_type = ida_kernwin.get_widget_type(twidget)
                if (
                    widget_type == ida_kernwin.BWN_DISASM
                        or widget_type == ida_kernwin.BWN_DUMP
                ):
                    ida_kernwin.jumpto(packet.ea, ida_kernwin.UIJMP_DONTPUSH)

    def _handle_download_file(self, query):
        # Upload the current database
        SessionManager().project.reply_autosave_database(query)
