# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import os

import ida_idaapi
import ida_kernwin

from idarling.managers import *
from idarling.core.core import CoreModule
from .shared.utils import start_logging

import logging
logger = logging.getLogger("idarling.plugin")

class Plugin(ida_idaapi.plugin_t):
    """
    This is the main class of the plugin. It subclasses plugin_t as required
    by IDA. It holds the modules of plugin, which themselves provides the
    functionality of the plugin (hooking/events, interface, networking, etc.).
    """

    # Mandatory definitions
    PLUGIN_NAME = "IDArling"
    PLUGIN_VERSION = "0.0.1"
    PLUGIN_AUTHORS = "The IDArling Team"

    # These flags specify that the plugin should persist between databases
    # loading and saving, and should not have a menu entry.
    flags = ida_idaapi.PLUGIN_FIX | ida_idaapi.PLUGIN_HIDE
    comment = "Collaborative Reverse Engineering plugin"
    help = ""
    wanted_name = PLUGIN_NAME
    wanted_hotkey = ""

    @staticmethod
    def description():
        """Return the description displayed in the console."""
        return "{} v{}".format(Plugin.PLUGIN_NAME, Plugin.PLUGIN_VERSION)

    def __init__(self):
        # Check if the plugin is running with IDA terminal
        if not ida_kernwin.is_idaq():
            raise RuntimeError("IDArling cannot be used in terminal mode")

        # Load the default configuration
        # Then setup the default logger
        log_path = ResourceManager().user_resource("logs", "idarling.%s.log" % os.getpid())
        level = ConfigManager().level
        start_logging(log_path, level)

        self._core = CoreModule()
        self._interface = InterfaceManager()
        self._network = NetworkManager()
        self._update = UpdateManager()
        self._update.set_current_version(Plugin.PLUGIN_VERSION)

    @property
    def core(self):
        return self._core

    @property
    def interface(self):
        return self._interface

    @property
    def network(self):
        return self._network

    def init(self):
        """
        This method is called when IDA is loading the plugin. It will first
        load the configuration file, then initialize all the modules.
        """
        try:
            ConfigManager().load_config()

            self._interface.install()
            self._network.install()
            self._core.install()
            self._update.install()
        except Exception as e:
            logger.error("Failed to initialize")
            logger.exception(e)
            skip = ida_idaapi.PLUGIN_SKIP
            return skip

        self._print_banner()
        logger.info("Initialized properly")
        keep = ida_idaapi.PLUGIN_KEEP
        return keep

    def _print_banner(self):
        """Print the banner that you see in the console."""
        copyright = "(c) %s" % self.PLUGIN_AUTHORS
        logger.info("-" * 75)
        logger.info("%s - %s" % (self.description(), copyright))
        logger.info("-" * 75)

    def term(self):
        """
        This method is called when IDA is unloading the plugin. It will
        terminated all the modules, then save the configuration file.
        """
        try:
            self._core.uninstall()
            self._network.uninstall()
            self._interface.uninstall()

            ConfigManager().save_config()
        except Exception as e:
            logger.error("Failed to terminate properly")
            logger.exception(e)
            return

        logger.info("Terminated properly")

    def run(self, _):
        """
        This method is called when IDA is running the plugin as a script.
        Because IDArling isn't runnable per se, we need to return False.
        """
        ida_kernwin.warning("IDArling cannot be run as a script")
        return False
