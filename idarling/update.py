# Update Manager
# Author: BrieflyX

import os
import shutil
from urllib2 import urlopen, URLError
import zipfile
import json
import tempfile

import idaapi
import ida_diskio
import ida_loader

import logging

from idarling.managers import ConfigManager
from idarling.shared.utils import Singleton
from module import Module

logger = logging.getLogger("idarling.update")

def version_t(ver_str):
    major, minor, patch = (0, 0, 0)
    vlist = ver_str.strip().replace('v', '').split('.')
    major = int(vlist[0])
    minor = int(vlist[1])
    if len(vlist) > 2:
        patch = int(vlist[2])
    return (major, minor, patch)

def v2str(ver):
    return 'v{}.{}.{}'.format(*ver)

@Singleton
class UpdateManager(Module):

    def __init__(self):
        Module.__init__(self)

        # conf = ConfigManager()
        # self._repo = conf.get('update')['repo']
        # self._startup = conf.get('update')['startup_check']
        # self._api = conf.get('update')['api']

        # TODO: Temporarily using a hard-coded value for config
        self._repo = 'THUCSTA/IDArlingProject/IDArling'
        self._startup = False
        self._api = 'gitlab'

    def set_current_version(self, ver_str):
        self._current_version = version_t(ver_str)
        self._target_version_str = None

    def _install(self):
        if self._startup:
            self.check_update()

    def check_update(self):
        latest_version, latest_version_str = self.get_version()
        if not latest_version:
            return
        logger.info('Latest idarling version is {}'.format(latest_version_str))
        if latest_version > self._current_version:
            self._target_version_str = latest_version_str
            # Pop a dialog asking whether update
            if idaapi.ask_yn(idaapi.ASKBTN_YES, 'There is a newer version {} of idarling, do you want to update?'.format(v2str(latest_version))) == idaapi.ASKBTN_YES:
                try:
                    self.do_update()
                    idaapi.info('Idarling updated successfully. Please restart your IDA pro.')
                except Exception as e:
                    idaapi.info('Update failed: {}'.format(str(e)))
        else:
            idaapi.info('Current version {} is the newest!'.format(v2str(self._current_version)))

    def get_version(self):
        tag_url = 'https://gitlab.com/api/v4/projects/' + self._repo.replace('/', '%2F') + '/repository/tags'
        try:
            data = json.loads(urlopen(tag_url, timeout=5).read())
        except URLError as e:
            idaapi.info('Check version error: {}'.format(str(e)))
            return None, None

        latest_version = version_t('0.0.0')
        latest_version_str = 'v0.0.0'
        for v in data:
            if version_t(v['name']) > latest_version:
                latest_version = version_t(v['name'])
                latest_version_str = v['name']
        return latest_version, latest_version_str

    def do_update(self):
        # Mostly copy `easy_install.py`
        logger.debug('Updating idarling ...')

        plug_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        logger.debug('Plugin directory: {}'.format(plug_dir))
        temp_dir = tempfile.mkdtemp()

        archive_path = os.path.join(temp_dir, "release.zip")
        zip_url = 'https://gitlab.com/{}/-/archive/{}/{}-{}.zip'.format(self._repo, self._target_version_str, self._repo.split('/')[-1], self._target_version_str)
        if os.path.exists(archive_path):
            os.remove(archive_path)
        logger.debug('Download url: {}'.format(zip_url))
        with open(archive_path, "wb") as f:
            f.write(urlopen(zip_url, timeout=5).read())

        archive_dir = os.path.join(temp_dir, '{}-{}'.format(self._repo.split('/')[-1], self._target_version_str))
        with zipfile.ZipFile(archive_path, "r") as zip:
            zip.extractall(temp_dir)
            # for zip_file in zip.namelist():
            #     if zip_file.startswith(os.path.basename(archive_dir)):
            #         zip.extract(zip_file, plug_dir)

        logger.debug("Moving the IDArling files...")
        src_path = os.path.join(archive_dir, "idarling_plugin.py")
        dst_path = os.path.join(plug_dir, os.path.basename(src_path))
        if os.path.exists(dst_path):
            os.remove(dst_path)
        shutil.move(src_path, dst_path)
        src_dir = os.path.join(archive_dir, "idarling")
        dst_dir = os.path.join(plug_dir, os.path.basename(src_dir))
        if os.path.exists(dst_dir):
            shutil.rmtree(dst_dir)
        shutil.move(src_dir, dst_dir)

        print("[*] Removing temp directory...")
        shutil.rmtree(temp_dir)

        # print("[*] Loading IDArling into IDA Pro...")
        # plugin_path = os.path.join(plug_dir, "idarling_plugin.py")
        # ida_loader.load_plugin(plugin_path)
        return True
