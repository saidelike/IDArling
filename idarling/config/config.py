import colorsys
import json
import os
import random
from idarling.shared.utils import Singleton, ROOT_LOGGER_NAME

import logging
logger = logging.getLogger("idarling.config")

from idarling.managers import *


class ConfigBase(object):
    """
        Base class of config item, will automatically do the serialization
        Supports nested config item
    """
    ATTRS = []

    def __init__(self, **kwargs):
        for attr in self.ATTRS:
            if kwargs.has_key(attr):
                setattr(self, attr, kwargs.pop(attr))
            # else:
            #    raise ValueError("Missing attr in init parameter: %s" % attr)

    def serialize(self):
        if self.ATTRS is not None:
            ret = {}
            for k in self.ATTRS:
                v = getattr(self, k)
                if issubclass(type(v), ConfigBase):
                    v = v.serialize()
                ret[k] = v
            return ret
        else:
            return {}

    def deserialize(self, dct):
        for k, v in dct.items():
            if k in self.ATTRS:
                setattr(self, k, v)

@Singleton
class ConfigManager(ConfigBase):
    """Root item of the config, which is also the manager for config"""
    ATTRS = ["level", "servers", "keep", "cursors", "user"]

    @property
    def level(self):
        """
        
        :return:
        """
        return getattr(self, "_level", self.__defaults["_level"])

    @level.setter
    def level(self, value):
        self._level = value

    @property
    def servers(self):
        return getattr(self, "_servers", self.__defaults["_servers"])

    @servers.setter
    def servers(self, value):
        self._servers = value

    @property
    def keep(self):
        return getattr(self, "_keep", self.__defaults["_keep"])

    @keep.setter
    def keep(self, value):
        if type(value) is ConfigManager.KeepConfig:
            self._keep = value
        else:
            if not hasattr(self, "_keep") or getattr(self, "_keep") is None:
                self._keep = ConfigManager.KeepConfig()
            self._keep.deserialize(value)

    @property
    def cursors(self):
        return getattr(self, "_cursor", self.__defaults["_cursor"])

    @cursors.setter
    def cursors(self, value):
        if type(value) is ConfigManager.CursorsConfig:
            self._cursors = value
        else:
            if not hasattr(self, "_cursors") or getattr(self, "_cursors") is None:
                self._cursors = ConfigManager.CursorsConfig()
            self._cursors.deserialize(value)

    @property
    def user(self):
        return getattr(self, "_user", self.__defaults["_user"])

    @user.setter
    def user(self, value):
        if type(value) is ConfigManager.UserConfig:
            self._user = value
        else:
            if not hasattr(self, "_user") or getattr(self, "_user") is None:
                self._user = ConfigManager.UserConfig()
            self._user.deserialize(value)

    class KeepConfig(ConfigBase):
        ATTRS = ["cnt", "intvl", "idle"]

    class CursorsConfig(ConfigBase):
        ATTRS = ["navbar", "funcs", "disasm", "hexrays"]

    class UserConfig(ConfigBase):
        ATTRS = ["color", "name", "notifications"]

    def __init__(self):
        r, g, b = colorsys.hls_to_rgb(random.random(), 0.5, 1.0)
        color = int(b * 255) << 16 | int(g * 255) << 8 | int(r * 255)

        self.__defaults = { # make sure all config attrs are presend here
            "_level": logging.INFO,
            "_servers": [],
            "_keep": ConfigManager.KeepConfig(cnt=4, intvl=15, idle=240),
            "_cursor": ConfigManager.CursorsConfig(navbar=True, funcs=True, disasm=True, hexrays=False),
            "_user": ConfigManager. UserConfig(color=color, name="unnamed", notifications=True)
        }
        '''
        if type(config) is Config:
            self.level = config.level
            self.servers = config.servers
            self.keep = config.keep
            self.cursors = config.cursors
            self.user = config.user
        else:
            self.deserialize(config)
        '''
        ConfigBase.__init__(self)

    def load_config(self):
        """
        Load the configuration file. It is a JSON file that contains all the
        settings of the plugin. The configured log level is set here.
        """
        config_path = ResourceManager().user_resource("files", "config.json")
        if not os.path.isfile(config_path):
            return
        with open(config_path, "rb") as config_file:
            try:
                loaded_config = json.loads(config_file.read())
                self.deserialize(loaded_config)
            except ValueError:
                logger.warning("Couldn't load config file")
                return
            logging.getLogger(ROOT_LOGGER_NAME).setLevel(self.level)
            logger.debug("Loaded config: %s" % loaded_config)

    def save_config(self):
        """Save the configuration file."""
        config_path = ResourceManager().user_resource("files", "config.json")
        with open(config_path, "wb") as config_file:
            serialized_config = json.dumps(self.serialize())
            config_file.write(serialized_config)
            logger.debug("Saved config: %s" % serialized_config)

    def reset_config(self):
        for attr in self.__defaults.keys():
            if hasattr(self, attr):
                delattr(self, attr)