def ConfigManager():
    import idarling.config.config  # import ConfigManager
    return idarling.config.config.ConfigManager()

def InterfaceManager():
    import idarling.interface.interface  # import InterfaceManager
    return idarling.interface.interface.InterfaceManager()

def NetworkManager():
    import idarling.network.networkmanager  # import NetworkManager
    return idarling.network.networkmanager.NetworkManager()

def SessionManager():
    import idarling.core.session.session
    return idarling.core.session.session.SessionManager()

def HookManager():
    import idarling.core.hooks.hook
    return idarling.core.hooks.hook.HookManager()

def ProjectManager():
    import idarling.core.project.project  # import ProjectManager
    return idarling.core.project.project.ProjectManager()

def UserManager():
    import idarling.core.user  # import UserManager
    return idarling.core.user.UserManager()

def ResourceManager():
    import idarling.shared.resource  # import ResourceManager
    return idarling.shared.resource.ResourceManager()

def UpdateManager():
    import idarling.update
    return idarling.update.UpdateManager()
